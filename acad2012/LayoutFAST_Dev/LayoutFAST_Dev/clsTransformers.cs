﻿#region Namespaces

using System;
using System.Text;
using System.Linq;
using System.Xml;
using System.Reflection;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.IO;


#endregion

namespace testplugin
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class clsTransformers : IExternalCommand
    {
        #region Cached Variables

        private static ExternalCommandData _cachedCmdData;
        
        public static UIApplication CachedUiApp
        {
            get
            {
                return _cachedCmdData.Application;
            }
        }

        public static RvtApplication CachedApp
        {
            get
            {
                return CachedUiApp.Application;
            }
        }

        public static RvtDocument CachedDoc
        {
            get
            {
                return CachedUiApp.ActiveUIDocument.Document;
            }
        }
        
        #endregion

        #region IExternalCommand Members         

        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            _cachedCmdData = cmdData;

            try
            {
                //TODO: add your code below.

                bool blnexitnow = false;
                UIApplication application = cmdData.Application;
                Document document = application.ActiveUIDocument.Document;
                if (document.ActiveView.ViewType.ToString() == "ThreeD" || document.ActiveView.ViewType.ToString().Contains("plan") || document.ActiveView.ViewType.ToString().Contains("Plan"))
                {
                    frmSEProducts f = new frmSEProducts(cmdData, out blnexitnow);
                    if (!blnexitnow)
                    {
                        f.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Your current view type is : " + document.ActiveView.ViewType.ToString() +
                                    "\n Your current view is : " + document.ActiveView.ViewName.ToString() +
                                    "\n\nPlease change view to either the 3D view or a Plan View");
                }

                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }

        #endregion
    }
}