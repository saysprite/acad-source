﻿namespace LayoutFAST_Dev
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnSwitchboards = new System.Windows.Forms.Button();
            this.btnTransformers = new System.Windows.Forms.Button();
            this.btnPanelboards = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSwitchboards
            // 
            this.btnSwitchboards.BackColor = System.Drawing.Color.White;
            this.btnSwitchboards.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSwitchboards.Image = global::LayoutFAST_Dev.Properties.Resources.Distribution_Section_32x32;
            this.btnSwitchboards.Location = new System.Drawing.Point(232, 11);
            this.btnSwitchboards.Name = "btnSwitchboards";
            this.btnSwitchboards.Size = new System.Drawing.Size(111, 69);
            this.btnSwitchboards.TabIndex = 2;
            this.btnSwitchboards.Text = "Switchboards";
            this.btnSwitchboards.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSwitchboards.UseVisualStyleBackColor = false;
            this.btnSwitchboards.Click += new System.EventHandler(this.btnSwitchboards_Click);
            // 
            // btnTransformers
            // 
            this.btnTransformers.BackColor = System.Drawing.Color.White;
            this.btnTransformers.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTransformers.Image = global::LayoutFAST_Dev.Properties.Resources.transformer_32x32;
            this.btnTransformers.Location = new System.Drawing.Point(117, 11);
            this.btnTransformers.Name = "btnTransformers";
            this.btnTransformers.Size = new System.Drawing.Size(111, 69);
            this.btnTransformers.TabIndex = 1;
            this.btnTransformers.Text = "Transformers";
            this.btnTransformers.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTransformers.UseVisualStyleBackColor = false;
            this.btnTransformers.Click += new System.EventHandler(this.btnTransformers_Click);
            // 
            // btnPanelboards
            // 
            this.btnPanelboards.BackColor = System.Drawing.Color.White;
            this.btnPanelboards.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPanelboards.Image = global::LayoutFAST_Dev.Properties.Resources.trim_32x32;
            this.btnPanelboards.Location = new System.Drawing.Point(2, 11);
            this.btnPanelboards.Name = "btnPanelboards";
            this.btnPanelboards.Size = new System.Drawing.Size(111, 69);
            this.btnPanelboards.TabIndex = 0;
            this.btnPanelboards.Text = "Panelboards";
            this.btnPanelboards.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPanelboards.UseVisualStyleBackColor = false;
            this.btnPanelboards.Click += new System.EventHandler(this.btnPanelboards_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 86);
            this.Controls.Add(this.btnSwitchboards);
            this.Controls.Add(this.btnTransformers);
            this.Controls.Add(this.btnPanelboards);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LayoutFAST Dev";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPanelboards;
        private System.Windows.Forms.Button btnTransformers;
        private System.Windows.Forms.Button btnSwitchboards;
    }
}