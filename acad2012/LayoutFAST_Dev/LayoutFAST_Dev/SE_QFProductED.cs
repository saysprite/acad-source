﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using System.Xml;
using System.Xml.Serialization;

namespace LayoutFAST_Dev
{
    class SE_QFProductED
    {

        private List<ReadXLS.clsSEQF_Product> XLS2XML_Deserializer(string strReadXMLFile)
        {
            List<ReadXLS.clsSEQF_Product> lstCOEnt = new List<ReadXLS.clsSEQF_Product>();

            XmlSerializer deserializer = new XmlSerializer(typeof(List<ReadXLS.clsSEQF_Product>));

            if (File.Exists(strReadXMLFile))
            {
                TextReader textReader = new StreamReader(strReadXMLFile);
                lstCOEnt = (List<ReadXLS.clsSEQF_Product>)deserializer.Deserialize(textReader);
                textReader.Close();
            }  //if (File.Exists(strReadXMLFile))

            return lstCOEnt;
        }

        private void XLS2XML_SerializeToXMLList(List<ReadXLS.clsSEQF_Product> lst, string strWriteXMLFile)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<ReadXLS.clsSEQF_Product>));
            TextWriter textwriter = new StreamWriter(strWriteXMLFile);

            serializer.Serialize(textwriter, lst);
            textwriter.Close();
        }

        public int XSL2XML_CreateNEWXML(List<ReadXLS.clsSEQF_Product> lst, string strXMLFile)
        {
            int i = 0;
            
            try
            {
                i = 0;
                XLS2XML_SerializeToXMLList(lst, strXMLFile);
            }
            catch
            {
                i = -1;
            }

            return i;
        }

        public int XLS2XML_InsertRecord(string strXMLFile,string column,int irownumber,string row,string sheet,string value)
        {
            int ireturn = 0;

            List<ReadXLS.clsSEQF_Product> lstCOEnt = new List<ReadXLS.clsSEQF_Product>();
            
            try
            {
                lstCOEnt = XLS2XML_Deserializer(strXMLFile);
                //add new cls
                ReadXLS.clsSEQF_Product cls = new ReadXLS.clsSEQF_Product();
                cls.column = column;
                cls.irownumber = irownumber;
                cls.row=row;
                cls.sheet= sheet;
                cls.value = value;
                lstCOEnt.Add(cls);

                //write list to xml file
                XLS2XML_SerializeToXMLList(lstCOEnt, strXMLFile);
            }
            catch
            {
                ireturn = -1;
            }

            return ireturn;
        }

        public List<ReadXLS.clsSEQF_Product> XLS2XML_readRecord(string strReadXMLFile)
        {
            List<ReadXLS.clsSEQF_Product> lstCOEnt = new List<ReadXLS.clsSEQF_Product>();
         
            try
            {
                lstCOEnt = XLS2XML_Deserializer(strReadXMLFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return lstCOEnt;
        }   

    }
}
