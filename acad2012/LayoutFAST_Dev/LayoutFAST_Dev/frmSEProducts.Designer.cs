﻿namespace testplugin
{
    partial class frmSEProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSEProducts));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.combo_LVTFavoritePanel = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.listBox_LVTransformer = new System.Windows.Forms.ListBox();
            this.label16 = new System.Windows.Forms.Label();
            this.combo_Transformer = new System.Windows.Forms.ComboBox();
            this.btnLVTransformer_bottomIncoming = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.exp_SpecificQuestions_LVTransformer = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.textbox_LVTPanelName = new System.Windows.Forms.TextBox();
            this.btnLVTRemove = new System.Windows.Forms.Button();
            this.btnLVTClearAnswers = new System.Windows.Forms.Button();
            this.btnLVTSave = new System.Windows.Forms.Button();
            this.btnLVTDuplicate = new System.Windows.Forms.Button();
            this.txtLVDepth = new System.Windows.Forms.TextBox();
            this.txtLVWidth = new System.Windows.Forms.TextBox();
            this.txtLVHeight = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLVDigest = new System.Windows.Forms.Button();
            this.btnLVRevit = new System.Windows.Forms.Button();
            this.btnLVSpecifications = new System.Windows.Forms.Button();
            this.btnLVCatalog = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tutorialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(712, 578);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(704, 549);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Transformers";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.combo_LVTFavoritePanel);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.listBox_LVTransformer);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.combo_Transformer);
            this.panel1.Controls.Add(this.btnLVTransformer_bottomIncoming);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.btnLVTRemove);
            this.panel1.Controls.Add(this.btnLVTClearAnswers);
            this.panel1.Controls.Add(this.btnLVTSave);
            this.panel1.Controls.Add(this.btnLVTDuplicate);
            this.panel1.Controls.Add(this.txtLVDepth);
            this.panel1.Controls.Add(this.txtLVWidth);
            this.panel1.Controls.Add(this.txtLVHeight);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnLVDigest);
            this.panel1.Controls.Add(this.btnLVRevit);
            this.panel1.Controls.Add(this.btnLVSpecifications);
            this.panel1.Controls.Add(this.btnLVCatalog);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(698, 543);
            this.panel1.TabIndex = 121;
            // 
            // combo_LVTFavoritePanel
            // 
            this.combo_LVTFavoritePanel.ForeColor = System.Drawing.Color.Black;
            this.combo_LVTFavoritePanel.FormattingEnabled = true;
            this.combo_LVTFavoritePanel.Location = new System.Drawing.Point(580, 28);
            this.combo_LVTFavoritePanel.Name = "combo_LVTFavoritePanel";
            this.combo_LVTFavoritePanel.Size = new System.Drawing.Size(115, 24);
            this.combo_LVTFavoritePanel.TabIndex = 82;
            this.combo_LVTFavoritePanel.Text = "Enter Name";
            this.combo_LVTFavoritePanel.SelectedIndexChanged += new System.EventHandler(this.combo_LVTFavoritePanel_SelectedIndexChanged);
            this.combo_LVTFavoritePanel.GotFocus += new System.EventHandler(this.combo_LVTFavoritePanel_GotFocus);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(580, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 17);
            this.label12.TabIndex = 81;
            this.label12.Text = "Favorite ";
            // 
            // listBox_LVTransformer
            // 
            this.listBox_LVTransformer.FormattingEnabled = true;
            this.listBox_LVTransformer.ItemHeight = 16;
            this.listBox_LVTransformer.Location = new System.Drawing.Point(344, 3);
            this.listBox_LVTransformer.Name = "listBox_LVTransformer";
            this.listBox_LVTransformer.Size = new System.Drawing.Size(122, 36);
            this.listBox_LVTransformer.TabIndex = 122;
            this.listBox_LVTransformer.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(296, 500);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(258, 17);
            this.label16.TabIndex = 166;
            this.label16.Text = "Existing Transformer Models in Project :";
            this.label16.Visible = false;
            // 
            // combo_Transformer
            // 
            this.combo_Transformer.FormattingEnabled = true;
            this.combo_Transformer.Location = new System.Drawing.Point(296, 520);
            this.combo_Transformer.Name = "combo_Transformer";
            this.combo_Transformer.Size = new System.Drawing.Size(276, 24);
            this.combo_Transformer.TabIndex = 165;
            this.combo_Transformer.Visible = false;
            this.combo_Transformer.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnLVTransformer_bottomIncoming
            // 
            this.btnLVTransformer_bottomIncoming.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power;
            this.btnLVTransformer_bottomIncoming.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLVTransformer_bottomIncoming.Location = new System.Drawing.Point(445, 452);
            this.btnLVTransformer_bottomIncoming.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVTransformer_bottomIncoming.Name = "btnLVTransformer_bottomIncoming";
            this.btnLVTransformer_bottomIncoming.Size = new System.Drawing.Size(31, 26);
            this.btnLVTransformer_bottomIncoming.TabIndex = 164;
            this.btnLVTransformer_bottomIncoming.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Controls.Add(this.panel6);
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(290, 543);
            this.panel7.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.exp_SpecificQuestions_LVTransformer);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 100);
            this.panel3.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(290, 443);
            this.panel3.TabIndex = 140;
            // 
            // exp_SpecificQuestions_LVTransformer
            // 
            this.exp_SpecificQuestions_LVTransformer.AutoScroll = true;
            this.exp_SpecificQuestions_LVTransformer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exp_SpecificQuestions_LVTransformer.ForeColor = System.Drawing.Color.White;
            this.exp_SpecificQuestions_LVTransformer.Location = new System.Drawing.Point(0, 0);
            this.exp_SpecificQuestions_LVTransformer.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.exp_SpecificQuestions_LVTransformer.Name = "exp_SpecificQuestions_LVTransformer";
            this.exp_SpecificQuestions_LVTransformer.Size = new System.Drawing.Size(290, 443);
            this.exp_SpecificQuestions_LVTransformer.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.textbox_LVTPanelName);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(290, 100);
            this.panel6.TabIndex = 164;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(20, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 17);
            this.label13.TabIndex = 80;
            this.label13.Text = "Transformer Name";
            // 
            // textbox_LVTPanelName
            // 
            this.textbox_LVTPanelName.Location = new System.Drawing.Point(20, 32);
            this.textbox_LVTPanelName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textbox_LVTPanelName.Name = "textbox_LVTPanelName";
            this.textbox_LVTPanelName.Size = new System.Drawing.Size(210, 22);
            this.textbox_LVTPanelName.TabIndex = 79;
            this.textbox_LVTPanelName.Text = "Transformer Name";
            // 
            // btnLVTRemove
            // 
            this.btnLVTRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVTRemove.ForeColor = System.Drawing.Color.Black;
            this.btnLVTRemove.Location = new System.Drawing.Point(580, 106);
            this.btnLVTRemove.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVTRemove.Name = "btnLVTRemove";
            this.btnLVTRemove.Size = new System.Drawing.Size(117, 31);
            this.btnLVTRemove.TabIndex = 163;
            this.btnLVTRemove.Text = "Remove";
            this.btnLVTRemove.UseVisualStyleBackColor = true;
            this.btnLVTRemove.Click += new System.EventHandler(this.btnLVTRemove_Click);
            // 
            // btnLVTClearAnswers
            // 
            this.btnLVTClearAnswers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVTClearAnswers.ForeColor = System.Drawing.Color.Black;
            this.btnLVTClearAnswers.Location = new System.Drawing.Point(580, 145);
            this.btnLVTClearAnswers.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVTClearAnswers.Name = "btnLVTClearAnswers";
            this.btnLVTClearAnswers.Size = new System.Drawing.Size(117, 31);
            this.btnLVTClearAnswers.TabIndex = 162;
            this.btnLVTClearAnswers.Text = "New";
            this.btnLVTClearAnswers.UseVisualStyleBackColor = true;
            this.btnLVTClearAnswers.Click += new System.EventHandler(this.btnLVTClearAnswers_Click);
            // 
            // btnLVTSave
            // 
            this.btnLVTSave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVTSave.ForeColor = System.Drawing.Color.Black;
            this.btnLVTSave.Location = new System.Drawing.Point(580, 69);
            this.btnLVTSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVTSave.Name = "btnLVTSave";
            this.btnLVTSave.Size = new System.Drawing.Size(117, 31);
            this.btnLVTSave.TabIndex = 161;
            this.btnLVTSave.Text = "Save";
            this.btnLVTSave.UseVisualStyleBackColor = true;
            this.btnLVTSave.Click += new System.EventHandler(this.btnLVTSave_Click);
            // 
            // btnLVTDuplicate
            // 
            this.btnLVTDuplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVTDuplicate.ForeColor = System.Drawing.Color.Black;
            this.btnLVTDuplicate.Location = new System.Drawing.Point(580, 184);
            this.btnLVTDuplicate.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVTDuplicate.Name = "btnLVTDuplicate";
            this.btnLVTDuplicate.Size = new System.Drawing.Size(117, 31);
            this.btnLVTDuplicate.TabIndex = 160;
            this.btnLVTDuplicate.Text = "Duplicate";
            this.btnLVTDuplicate.UseVisualStyleBackColor = true;
            this.btnLVTDuplicate.Click += new System.EventHandler(this.btnLVTDuplicate_Click);
            // 
            // txtLVDepth
            // 
            this.txtLVDepth.Location = new System.Drawing.Point(296, 189);
            this.txtLVDepth.Name = "txtLVDepth";
            this.txtLVDepth.ReadOnly = true;
            this.txtLVDepth.Size = new System.Drawing.Size(42, 22);
            this.txtLVDepth.TabIndex = 151;
            // 
            // txtLVWidth
            // 
            this.txtLVWidth.Location = new System.Drawing.Point(296, 83);
            this.txtLVWidth.Name = "txtLVWidth";
            this.txtLVWidth.ReadOnly = true;
            this.txtLVWidth.Size = new System.Drawing.Size(42, 22);
            this.txtLVWidth.TabIndex = 150;
            // 
            // txtLVHeight
            // 
            this.txtLVHeight.Location = new System.Drawing.Point(296, 136);
            this.txtLVHeight.Name = "txtLVHeight";
            this.txtLVHeight.ReadOnly = true;
            this.txtLVHeight.Size = new System.Drawing.Size(42, 22);
            this.txtLVHeight.TabIndex = 149;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(296, 169);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 143;
            this.label4.Text = "Depth";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(296, 63);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 142;
            this.label5.Text = "Width";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(296, 116);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 17);
            this.label6.TabIndex = 141;
            this.label6.Text = "Height";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLVDigest
            // 
            this.btnLVDigest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVDigest.ForeColor = System.Drawing.Color.Black;
            this.btnLVDigest.Location = new System.Drawing.Point(580, 327);
            this.btnLVDigest.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVDigest.Name = "btnLVDigest";
            this.btnLVDigest.Size = new System.Drawing.Size(117, 31);
            this.btnLVDigest.TabIndex = 139;
            this.btnLVDigest.Text = "Digest";
            this.btnLVDigest.UseVisualStyleBackColor = true;
            this.btnLVDigest.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnLVRevit
            // 
            this.btnLVRevit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVRevit.ForeColor = System.Drawing.Color.Black;
            this.btnLVRevit.Location = new System.Drawing.Point(580, 390);
            this.btnLVRevit.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVRevit.Name = "btnLVRevit";
            this.btnLVRevit.Size = new System.Drawing.Size(117, 53);
            this.btnLVRevit.TabIndex = 125;
            this.btnLVRevit.Text = "Insert Family";
            this.btnLVRevit.UseVisualStyleBackColor = true;
            this.btnLVRevit.Click += new System.EventHandler(this.btnLVRevit_Click);
            // 
            // btnLVSpecifications
            // 
            this.btnLVSpecifications.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVSpecifications.ForeColor = System.Drawing.Color.Black;
            this.btnLVSpecifications.Location = new System.Drawing.Point(580, 287);
            this.btnLVSpecifications.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVSpecifications.Name = "btnLVSpecifications";
            this.btnLVSpecifications.Size = new System.Drawing.Size(117, 31);
            this.btnLVSpecifications.TabIndex = 126;
            this.btnLVSpecifications.Text = "Specification";
            this.btnLVSpecifications.UseVisualStyleBackColor = true;
            this.btnLVSpecifications.Click += new System.EventHandler(this.btnLVSpecifications_Click);
            // 
            // btnLVCatalog
            // 
            this.btnLVCatalog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLVCatalog.ForeColor = System.Drawing.Color.Black;
            this.btnLVCatalog.Location = new System.Drawing.Point(580, 250);
            this.btnLVCatalog.Margin = new System.Windows.Forms.Padding(4);
            this.btnLVCatalog.Name = "btnLVCatalog";
            this.btnLVCatalog.Size = new System.Drawing.Size(117, 31);
            this.btnLVCatalog.TabIndex = 138;
            this.btnLVCatalog.Text = "Catalog";
            this.btnLVCatalog.UseVisualStyleBackColor = true;
            this.btnLVCatalog.Click += new System.EventHandler(this.btnLVCatalog_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Silver;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(348, 63);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(224, 380);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 122;
            this.pictureBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(698, 543);
            this.panel2.TabIndex = 120;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.tutorialToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(712, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "File";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem4,
            this.toolStripMenuItem2});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(44, 24);
            this.toolStripMenuItem3.Text = "File";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(119, 24);
            this.toolStripMenuItem1.Text = "About";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(119, 24);
            this.toolStripMenuItem4.Text = "Help";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(119, 24);
            this.toolStripMenuItem2.Text = "Close";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // tutorialToolStripMenuItem
            // 
            this.tutorialToolStripMenuItem.Name = "tutorialToolStripMenuItem";
            this.tutorialToolStripMenuItem.Size = new System.Drawing.Size(76, 24);
            this.tutorialToolStripMenuItem.Text = " Tutorial";
            this.tutorialToolStripMenuItem.Click += new System.EventHandler(this.tutorialToolStripMenuItem_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Image = global::LayoutFAST_Dev.Properties.Resources.Banner_LayoutFAST;
            this.pictureBox3.Location = new System.Drawing.Point(0, 606);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(712, 176);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // frmSEProducts
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(712, 771);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSEProducts";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LayoutFAST";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSEProducts_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel exp_SpecificQuestions_LVTransformer;
        internal System.Windows.Forms.Button btnLVDigest;
        internal System.Windows.Forms.Button btnLVRevit;
        internal System.Windows.Forms.Button btnLVSpecifications;
        internal System.Windows.Forms.Button btnLVCatalog;
        internal System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtLVDepth;
        private System.Windows.Forms.TextBox txtLVWidth;
        private System.Windows.Forms.TextBox txtLVHeight;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox combo_LVTFavoritePanel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        internal System.Windows.Forms.TextBox textbox_LVTPanelName;
        internal System.Windows.Forms.Button btnLVTRemove;
        internal System.Windows.Forms.Button btnLVTClearAnswers;
        internal System.Windows.Forms.Button btnLVTSave;
        internal System.Windows.Forms.Button btnLVTDuplicate;
        private System.Windows.Forms.Button btnLVTransformer_bottomIncoming;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox combo_Transformer;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem tutorialToolStripMenuItem;
        private System.Windows.Forms.ListBox listBox_LVTransformer;
    }
}