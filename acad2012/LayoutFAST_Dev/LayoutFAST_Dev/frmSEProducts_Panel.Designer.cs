﻿namespace LayoutFAST_Panelboards
{
    partial class frmSEProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSEProducts));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tutorialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.combo_ILineFavoritePanel = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.combo_Iline = new System.Windows.Forms.ComboBox();
            this.listBox_LVTransformer = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnILineSave = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.exp_SpecificQuestions_ILine = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.txtIlinePanelName = new System.Windows.Forms.TextBox();
            this.txtILineDepth = new System.Windows.Forms.TextBox();
            this.txtIlineWidth = new System.Windows.Forms.TextBox();
            this.txtILineHeight = new System.Windows.Forms.TextBox();
            this.btnIlineInsert = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnILineCatalog = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.btnILineIncomingTop = new System.Windows.Forms.Button();
            this.btnILineIncomingBottom = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.combo_FavoritePanel = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btn3D_Interactive = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.combo_NQ = new System.Windows.Forms.ComboBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.exp_SpecificQuestions_Enclosure = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPanelName = new System.Windows.Forms.TextBox();
            this.btnClearContent = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDuplicate = new System.Windows.Forms.Button();
            this.txtDepth = new System.Windows.Forms.TextBox();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.btnPnlIncomingTop = new System.Windows.Forms.Button();
            this.btnPnlIncomingBottom = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnInstallManual = new System.Windows.Forms.Button();
            this.btnRevit = new System.Windows.Forms.Button();
            this.btnQuoteFast = new System.Windows.Forms.Button();
            this.btn3DModel = new System.Windows.Forms.Button();
            this.btnDigestPlus = new System.Windows.Forms.Button();
            this.btnSpecifications = new System.Windows.Forms.Button();
            this.btnCatalog = new System.Windows.Forms.Button();
            this.dgv_PartsList = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listBox_Interior = new System.Windows.Forms.ListBox();
            this.listBox_Box = new System.Windows.Forms.ListBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.listBox_Enclosure = new System.Windows.Forms.ListBox();
            this.exp_SpecificQuestions_Interior = new System.Windows.Forms.Panel();
            this.panel_Interior = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_PartsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.exp_SpecificQuestions_Interior.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.tutorialToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(712, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "File";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem4,
            this.toolStripMenuItem2});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(44, 24);
            this.toolStripMenuItem3.Text = "File";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(119, 24);
            this.toolStripMenuItem1.Text = "About";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(119, 24);
            this.toolStripMenuItem4.Text = "Help";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(119, 24);
            this.toolStripMenuItem2.Text = "Close";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // tutorialToolStripMenuItem
            // 
            this.tutorialToolStripMenuItem.Name = "tutorialToolStripMenuItem";
            this.tutorialToolStripMenuItem.Size = new System.Drawing.Size(76, 24);
            this.tutorialToolStripMenuItem.Text = " Tutorial";
            this.tutorialToolStripMenuItem.Click += new System.EventHandler(this.tutorialToolStripMenuItem_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Image = global::LayoutFAST_Dev.Properties.Resources.Banner_LayoutFAST;
            this.pictureBox3.Location = new System.Drawing.Point(0, 606);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(712, 176);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.combo_ILineFavoritePanel);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.combo_Iline);
            this.tabPage2.Controls.Add(this.listBox_LVTransformer);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.btnILineSave);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.txtILineDepth);
            this.tabPage2.Controls.Add(this.txtIlineWidth);
            this.tabPage2.Controls.Add(this.txtILineHeight);
            this.tabPage2.Controls.Add(this.btnIlineInsert);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.btnILineCatalog);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.btnILineIncomingTop);
            this.tabPage2.Controls.Add(this.btnILineIncomingBottom);
            this.tabPage2.Controls.Add(this.pictureBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(704, 549);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Distribution Panelboard";
            // 
            // combo_ILineFavoritePanel
            // 
            this.combo_ILineFavoritePanel.ForeColor = System.Drawing.Color.Black;
            this.combo_ILineFavoritePanel.FormattingEnabled = true;
            this.combo_ILineFavoritePanel.Location = new System.Drawing.Point(580, 28);
            this.combo_ILineFavoritePanel.Name = "combo_ILineFavoritePanel";
            this.combo_ILineFavoritePanel.Size = new System.Drawing.Size(117, 24);
            this.combo_ILineFavoritePanel.TabIndex = 82;
            this.combo_ILineFavoritePanel.Text = "Enter Name";
            this.combo_ILineFavoritePanel.SelectedIndexChanged += new System.EventHandler(this.combo_ILineFavoritePanel_SelectedIndexChanged);
            this.combo_ILineFavoritePanel.GotFocus += new System.EventHandler(this.combo_ILineFavoritePanel_GotFocus);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(296, 500);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(328, 17);
            this.label17.TabIndex = 174;
            this.label17.Text = "Existing Distribution Panelboard Models in Project :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(580, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 17);
            this.label14.TabIndex = 81;
            this.label14.Text = "Favorite";
            // 
            // combo_Iline
            // 
            this.combo_Iline.FormattingEnabled = true;
            this.combo_Iline.Location = new System.Drawing.Point(296, 520);
            this.combo_Iline.Name = "combo_Iline";
            this.combo_Iline.Size = new System.Drawing.Size(276, 24);
            this.combo_Iline.TabIndex = 173;
            // 
            // listBox_LVTransformer
            // 
            this.listBox_LVTransformer.FormattingEnabled = true;
            this.listBox_LVTransformer.ItemHeight = 16;
            this.listBox_LVTransformer.Location = new System.Drawing.Point(267, 16);
            this.listBox_LVTransformer.Name = "listBox_LVTransformer";
            this.listBox_LVTransformer.Size = new System.Drawing.Size(84, 4);
            this.listBox_LVTransformer.TabIndex = 172;
            this.listBox_LVTransformer.Visible = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(580, 106);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 31);
            this.button1.TabIndex = 171;
            this.button1.Text = "Remove";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(580, 145);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 31);
            this.button2.TabIndex = 170;
            this.button2.Text = "New";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btnILineSave
            // 
            this.btnILineSave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnILineSave.Location = new System.Drawing.Point(580, 69);
            this.btnILineSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnILineSave.Name = "btnILineSave";
            this.btnILineSave.Size = new System.Drawing.Size(117, 31);
            this.btnILineSave.TabIndex = 169;
            this.btnILineSave.Text = "Save";
            this.btnILineSave.UseVisualStyleBackColor = true;
            this.btnILineSave.Click += new System.EventHandler(this.btnILineSave_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Location = new System.Drawing.Point(580, 184);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(117, 31);
            this.button5.TabIndex = 168;
            this.button5.Text = "Duplicate";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.exp_SpecificQuestions_ILine);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(290, 549);
            this.panel8.TabIndex = 166;
            // 
            // exp_SpecificQuestions_ILine
            // 
            this.exp_SpecificQuestions_ILine.AutoScroll = true;
            this.exp_SpecificQuestions_ILine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exp_SpecificQuestions_ILine.ForeColor = System.Drawing.Color.White;
            this.exp_SpecificQuestions_ILine.Location = new System.Drawing.Point(0, 66);
            this.exp_SpecificQuestions_ILine.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.exp_SpecificQuestions_ILine.Name = "exp_SpecificQuestions_ILine";
            this.exp_SpecificQuestions_ILine.Size = new System.Drawing.Size(290, 483);
            this.exp_SpecificQuestions_ILine.TabIndex = 148;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.txtIlinePanelName);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(290, 66);
            this.panel9.TabIndex = 149;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(20, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(160, 17);
            this.label15.TabIndex = 80;
            this.label15.Text = "Distribution Panel Name";
            // 
            // txtIlinePanelName
            // 
            this.txtIlinePanelName.Location = new System.Drawing.Point(20, 32);
            this.txtIlinePanelName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIlinePanelName.Name = "txtIlinePanelName";
            this.txtIlinePanelName.Size = new System.Drawing.Size(210, 22);
            this.txtIlinePanelName.TabIndex = 79;
            this.txtIlinePanelName.Text = "Panel Name";
            // 
            // txtILineDepth
            // 
            this.txtILineDepth.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtILineDepth.Location = new System.Drawing.Point(296, 189);
            this.txtILineDepth.Name = "txtILineDepth";
            this.txtILineDepth.ReadOnly = true;
            this.txtILineDepth.Size = new System.Drawing.Size(42, 22);
            this.txtILineDepth.TabIndex = 165;
            // 
            // txtIlineWidth
            // 
            this.txtIlineWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIlineWidth.Location = new System.Drawing.Point(296, 83);
            this.txtIlineWidth.Name = "txtIlineWidth";
            this.txtIlineWidth.ReadOnly = true;
            this.txtIlineWidth.Size = new System.Drawing.Size(42, 22);
            this.txtIlineWidth.TabIndex = 164;
            // 
            // txtILineHeight
            // 
            this.txtILineHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtILineHeight.Location = new System.Drawing.Point(296, 136);
            this.txtILineHeight.Name = "txtILineHeight";
            this.txtILineHeight.ReadOnly = true;
            this.txtILineHeight.Size = new System.Drawing.Size(42, 22);
            this.txtILineHeight.TabIndex = 163;
            // 
            // btnIlineInsert
            // 
            this.btnIlineInsert.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIlineInsert.Location = new System.Drawing.Point(580, 390);
            this.btnIlineInsert.Margin = new System.Windows.Forms.Padding(4);
            this.btnIlineInsert.Name = "btnIlineInsert";
            this.btnIlineInsert.Size = new System.Drawing.Size(117, 53);
            this.btnIlineInsert.TabIndex = 154;
            this.btnIlineInsert.Text = "Insert Family";
            this.btnIlineInsert.UseVisualStyleBackColor = true;
            this.btnIlineInsert.Click += new System.EventHandler(this.btnIlineInsert_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(580, 327);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(117, 31);
            this.button4.TabIndex = 157;
            this.button4.Text = "Digest";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnILineCatalog
            // 
            this.btnILineCatalog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnILineCatalog.Location = new System.Drawing.Point(580, 250);
            this.btnILineCatalog.Margin = new System.Windows.Forms.Padding(4);
            this.btnILineCatalog.Name = "btnILineCatalog";
            this.btnILineCatalog.Size = new System.Drawing.Size(117, 31);
            this.btnILineCatalog.TabIndex = 155;
            this.btnILineCatalog.Text = "Catalog";
            this.btnILineCatalog.UseVisualStyleBackColor = true;
            this.btnILineCatalog.Click += new System.EventHandler(this.btnILineCatalog_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(296, 169);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 160;
            this.label7.Text = "Depth";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(296, 63);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 17);
            this.label8.TabIndex = 159;
            this.label8.Text = "Width";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(296, 116);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 17);
            this.label9.TabIndex = 158;
            this.label9.Text = "Height";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Location = new System.Drawing.Point(580, 287);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(117, 31);
            this.button6.TabIndex = 156;
            this.button6.Text = "Specification";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnILineIncomingTop
            // 
            this.btnILineIncomingTop.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power;
            this.btnILineIncomingTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnILineIncomingTop.Location = new System.Drawing.Point(445, 28);
            this.btnILineIncomingTop.Margin = new System.Windows.Forms.Padding(4);
            this.btnILineIncomingTop.Name = "btnILineIncomingTop";
            this.btnILineIncomingTop.Size = new System.Drawing.Size(31, 26);
            this.btnILineIncomingTop.TabIndex = 162;
            this.btnILineIncomingTop.UseVisualStyleBackColor = true;
            this.btnILineIncomingTop.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // btnILineIncomingBottom
            // 
            this.btnILineIncomingBottom.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power_off;
            this.btnILineIncomingBottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnILineIncomingBottom.Location = new System.Drawing.Point(445, 452);
            this.btnILineIncomingBottom.Margin = new System.Windows.Forms.Padding(4);
            this.btnILineIncomingBottom.Name = "btnILineIncomingBottom";
            this.btnILineIncomingBottom.Size = new System.Drawing.Size(31, 26);
            this.btnILineIncomingBottom.TabIndex = 161;
            this.btnILineIncomingBottom.UseVisualStyleBackColor = true;
            this.btnILineIncomingBottom.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.BackColor = System.Drawing.Color.Silver;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(348, 63);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(224, 380);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 149;
            this.pictureBox4.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.combo_FavoritePanel);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.btn3D_Interactive);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.combo_NQ);
            this.tabPage3.Controls.Add(this.btnRemove);
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Controls.Add(this.btnClearContent);
            this.tabPage3.Controls.Add(this.btnSave);
            this.tabPage3.Controls.Add(this.btnDuplicate);
            this.tabPage3.Controls.Add(this.txtDepth);
            this.tabPage3.Controls.Add(this.txtWidth);
            this.tabPage3.Controls.Add(this.txtHeight);
            this.tabPage3.Controls.Add(this.btnPnlIncomingTop);
            this.tabPage3.Controls.Add(this.btnPnlIncomingBottom);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.btnInstallManual);
            this.tabPage3.Controls.Add(this.btnRevit);
            this.tabPage3.Controls.Add(this.btnQuoteFast);
            this.tabPage3.Controls.Add(this.btn3DModel);
            this.tabPage3.Controls.Add(this.btnDigestPlus);
            this.tabPage3.Controls.Add(this.btnSpecifications);
            this.tabPage3.Controls.Add(this.btnCatalog);
            this.tabPage3.Controls.Add(this.dgv_PartsList);
            this.tabPage3.Controls.Add(this.listBox_Interior);
            this.tabPage3.Controls.Add(this.listBox_Box);
            this.tabPage3.Controls.Add(this.PictureBox1);
            this.tabPage3.Controls.Add(this.listBox_Enclosure);
            this.tabPage3.Controls.Add(this.exp_SpecificQuestions_Interior);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(704, 549);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Panelboards";
            // 
            // combo_FavoritePanel
            // 
            this.combo_FavoritePanel.AllowDrop = true;
            this.combo_FavoritePanel.ForeColor = System.Drawing.Color.Black;
            this.combo_FavoritePanel.FormattingEnabled = true;
            this.combo_FavoritePanel.Location = new System.Drawing.Point(580, 28);
            this.combo_FavoritePanel.Name = "combo_FavoritePanel";
            this.combo_FavoritePanel.Size = new System.Drawing.Size(117, 24);
            this.combo_FavoritePanel.TabIndex = 82;
            this.combo_FavoritePanel.Text = "Enter Name";
            this.combo_FavoritePanel.SelectedIndexChanged += new System.EventHandler(this.combo_FavoritePanel_SelectedIndexChanged);
            this.combo_FavoritePanel.GotFocus += new System.EventHandler(this.combo_FavoritePanel_GotFocus);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(580, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 17);
            this.label11.TabIndex = 81;
            this.label11.Text = "Favorite";
            // 
            // btn3D_Interactive
            // 
            this.btn3D_Interactive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn3D_Interactive.Location = new System.Drawing.Point(580, 238);
            this.btn3D_Interactive.Margin = new System.Windows.Forms.Padding(4);
            this.btn3D_Interactive.Name = "btn3D_Interactive";
            this.btn3D_Interactive.Size = new System.Drawing.Size(117, 31);
            this.btn3D_Interactive.TabIndex = 178;
            this.btn3D_Interactive.Text = "3D Interactive";
            this.btn3D_Interactive.UseVisualStyleBackColor = true;
            this.btn3D_Interactive.Click += new System.EventHandler(this.btn3D_Interactive_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(296, 500);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(253, 17);
            this.label18.TabIndex = 176;
            this.label18.Text = "Existing Panelboard Models in Project :";
            // 
            // combo_NQ
            // 
            this.combo_NQ.FormattingEnabled = true;
            this.combo_NQ.Location = new System.Drawing.Point(296, 520);
            this.combo_NQ.Name = "combo_NQ";
            this.combo_NQ.Size = new System.Drawing.Size(276, 24);
            this.combo_NQ.TabIndex = 175;
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.Location = new System.Drawing.Point(580, 106);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(4);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(117, 31);
            this.btnRemove.TabIndex = 159;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.exp_SpecificQuestions_Enclosure);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(290, 549);
            this.panel5.TabIndex = 158;
            // 
            // exp_SpecificQuestions_Enclosure
            // 
            this.exp_SpecificQuestions_Enclosure.AutoScroll = true;
            this.exp_SpecificQuestions_Enclosure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exp_SpecificQuestions_Enclosure.ForeColor = System.Drawing.Color.White;
            this.exp_SpecificQuestions_Enclosure.Location = new System.Drawing.Point(0, 66);
            this.exp_SpecificQuestions_Enclosure.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.exp_SpecificQuestions_Enclosure.Name = "exp_SpecificQuestions_Enclosure";
            this.exp_SpecificQuestions_Enclosure.Size = new System.Drawing.Size(290, 483);
            this.exp_SpecificQuestions_Enclosure.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.txtPanelName);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(290, 66);
            this.panel4.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 17);
            this.label10.TabIndex = 80;
            this.label10.Text = "Panelboard Name";
            // 
            // txtPanelName
            // 
            this.txtPanelName.Location = new System.Drawing.Point(20, 32);
            this.txtPanelName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPanelName.Name = "txtPanelName";
            this.txtPanelName.Size = new System.Drawing.Size(210, 22);
            this.txtPanelName.TabIndex = 79;
            this.txtPanelName.Text = "Panel Name";
            // 
            // btnClearContent
            // 
            this.btnClearContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearContent.Location = new System.Drawing.Point(580, 145);
            this.btnClearContent.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearContent.Name = "btnClearContent";
            this.btnClearContent.Size = new System.Drawing.Size(117, 31);
            this.btnClearContent.TabIndex = 157;
            this.btnClearContent.Text = "New";
            this.btnClearContent.UseVisualStyleBackColor = true;
            this.btnClearContent.Click += new System.EventHandler(this.btnClearContent_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(580, 69);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 31);
            this.btnSave.TabIndex = 156;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDuplicate
            // 
            this.btnDuplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDuplicate.Location = new System.Drawing.Point(580, 184);
            this.btnDuplicate.Margin = new System.Windows.Forms.Padding(4);
            this.btnDuplicate.Name = "btnDuplicate";
            this.btnDuplicate.Size = new System.Drawing.Size(117, 31);
            this.btnDuplicate.TabIndex = 154;
            this.btnDuplicate.Text = "Duplicate";
            this.btnDuplicate.UseVisualStyleBackColor = true;
            this.btnDuplicate.Click += new System.EventHandler(this.btnDuplicate_Click);
            // 
            // txtDepth
            // 
            this.txtDepth.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDepth.Location = new System.Drawing.Point(296, 189);
            this.txtDepth.Name = "txtDepth";
            this.txtDepth.ReadOnly = true;
            this.txtDepth.Size = new System.Drawing.Size(42, 22);
            this.txtDepth.TabIndex = 150;
            // 
            // txtWidth
            // 
            this.txtWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWidth.Location = new System.Drawing.Point(296, 83);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.ReadOnly = true;
            this.txtWidth.Size = new System.Drawing.Size(42, 22);
            this.txtWidth.TabIndex = 149;
            // 
            // txtHeight
            // 
            this.txtHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHeight.Location = new System.Drawing.Point(296, 136);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.ReadOnly = true;
            this.txtHeight.Size = new System.Drawing.Size(42, 22);
            this.txtHeight.TabIndex = 148;
            // 
            // btnPnlIncomingTop
            // 
            this.btnPnlIncomingTop.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power;
            this.btnPnlIncomingTop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPnlIncomingTop.Location = new System.Drawing.Point(445, 28);
            this.btnPnlIncomingTop.Margin = new System.Windows.Forms.Padding(4);
            this.btnPnlIncomingTop.Name = "btnPnlIncomingTop";
            this.btnPnlIncomingTop.Size = new System.Drawing.Size(31, 26);
            this.btnPnlIncomingTop.TabIndex = 147;
            this.btnPnlIncomingTop.UseVisualStyleBackColor = true;
            this.btnPnlIncomingTop.Click += new System.EventHandler(this.btnPnlIncomingTop_Click);
            // 
            // btnPnlIncomingBottom
            // 
            this.btnPnlIncomingBottom.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power_off;
            this.btnPnlIncomingBottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPnlIncomingBottom.Location = new System.Drawing.Point(445, 452);
            this.btnPnlIncomingBottom.Margin = new System.Windows.Forms.Padding(4);
            this.btnPnlIncomingBottom.Name = "btnPnlIncomingBottom";
            this.btnPnlIncomingBottom.Size = new System.Drawing.Size(31, 26);
            this.btnPnlIncomingBottom.TabIndex = 146;
            this.btnPnlIncomingBottom.UseVisualStyleBackColor = true;
            this.btnPnlIncomingBottom.Click += new System.EventHandler(this.btnPnlIncomingBottom_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(296, 169);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 122;
            this.label3.Text = "Depth";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(296, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 121;
            this.label2.Text = "Width";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(296, 116);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 120;
            this.label1.Text = "Height";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnInstallManual
            // 
            this.btnInstallManual.Location = new System.Drawing.Point(670, 597);
            this.btnInstallManual.Margin = new System.Windows.Forms.Padding(4);
            this.btnInstallManual.Name = "btnInstallManual";
            this.btnInstallManual.Size = new System.Drawing.Size(136, 31);
            this.btnInstallManual.TabIndex = 119;
            this.btnInstallManual.Text = "Installation Manual";
            this.btnInstallManual.UseVisualStyleBackColor = true;
            this.btnInstallManual.Visible = false;
            this.btnInstallManual.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRevit
            // 
            this.btnRevit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRevit.Location = new System.Drawing.Point(580, 390);
            this.btnRevit.Margin = new System.Windows.Forms.Padding(4);
            this.btnRevit.Name = "btnRevit";
            this.btnRevit.Size = new System.Drawing.Size(117, 53);
            this.btnRevit.TabIndex = 113;
            this.btnRevit.Text = "Insert Family";
            this.btnRevit.UseVisualStyleBackColor = true;
            this.btnRevit.Click += new System.EventHandler(this.btnRevit_Click);
            // 
            // btnQuoteFast
            // 
            this.btnQuoteFast.Location = new System.Drawing.Point(634, 605);
            this.btnQuoteFast.Margin = new System.Windows.Forms.Padding(4);
            this.btnQuoteFast.Name = "btnQuoteFast";
            this.btnQuoteFast.Size = new System.Drawing.Size(149, 31);
            this.btnQuoteFast.TabIndex = 112;
            this.btnQuoteFast.Text = "Send to QuoteFast";
            this.btnQuoteFast.UseVisualStyleBackColor = true;
            this.btnQuoteFast.Visible = false;
            // 
            // btn3DModel
            // 
            this.btn3DModel.Location = new System.Drawing.Point(676, 636);
            this.btn3DModel.Margin = new System.Windows.Forms.Padding(4);
            this.btn3DModel.Name = "btn3DModel";
            this.btn3DModel.Size = new System.Drawing.Size(99, 31);
            this.btn3DModel.TabIndex = 117;
            this.btn3DModel.Text = "3D Model";
            this.btn3DModel.UseVisualStyleBackColor = true;
            this.btn3DModel.Visible = false;
            this.btn3DModel.Click += new System.EventHandler(this.Button12_Click);
            // 
            // btnDigestPlus
            // 
            this.btnDigestPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDigestPlus.Location = new System.Drawing.Point(580, 346);
            this.btnDigestPlus.Margin = new System.Windows.Forms.Padding(4);
            this.btnDigestPlus.Name = "btnDigestPlus";
            this.btnDigestPlus.Size = new System.Drawing.Size(117, 31);
            this.btnDigestPlus.TabIndex = 116;
            this.btnDigestPlus.Text = "Digest";
            this.btnDigestPlus.UseVisualStyleBackColor = true;
            this.btnDigestPlus.Click += new System.EventHandler(this.Button11_Click);
            // 
            // btnSpecifications
            // 
            this.btnSpecifications.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSpecifications.Location = new System.Drawing.Point(580, 310);
            this.btnSpecifications.Margin = new System.Windows.Forms.Padding(4);
            this.btnSpecifications.Name = "btnSpecifications";
            this.btnSpecifications.Size = new System.Drawing.Size(117, 31);
            this.btnSpecifications.TabIndex = 115;
            this.btnSpecifications.Text = "Specification";
            this.btnSpecifications.UseVisualStyleBackColor = true;
            this.btnSpecifications.Click += new System.EventHandler(this.Button10_Click);
            // 
            // btnCatalog
            // 
            this.btnCatalog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCatalog.Location = new System.Drawing.Point(580, 274);
            this.btnCatalog.Margin = new System.Windows.Forms.Padding(4);
            this.btnCatalog.Name = "btnCatalog";
            this.btnCatalog.Size = new System.Drawing.Size(117, 31);
            this.btnCatalog.TabIndex = 114;
            this.btnCatalog.Text = "Catalog";
            this.btnCatalog.UseVisualStyleBackColor = true;
            this.btnCatalog.Click += new System.EventHandler(this.Button9_Click);
            // 
            // dgv_PartsList
            // 
            this.dgv_PartsList.AllowUserToAddRows = false;
            this.dgv_PartsList.AllowUserToDeleteRows = false;
            this.dgv_PartsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_PartsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column6});
            this.dgv_PartsList.Location = new System.Drawing.Point(6, 669);
            this.dgv_PartsList.Name = "dgv_PartsList";
            this.dgv_PartsList.RowHeadersVisible = false;
            this.dgv_PartsList.RowTemplate.Height = 24;
            this.dgv_PartsList.Size = new System.Drawing.Size(725, 230);
            this.dgv_PartsList.TabIndex = 96;
            this.dgv_PartsList.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Description";
            this.Column1.Name = "Column1";
            this.Column1.Width = 240;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Catalog Number";
            this.Column2.Name = "Column2";
            this.Column2.Width = 120;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Quantity";
            this.Column3.Name = "Column3";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Unit Cost";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Total Cost";
            this.Column6.Name = "Column6";
            // 
            // listBox_Interior
            // 
            this.listBox_Interior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(155)))), ((int)(((byte)(62)))));
            this.listBox_Interior.FormattingEnabled = true;
            this.listBox_Interior.ItemHeight = 16;
            this.listBox_Interior.Location = new System.Drawing.Point(11, 817);
            this.listBox_Interior.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.listBox_Interior.Name = "listBox_Interior";
            this.listBox_Interior.Size = new System.Drawing.Size(186, 68);
            this.listBox_Interior.Sorted = true;
            this.listBox_Interior.TabIndex = 118;
            this.listBox_Interior.Visible = false;
            // 
            // listBox_Box
            // 
            this.listBox_Box.FormattingEnabled = true;
            this.listBox_Box.ItemHeight = 16;
            this.listBox_Box.Location = new System.Drawing.Point(206, 815);
            this.listBox_Box.Name = "listBox_Box";
            this.listBox_Box.Size = new System.Drawing.Size(188, 68);
            this.listBox_Box.TabIndex = 5;
            this.listBox_Box.Visible = false;
            // 
            // PictureBox1
            // 
            this.PictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBox1.BackColor = System.Drawing.Color.Silver;
            this.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox1.Image")));
            this.PictureBox1.Location = new System.Drawing.Point(348, 63);
            this.PictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(224, 380);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 6;
            this.PictureBox1.TabStop = false;
            // 
            // listBox_Enclosure
            // 
            this.listBox_Enclosure.FormattingEnabled = true;
            this.listBox_Enclosure.ItemHeight = 16;
            this.listBox_Enclosure.Location = new System.Drawing.Point(400, 819);
            this.listBox_Enclosure.Name = "listBox_Enclosure";
            this.listBox_Enclosure.Size = new System.Drawing.Size(120, 68);
            this.listBox_Enclosure.TabIndex = 4;
            this.listBox_Enclosure.Visible = false;
            // 
            // exp_SpecificQuestions_Interior
            // 
            this.exp_SpecificQuestions_Interior.AutoScroll = true;
            this.exp_SpecificQuestions_Interior.Controls.Add(this.panel_Interior);
            this.exp_SpecificQuestions_Interior.ForeColor = System.Drawing.Color.White;
            this.exp_SpecificQuestions_Interior.Location = new System.Drawing.Point(323, 737);
            this.exp_SpecificQuestions_Interior.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.exp_SpecificQuestions_Interior.Name = "exp_SpecificQuestions_Interior";
            this.exp_SpecificQuestions_Interior.Size = new System.Drawing.Size(252, 149);
            this.exp_SpecificQuestions_Interior.TabIndex = 3;
            this.exp_SpecificQuestions_Interior.Visible = false;
            // 
            // panel_Interior
            // 
            this.panel_Interior.AutoScroll = true;
            this.panel_Interior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Interior.ForeColor = System.Drawing.Color.White;
            this.panel_Interior.Location = new System.Drawing.Point(0, 0);
            this.panel_Interior.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panel_Interior.Name = "panel_Interior";
            this.panel_Interior.Size = new System.Drawing.Size(252, 149);
            this.panel_Interior.TabIndex = 2;
            this.panel_Interior.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 28);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(712, 578);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseClick);
            // 
            // frmSEProducts
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(712, 771);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSEProducts";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LayoutFAST";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSEProducts_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_PartsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.exp_SpecificQuestions_Interior.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem tutorialToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox combo_Iline;
        private System.Windows.Forms.ListBox listBox_LVTransformer;
        internal System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.Button btnILineSave;
        internal System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel exp_SpecificQuestions_ILine;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.ComboBox combo_ILineFavoritePanel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        internal System.Windows.Forms.TextBox txtIlinePanelName;
        private System.Windows.Forms.TextBox txtILineDepth;
        private System.Windows.Forms.TextBox txtIlineWidth;
        private System.Windows.Forms.TextBox txtILineHeight;
        internal System.Windows.Forms.Button btnIlineInsert;
        internal System.Windows.Forms.Button button4;
        internal System.Windows.Forms.Button btnILineCatalog;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnILineIncomingTop;
        private System.Windows.Forms.Button btnILineIncomingBottom;
        internal System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TabPage tabPage3;
        internal System.Windows.Forms.Button btn3D_Interactive;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox combo_NQ;
        internal System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel exp_SpecificQuestions_Enclosure;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox combo_FavoritePanel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        internal System.Windows.Forms.TextBox txtPanelName;
        internal System.Windows.Forms.Button btnClearContent;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnDuplicate;
        private System.Windows.Forms.TextBox txtDepth;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Button btnPnlIncomingTop;
        private System.Windows.Forms.Button btnPnlIncomingBottom;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Button btnInstallManual;
        internal System.Windows.Forms.Button btnRevit;
        internal System.Windows.Forms.Button btnQuoteFast;
        internal System.Windows.Forms.Button btn3DModel;
        internal System.Windows.Forms.Button btnDigestPlus;
        internal System.Windows.Forms.Button btnSpecifications;
        internal System.Windows.Forms.Button btnCatalog;
        private System.Windows.Forms.DataGridView dgv_PartsList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.ListBox listBox_Interior;
        private System.Windows.Forms.ListBox listBox_Box;
        internal System.Windows.Forms.PictureBox PictureBox1;
        private System.Windows.Forms.ListBox listBox_Enclosure;
        private System.Windows.Forms.Panel exp_SpecificQuestions_Interior;
        private System.Windows.Forms.Panel panel_Interior;
        private System.Windows.Forms.TabControl tabControl1;
    }
}