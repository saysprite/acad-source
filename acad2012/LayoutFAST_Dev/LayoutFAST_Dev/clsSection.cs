﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Switchboard_Selector
{
    class clsSection
    {
        public int iSectionNumber;
        public Double dblWidth;
        public Double dblDepth;
        public string strPartID;
        public string strFamily;
        public string strRFA;
        public string strCatType;
        public string strInstance;
        public string strShortDescription;
        public string strSectionAmps;
        public string strSectionType;
        public string combovalue;

        public clsSection  (int iSectionNumber, 
                            Double dblWidth,
                            Double dblDepth,
                            string strPartID,
                            string strFamily, 
                            string strRFA,
                            string strCatType,
                            string strInstance, string strShortDescription, string strSectionAmps, string strSectionType,string combovalue)
        {
            this.iSectionNumber=iSectionNumber;
            this.dblWidth=dblWidth;
            this.dblDepth = dblDepth;
            this.strPartID = strPartID;
            this.strFamily = strFamily;
            this.strRFA=strRFA;
            this.strCatType = strCatType;
            this.strInstance = strInstance;
            this.strShortDescription=strShortDescription;
            this.strSectionAmps = strSectionAmps;
            this.strSectionType = strSectionType;
            this.combovalue = combovalue;
        }

    }
}
