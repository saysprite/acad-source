﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gc_panelboards_r3
{
    class clsPanel
    {
        public string strSide;
        public int iPosition;
        public int iPoles;
        public string strTrip;
        public string id;

        public clsPanel(string strSide, int iPosition, int iPoles, string strTrip,string id)
        {
            this.strSide=strSide;
            this.iPosition =iPosition;
            this.iPoles = iPoles;
            this.strTrip=strTrip;
            this.id = id;
        }
    }
}
