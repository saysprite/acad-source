﻿#region Namespaces

using System;
using System.Text;
using System.Linq;
using System.Xml;
using System.Reflection;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using MgdAcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using MgdAcDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcWindowsNS = Autodesk.AutoCAD.Windows;

#endregion

namespace LayoutFAST_Dev
{
    public class clsLayoutFAST_Dev
    {
        [CommandMethod("CmdGroup1", "LayoutFAST_Dev", null, CommandFlags.Modal, null, "LayoutFAST_Dev", "Command1")]
        public void LayoutFAST_Dev_Method()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = MgdAcApplication.DocumentManager.MdiActiveDocument.Editor;
            
            try
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    //TODO: add your code below.    
                    frmMain fm = new frmMain();
                    fm.Show();

                    tr.Commit();
                }
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                ed.WriteMessage(ex.ToString());
            }
        }

    }
}
