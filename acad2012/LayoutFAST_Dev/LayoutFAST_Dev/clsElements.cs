﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Switchboard_Selector
{
    public class clsElements
    {
        //public Element element;
        public String strLineup;
        public int iSectionNumber;
        public String strLineupDescription;
        public String strFamily;
        public string strCatType;
        public Double dblDepth;
        public Double dblWidth;
        public String Part_ID;

        public clsElements(//Element element,
                            String strLineup,
                            int iSectionNumber,
                            String strLineupDescription,
                            String strFamily,
                            string strCatType,
                            Double dblDepth,
                            Double dblWidth,
                            String Part_ID)
        {
            //this.element = element;
            this.strLineup = strLineup;
            this.iSectionNumber=iSectionNumber;
            this.strLineupDescription = strLineupDescription;
            this.strFamily= strFamily;
            this.strCatType=strCatType;
            this.dblDepth = dblDepth;
            this.dblWidth = dblWidth;
            this.Part_ID = Part_ID;
        }
    }
}
