﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LayoutFAST_Dev
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnTransformers_Click(object sender, EventArgs e)
        {
            testplugin.frmSEProducts frmSEP = new testplugin.frmSEProducts();
            try
            {
                frmSEP.Show();
                frmSEP.BringToFront();
            }
            catch
            {
            }
        }

        private void btnPanelboards_Click(object sender, EventArgs e)
        {
            LayoutFAST_Panelboards.frmSEProducts frmsep = new LayoutFAST_Panelboards.frmSEProducts();
            try
            {
                frmsep.Show();
                frmsep.BringToFront();
            }
            catch
            {
            }
        }

        private void btnSwitchboards_Click(object sender, EventArgs e)
        {
            Switchboard_Selector.frmSwitchboardSelector fsw = new Switchboard_Selector.frmSwitchboardSelector();
            fsw.Show();
        }

       
    }
}
