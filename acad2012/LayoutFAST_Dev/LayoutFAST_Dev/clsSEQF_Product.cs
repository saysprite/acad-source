﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReadXLS
{
    public class clsSEQF_Product
    {
        public int irownumber
        {
            get;
            set;
        }
        public string sheet
        {
            get;
            set;
        }
        public string  row
        {
            get;
            set;
        }

        public string column
        {
            get;
            set;
        }
        public string value
        {
            get;
            set;
        }

    }
}
