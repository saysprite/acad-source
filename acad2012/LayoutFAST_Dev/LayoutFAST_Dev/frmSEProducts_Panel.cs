﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using MgdAcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using MgdAcDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcWindowsNS = Autodesk.AutoCAD.Windows;


namespace LayoutFAST_Panelboards
{
    public partial class frmSEProducts : System.Windows.Forms.Form
    {
        bool blnNQPowerBottom = false;
        bool blnIlinePowerBottom = false;
        System.Windows.Forms.Panel currentpanel = null;
 
        int irow_enclosure = 0;
        List<String> lstCatalogIDEnclosure_Input = new List<String>();
        List<String> lstCatalogID_Interior = new List<String>();
        List<RadioButton> lstradio_Input = new List<RadioButton>();
      //  string defaultsize = "30";
  //      bool blnReset = false;
        List<System.Windows.Forms.ComboBox> lstcmb_Input = new List<System.Windows.Forms.ComboBox>();
        

        public string gbl_apppath = "";
        public string gbl_filepath = "";
        public string gbl_xml = "\\Cloud Offline\\switchboard_2013.XML";

        List<ReadXLS.clsSEQF_Product> lst_Accessories_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_Features_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_FeatureValue_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_ProdProduct_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_Product_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_Solutions_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_SolutionsTMP_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_SubProducts_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_XML_Enclosure = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsGroupBoxes> lstGroupBox_Enclosure = new List<ReadXLS.clsGroupBoxes>();

        List<ReadXLS.clsGroupBoxes> lstGroupBox_Interior = new List<ReadXLS.clsGroupBoxes>();
        List<ReadXLS.clsSEQF_Product> lst_Accessories_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_Features_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_FeatureValue_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_ProdProduct_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_Product_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_Solutions_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_SolutionsTMP_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_SubProducts_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_XML_Interior = new List<ReadXLS.clsSEQF_Product>();
        List<LVSelector3.clsQuestionsandAnswers> lstQA = new List<LVSelector3.clsQuestionsandAnswers>();

        List<ReadXLS.clsSEQF_Product> lst_Filtered = new List<ReadXLS.clsSEQF_Product>();
      
        List<ReadXLS.clsGroupBoxes> lstGroupBox_LVTransformer = new List<ReadXLS.clsGroupBoxes>();

        List<String> lstCatalogID_LVTransformer_Input = new List<String>();

        string strPanelPartID = "";

      

        Database db = HostApplicationServices.WorkingDatabase;
        Editor ed = MgdAcApplication.DocumentManager.MdiActiveDocument.Editor;

        public frmSEProducts()
        {
            int ierror = 0;

            bool blnexitnow = false;
            gbl_apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            gbl_filepath = Path.GetDirectoryName(gbl_apppath).ToString();
            InitializeComponent();
            

            if (File.Exists(gbl_filepath+"\\lvpanel.inuse"))
            {
                DialogResult dialogResult = MessageBox.Show("Would you like to continue?", "LayoutFAST already started", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    //do something
                }
                else if (dialogResult == DialogResult.No)
                {
                    blnexitnow = true;
                }
                         
            }
            else
            {
                File.Create(gbl_filepath + "\\lvpanel.inuse");
            }
            if (!blnexitnow)
            {
                ierror = tab0_selected();

                if (ierror < 0)
                {
                    MessageBox.Show("Database files do not exist");
                    blnexitnow = true;
                }


                if (!blnexitnow)
                {
                    tabPage3.ToolTipText = "For small to medium commercial and industrial lighting and power distribution.";
                    tabControl1.ShowToolTips = true;
                }

            }
        }

        private int tab0_selected()
        {
            int ierror = 0;
            
            string strInput2 = "A215_610";

            txtPanelName.Text = "Panel Name";
            currentpanel = exp_SpecificQuestions_Enclosure;
            gbl_apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            gbl_filepath = Path.GetDirectoryName(gbl_apppath).ToString();

            btnRevit.Text = "Insert Family";

            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            string strNQCount = "";
            int iNQCount = 0;
            int i = 0;
            strNQCount = ini.IniReadValue("NQFav", "Count");
            if (strNQCount != "")
            {
                try
                {
                    iNQCount = Convert.ToInt32(strNQCount);
                }
                catch
                {
                }
            }
            else
            {
                iNQCount = 0;
            }
            i = 0;
            combo_FavoritePanel.Text = "Enter Name";
            //this.combo_FavoritePanel.ForeColor = System.Drawing.Color.Silver;
            combo_FavoritePanel.Items.Clear();
            while (i < iNQCount)
            {
                combo_FavoritePanel.Items.Add(ini.IniReadValue("NQFav", i.ToString()));
                i++;
            }



            lstQA.Clear();
            btnRevit.Enabled = false;
            btnSave.Enabled = false;

            if (File.Exists(gbl_filepath + "\\Cloud Offline\\" + strInput2 + ".XML"))
            {
                load_xmlfile_Enclosure(gbl_filepath + "\\Cloud Offline\\" + strInput2 + ".XML", true);

                load_question();
                lst_Filtered = lst_Solutions_Enclosure;
                unupdate_dimension(txtDepth, txtWidth, txtHeight);
            }
            else
            {
                ierror = -1;
            }


         
            combo_NQ.Items.Clear();
            combo_NQ.Text = "";
       
            return ierror;
        }

        private int tab2_selected()
        {
            int ierror = 0;

            string strInput1 = "iline";

            txtIlinePanelName.Text = "Panel Name";
            btnIlineInsert.Text = "Insert Family";
            lstQA.Clear();
            btnIlineInsert.Enabled = false;
            btnILineSave.Enabled = false;
            currentpanel = exp_SpecificQuestions_ILine;
            gbl_apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            gbl_filepath = Path.GetDirectoryName(gbl_apppath).ToString();


            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            string strNQCount = "";
            int iNQCount = 0;
            int i = 0;
            strNQCount = ini.IniReadValue("ILineFav", "Count");
            if (strNQCount != "")
            {
                try
                {
                    iNQCount = Convert.ToInt32(strNQCount);
                }
                catch
                {
                }
            }
            else
            {
                iNQCount = 0;
            }
            i = 0;

            combo_ILineFavoritePanel.Text = "Enter Name";
            //combo_ILineFavoritePanel.ForeColor = System.Drawing.Color.Silver;
            combo_ILineFavoritePanel.Items.Clear();
            while (i < iNQCount)
            {
                combo_ILineFavoritePanel.Items.Add(ini.IniReadValue("ILineFav", i.ToString()));
                i++;
            }


            if (File.Exists(gbl_filepath + "\\Cloud Offline\\" + strInput1 + ".XML"))
            {
                load_xmlfile_Enclosure(gbl_filepath + "\\Cloud Offline\\" + strInput1 + ".XML", true);
                load_question();
                lst_Filtered = lst_Solutions_Enclosure;
                unupdate_dimension(txtILineDepth, txtIlineWidth,txtILineHeight);
            }
            else
            {
                ierror = -1;
            }
             combo_Iline.Items.Clear();
            combo_Iline.Text = "";
          


            return ierror;
        }

    
        private void load_xmlfile_Enclosure(string strInputFile, bool blnAddto)
        {
            List<ReadXLS.clsSEQF_Product> lst_XML = new List<ReadXLS.clsSEQF_Product>();

            LayoutFAST_Dev.SE_QFProductED seqf = new LayoutFAST_Dev.SE_QFProductED();

            lst_Accessories_Enclosure = new List<ReadXLS.clsSEQF_Product>();
            lst_Features_Enclosure = new List<ReadXLS.clsSEQF_Product>();
            lst_FeatureValue_Enclosure = new List<ReadXLS.clsSEQF_Product>();
            lst_ProdProduct_Enclosure = new List<ReadXLS.clsSEQF_Product>();
            lst_Product_Enclosure = new List<ReadXLS.clsSEQF_Product>();
            lst_Solutions_Enclosure = new List<ReadXLS.clsSEQF_Product>();
            lst_SubProducts_Enclosure = new List<ReadXLS.clsSEQF_Product>();

            if (File.Exists(strInputFile))
            {
                lst_XML_Interior = seqf.XLS2XML_readRecord(strInputFile);

                foreach (ReadXLS.clsSEQF_Product clp in lst_XML_Interior)
                {
                    if (clp.sheet == "Product")
                        lst_Product_Enclosure.Add(clp);
                    else if (clp.sheet == "Features")
                        lst_Features_Enclosure.Add(clp);
                    else if (clp.sheet == "Feature Values")
                        lst_FeatureValue_Enclosure.Add(clp);
                    else if (clp.sheet == "Solutions")
                    {
                        lst_Solutions_Enclosure.Add(clp);
                    }
                    else if (clp.sheet.Contains("Accessories"))
                    {
                        lst_Accessories_Enclosure.Add(clp);
                    }  //if (clp.sheet == "Product")

                }  //foreach (LVSelector3.clsSEQF_Product clp in lst_XML)

            }  //if (File.Exists(gbstrXMLFileName))

        }

        private List<ReadXLS.clsSEQF_Product> get_distinct_answers(List<ReadXLS.clsSEQF_Product> lst, string columnname)
        {
            List<ReadXLS.clsSEQF_Product> ltmp = new List<ReadXLS.clsSEQF_Product>();
            ltmp = lst.Where(lp => lp.column == columnname).GroupBy(x => x.value).Select(y => y.First()).ToList();
            return ltmp;
        }

        private void place_Calculate_button_Enclosure(System.Windows.Forms.Panel panelx, int x, int y, out int yOut)
        {
            int xs = 0;

            System.Windows.Forms.Button cmb = new System.Windows.Forms.Button();

            xs = (panelx.Width - 187) / 2;

            cmb.Enabled = true;
            cmb.Size = new System.Drawing.Size(187, 41);
            cmb.Location = new System.Drawing.Point(xs, y);
            cmb.Name = "btnReset";
            //cmb.Text = "Calculate Box Size";
            cmb.Text = "Clear all Answers";
            cmb.UseVisualStyleBackColor = true;
            cmb.ForeColor = System.Drawing.Color.Black;
            cmb.Click += new EventHandler(Calc_values_Enclosure);
            panelx.Controls.Add(cmb);
            yOut = y;
        }

        private void load_question()
        {
            bool found = false;

            int jj = 0;
            int x = 10;
            int xg = 5;
            int y = 10;
            int yg = 10;

            lstGroupBox_Enclosure.Clear();

            ReadXLS.clsGroupBoxes cGB = new ReadXLS.clsGroupBoxes();

            List<ReadXLS.clsSEQF_Product> tmplst = new List<ReadXLS.clsSEQF_Product>();
            List<LVSelector3.clsQuestionsandAnswers> lstq = new List<LVSelector3.clsQuestionsandAnswers>();

            GroupBox g;
            
            RadioButton rb = new RadioButton();
            RadioButton rb1 = new RadioButton();

            System.Windows.Forms.ComboBox cb = new System.Windows.Forms.ComboBox();

            currentpanel.Controls.Clear();

            foreach (ReadXLS.clsSEQF_Product cp in lst_Features_Enclosure)
            {
                cGB = new ReadXLS.clsGroupBoxes();

                if (cp.sheet == "Features")
                {

                    if (cp.column == "Field_Type" && cp.value != "button")
                    {
                        g = place_Groupbox_Input(currentpanel, cp.row, x, y);
                        cGB.gbQuestion = g;
                        lstGroupBox_Enclosure.Add(cGB);
                        xg = 10;
                        yg = 20;
                        System.Windows.Forms.Button bx = place_little_button(g, cp.row);

                        if (cp.value == "select")
                        {
                            // create combobox
                            System.Windows.Forms.ComboBox cc = place_cmbbox_Enclosure(g, cp.row, "any", xg, yg, out yg);

                            tmplst = get_distinct_answers(lst_Solutions_Enclosure, cp.row);

                            foreach (ReadXLS.clsSEQF_Product cf in tmplst)
                            {
                                //add to combobox list of answers
                                add_to_combo(cc, cf.value);
                            }  //foreach (LVSelector3.clsSEQF_Product cf in tmplst)

                            if (cc.Items.Count == 1)
                            {
                                cc.Text = cc.Items[0].ToString();
                                lstq = lstQA.Where(lp => lp.gbQuestion == cp.column).ToList();
                                
                                if (lstq.Count > 0)
                                {
                                    lstq[0].gbAnswer = cc.Text;
                                }

                            }

                        }
                        else if (cp.value == "radio")
                        {
                            
                            tmplst = lst_FeatureValue_Enclosure.Where(lp => lp.row == cp.row && lp.column == "Value").ToList();

                            foreach (ReadXLS.clsSEQF_Product cf in tmplst)
                            {
                                rb = place_radiobutton_Enclosure(g, cf.value, xg, yg, out yg);
                            }  //foreach (LVSelector3.clsSEQF_Product cf in tmplst)
                            
                            tmplst = get_distinct_answers(lst_Solutions_Enclosure, cp.row);

                            if (tmplst.Count == 1)
                            {
                                jj = 0;
                                found = false;

                                while (jj < g.Controls.Count && !found)
                                {

                                    if (g.Controls[jj].GetType().ToString() == "System.Windows.Forms.RadioButton")
                                    {
                                        rb1 = (RadioButton)g.Controls[jj];

                                        if (rb1.Text == tmplst[0].value.ToString())
                                        {
                                            found = true;
                                            rb1.Checked = true;
                                        }

                                    }

                                    jj++;
                                }

                                if (found)
                                {
                                    jj = 0;
                                    while (jj < g.Controls.Count)
                                    {
                                        if (g.Controls[jj].GetType().ToString() == "System.Windows.Forms.RadioButton")
                                        {
                                            rb1 = (RadioButton)g.Controls[jj];
                                            rb1.Enabled = false;
                                        }

                                        jj++;
                                    }

                                }

                            }

                        }  //if (cp.value == "select")

                        y = y + g.Size.Height + 9;
                    }  //if (cp.column == "Field_Type" && cp.value != "button")

                }  //if (cp.sheet == "Features")

            }  //foreach (LVSelector3.clsSEQF_Product cp in lst_Features)

      //      place_Calculate_button_Enclosure(currentpanel, x, y, out y);
        }

        private void editbutton_click(object sender, EventArgs e)
        {
            bool found = false;

            Button bb = (Button)sender;

            int i = 0;
            int j = 0;

            GroupBox g = (GroupBox)bb.Parent;

            List<LVSelector3.clsQuestionsandAnswers> lstq = new List<LVSelector3.clsQuestionsandAnswers>();

            List<ReadXLS.clsSEQF_Product> lstdistinctrows = new List<ReadXLS.clsSEQF_Product>();

            RadioButton rb;

            string strname = "";

            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();
            
            i = bb.Name.IndexOf("btnEdit_");

            if (i >= 0)
            {
                strname = bb.Name.Substring(8, bb.Name.Length - i - 8);
                i = 0;
                lstq = lstQA.Where(lp => lp.gbQuestion == strname).ToList();
            
                if (lstq.Count > 0)
                {
                
                    if (lstq[0].typeanswer == "Combo")
                    {
                        j = 0;
                        found = false;
                    
                        while (j < g.Controls.Count && !found)
                        {
                        
                            if (g.Controls[j].GetType().ToString() == "System.Windows.Forms.ComboBox")
                            {
                                g.Controls[j].Enabled = true;
                                found = true;
                                lstq[0].gbAnswer = null;
                                System.Windows.Forms.ComboBox cc = (System.Windows.Forms.ComboBox)g.Controls[j];
                                cc.Text = "";
                            }
                            else
                            {
                                j++;
                            }

                        }

                    }
                    else //radio button   thelma
                    {
                        lstq[0].gbAnswer = null;
                        j = 0;
            
                        while (j < g.Controls.Count)
                        {
                        
                            if (g.Controls[j].GetType().ToString() == "System.Windows.Forms.RadioButton")
                            {
                                rb = (RadioButton)g.Controls[j];
                                rb.Enabled = true;
                                rb.Checked = false;
                            }
                            
                            j++;
                        }

                    }

                }

            }

            lst_Filtered = lst_Solutions_Enclosure;
            
            foreach (LVSelector3.clsQuestionsandAnswers cq in lstQA)
            {
            
                if (cq.gbAnswer != null)
                {
                    lst_Filtered = cRev.get_list_of_rows_opt3(lst_Filtered, cq.gbQuestion, cq.gbAnswer);
                }

                reload_all_answers(currentpanel, lst_Filtered, lstQA);
            }

            lstdistinctrows = get_distinct_answers(lst_Filtered, "Part_ID");

            if (lstdistinctrows.Count == 1)
            {
                
                if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                {
                    btnRevit.Enabled = true;
                    btnSave.Enabled = true;
                    update_dimensions(lst_Filtered, txtDepth, txtWidth, txtHeight);
                    strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                }
                
                else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                {
                    btnIlineInsert.Enabled = true;
                    btnILineSave.Enabled = true;
                    update_dimensions(lst_Filtered, txtILineDepth, txtIlineWidth, txtILineHeight);
                    strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                }

            }
            else
            {
                if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                {
                    btnRevit.Enabled = false;
                    btnSave.Enabled = false;
                    unupdate_dimension(txtDepth, txtWidth, txtHeight);
                }
              
                else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                {
                    btnIlineInsert.Enabled = false;
                    btnILineSave.Enabled = false;
                    unupdate_dimension(txtILineDepth,txtIlineWidth,txtILineHeight);
                }

            }

        }

        private void reload_all_answers(System.Windows.Forms.Panel panelx, List<ReadXLS.clsSEQF_Product> lsttmp, List<LVSelector3.clsQuestionsandAnswers> lstq)
        {
            bool found = false;

            GroupBox g = new GroupBox();

            int i = 0;
            int j = 0;
            int jj = 0;
            
            List<LVSelector3.clsQuestionsandAnswers> ctmp = new List<LVSelector3.clsQuestionsandAnswers>();

            List<ReadXLS.clsSEQF_Product> lstdistanswers = new List<ReadXLS.clsSEQF_Product>();
            
            RadioButton rb = new RadioButton();
         
            while (i < panelx.Controls.Count)
            {
                ctmp = lstq.Where(lp => "gb_" + lp.gbQuestion == panelx.Controls[i].Name).ToList();
                
                if (ctmp.Count > 0)
                {
                    g = (GroupBox)panelx.Controls[i];
                    
                    if (ctmp[0].gbAnswer == null)
                    {
                        
                        if (ctmp[0].typeanswer == "Combo")
                        {
                            j = 0;
                            found = false;
                        
                            while (j < g.Controls.Count && !found)
                            {
                            
                                if (g.Controls[j].Name == "cmb" + ctmp[0].gbQuestion)
                                {
                                    found = true;
                                }
                                else
                                {
                                    j++;
                                }

                            }

                            if (found)
                            {
                                System.Windows.Forms.ComboBox cc = (System.Windows.Forms.ComboBox)g.Controls[j];
                                lstdistanswers = get_distinct_answers(lsttmp, ctmp[0].gbQuestion);
                                cc.Items.Clear();
                                
                                foreach (ReadXLS.clsSEQF_Product cf in lstdistanswers)
                                {
                                    //add to combobox list of answers
                                    add_to_combo(cc, cf.value);
                                }  //foreach (LVSelector3.clsSEQF_Product cf in tmplst)
                                
                                if (cc.Items.Count == 1) cc.Text = cc.Items[0].ToString();
                            }

                        }
                        else
                        {
                            j = 0;
                            found = false;
                            jj = 0;
                            lstdistanswers = get_distinct_answers(lsttmp, ctmp[0].gbQuestion);

                            while (j < g.Controls.Count)
                            {
                                if (g.Controls[j].GetType().ToString() == "System.Windows.Forms.RadioButton")
                                {
                                    jj = 0;
                                    found = false;
                            
                                    while (jj < lstdistanswers.Count && !found)
                                    {
                                    
                                        if (lstdistanswers[jj].value.ToString() == g.Controls[j].Text)
                                        {
                                            found = true;
                                        }
                                        else
                                        {
                                            jj++;
                                        }
                                    }


                                    g.Controls[j].Enabled = found;
                                  
                                        g.Controls[j].Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          
                                    
                                    if (lstdistanswers.Count == 1 && g.Controls[j].Enabled)
                                    {
                                        rb = (RadioButton)g.Controls[j];
                                        rb.Checked = true;

                                        
                                    }

                                }

                                j++;
                            }

                        }

                    }

                }

                i++;
            }

        }

        private void unupdate_dimension(System.Windows.Forms.TextBox lDepth, System.Windows.Forms.TextBox lWidth, System.Windows.Forms.TextBox lHeight)
        {
            lDepth.Text = "?";
            lWidth.Text = "?";
            lHeight.Text = "?";
        }

   //     combo_FavoritePanel_Enter
    //    public void combo_FavoritePanel_Enter(object sender, EventArgs e)
        public void combo_FavoritePanel_GotFocus(object sender, EventArgs e)
        {
            System.Windows.Forms.ComboBox tb = (System.Windows.Forms.ComboBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= combo_FavoritePanel_GotFocus;
//            tb.Enter -= combo_FavoritePanel_Enter;
            if (tb.Text == "Enter Name")
            {
  //              this.combo_FavoritePanel.ForeColor = System.Drawing.Color.Silver;
            }
            else
            {
                this.combo_FavoritePanel.ForeColor = System.Drawing.Color.Black;
            }
        }
        public void combo_ILineFavoritePanel_GotFocus(object sender, EventArgs e)
        {
            System.Windows.Forms.ComboBox tb = (System.Windows.Forms.ComboBox)sender;
            tb.Text = string.Empty;
            tb.GotFocus -= combo_ILineFavoritePanel_GotFocus;
            if (tb.Text == "Enter Name")
            {
 //              this.combo_ILineFavoritePanel.ForeColor = System.Drawing.Color.Silver;
            }
            else
            {
                this.combo_ILineFavoritePanel.ForeColor = System.Drawing.Color.Black;

            }
        }

      
        private void update_dimensions(List<ReadXLS.clsSEQF_Product> lsttmp, System.Windows.Forms.TextBox lDepth, System.Windows.Forms.TextBox lWidth, System.Windows.Forms.TextBox lHeight)
        {
            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();
            
            if (lsttmp.Count > 0)
            {
                lDepth.Text = cRev.get_field(lsttmp[0].irownumber, lsttmp, "Depth")+"\"";
                lWidth.Text = cRev.get_field(lsttmp[0].irownumber, lsttmp, "Width") + "\"";
                lHeight.Text = cRev.get_field(lsttmp[0].irownumber, lsttmp, "Height") + "\"";
            }

        }

        private Button place_little_button(System.Windows.Forms.GroupBox panelx, string strHeader)
        {
            Button bbb = new Button();

            bbb.UseVisualStyleBackColor = true;
            bbb.BackgroundImage = LayoutFAST_Dev.Properties.Resources.penx;
            bbb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            bbb.Location = new System.Drawing.Point(panelx.Width - 20, 0);
            bbb.Margin = new System.Windows.Forms.Padding(4);
            bbb.Name = "btnEdit_" + strHeader;
            bbb.Size = new System.Drawing.Size(20, 20);
            bbb.TabIndex = 114;
            bbb.Text = "";
            bbb.UseVisualStyleBackColor = true;
            bbb.Click += new System.EventHandler(editbutton_click);
            panelx.Controls.Add(bbb);
            return bbb;
        }

        private void Calc_values_Enclosure(object sender, EventArgs e)
        {
            GroupBox g;

            int i = 0;
            int k = 0;
            int kk = 0;

            List<LVSelector3.clsQuestionsandAnswers> lstq = new List<LVSelector3.clsQuestionsandAnswers>();

            List<ReadXLS.clsSEQF_Product> lstdistinctrows = new List<ReadXLS.clsSEQF_Product>();

            RadioButton rb;

            string strname = "";

            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();

            while (k < currentpanel.Controls.Count)
            {
                if (currentpanel.Controls[k].GetType().ToString() == "System.Windows.Forms.GroupBox")
                {
                    g = (GroupBox)currentpanel.Controls[k];
                    kk = 0;
                    
                    while (kk < g.Controls.Count)
                    {
                        
                        if (g.Controls[kk].GetType().ToString() == "System.Windows.Forms.ComboBox")
                        {
                            System.Windows.Forms.ComboBox cc = (System.Windows.Forms.ComboBox)g.Controls[kk];
                            cc.Text = "";
                            cc.Enabled = true;
                            i = cc.Name.IndexOf("cmb");

                            strname = cc.Name.Substring(3, cc.Name.Length - i - 3);
                            lstq = lstQA.Where(lp => lp.gbQuestion == strname).ToList();
                        
                            if (lstq.Count > 0)
                            {
                                lstq[0].gbAnswer = null;
                            }
                            if (cc.Items.Count == 1)
                            {
                                cc.Text = cc.Items[0].ToString();
                            }

                        }
                        else if (g.Controls[kk].GetType().ToString() == "System.Windows.Forms.RadioButton")
                        {
                            i = g.Name.IndexOf("gb_");
                            strname = g.Name.Substring(3, g.Name.Length - i - 3);
                            lstq = lstQA.Where(lp => lp.gbQuestion == strname).ToList();
                         
                            if (lstq.Count > 0)
                            {
                                lstq[0].gbAnswer = null;
                            }

                            rb = (RadioButton)g.Controls[kk];
                            rb.Checked = false;
                            rb.Enabled = true;
                            rb.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                          
                        }

                        kk++;
                    }

                }

                k++;
            }

            lst_Filtered = lst_Solutions_Enclosure;
            
            foreach (LVSelector3.clsQuestionsandAnswers cq in lstQA)
            {
                
                if (cq.gbAnswer != null)
                {
                    lst_Filtered = cRev.get_list_of_rows_opt3(lst_Filtered, cq.gbQuestion, cq.gbAnswer);
                    reload_all_answers(currentpanel, lst_Filtered, lstQA);
                }

            }

            lstdistinctrows = get_distinct_answers(lst_Filtered, "Part_ID");
            
            if (lstdistinctrows.Count == 1)
            {

                if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                {
                    btnRevit.Enabled = true;
                    btnSave.Enabled = true;
                    update_dimensions(lst_Filtered, txtDepth, txtWidth, txtHeight);
                    strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                }
                else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                {
                    update_dimensions(lst_Filtered,txtILineDepth,txtIlineWidth,txtILineHeight);
                    btnIlineInsert.Enabled = true;
                    btnILineSave.Enabled = true;
                    strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                }

            }
            else
            {

                if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                {
                    unupdate_dimension(txtDepth, txtWidth, txtHeight);
                    btnRevit.Enabled = false;
                    btnSave.Enabled = false;
                }
                else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                {
                    unupdate_dimension(txtILineDepth, txtIlineWidth, txtILineHeight);
                    btnIlineInsert.Enabled = false;
                    btnILineSave.Enabled = false;
                }

            }

        }

        private int get_solutions_Input_Enclosure_row(string strProductName)
        {
            bool found = false;

            int i = 0;
            int ip = 0;

            while (i < lst_Solutions_Enclosure.Count && !found)
            {

                if (lst_Solutions_Enclosure[i].row == strProductName)
                {
                    ip = lst_Solutions_Enclosure[i].irownumber;
                    found = true;
                }
                else
                {
                    i++;
                }  //if (lst_Product[i].row == strProductName)

            }  //while (i < lst_Product.Count && !found)

            return ip;
        }

        private GroupBox place_Groupbox_Input(System.Windows.Forms.Panel panelx, string strHeader, int x, int y)
        {
            GroupBox g = new GroupBox();

            LVSelector3.clsQuestionsandAnswers cqa = new LVSelector3.clsQuestionsandAnswers();
            g.Location = new System.Drawing.Point(x, y);
            g.Name = "gb_" + strHeader;
            g.Size = new System.Drawing.Size(panelx.Width - x - 30, 30);
            g.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            g.Text = strHeader;
            cqa.gbQuestion = strHeader;
            lstQA.Add(cqa);
            panelx.Controls.Add(g);
            return g;
        }

        private void change_radio_answer(object sender, EventArgs e)
        {
            bool found = false;

            int i = 0;
            int j = 0;

            List<ReadXLS.clsSEQF_Product> lstdistinctrows = new List<ReadXLS.clsSEQF_Product>();

            RadioButton rb = (RadioButton)sender;
            RadioButton rb1 = (RadioButton)sender;

            string cQuestion = "";
            string stra = rb.Text;

            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();

            GroupBox g = (GroupBox)rb.Parent;

            while (i < g.Controls.Count)
            {
            
                if (g.Controls[i].GetType().ToString() == "System.Windows.Forms.RadioButton")
                {
                    rb1 = (RadioButton)g.Controls[i];
                    rb1.Enabled = false;
                }

                i++;
            }

            if (rb.Checked)
            {
                i = 0;
                cQuestion = g.Name.Substring(i + 3, g.Name.Length - i - 3);
                j = 0;
                found = false;
                
                while (j < lstQA.Count && !found)
                {
                
                    if (lstQA[j].gbQuestion == cQuestion)
                    {
                        found = true;
                    }
                    else
                    {
                        j++;
                    }

                }

                if (found) lstQA[j].gbAnswer = stra;

                List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
                List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
                lst_Filtered = cRev.get_list_of_rows_opt3(lst_Filtered, cQuestion, stra);
                reload_all_answers(currentpanel, lst_Filtered, lstQA);
                lstdistinctrows = get_distinct_answers(lst_Filtered, "Part_ID");

                if (lstdistinctrows.Count == 1)
                {
                    
                    if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                    {
                        btnRevit.Enabled = true;
                        btnSave.Enabled = true;
                        update_dimensions(lst_Filtered, txtDepth, txtWidth, txtHeight);
                        strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                    }
                    else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                    {
                        btnIlineInsert.Enabled = true;
                        btnILineSave.Enabled = true;
                        update_dimensions(lst_Filtered, txtILineDepth, txtIlineWidth, txtILineHeight);
                        strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                    }

                }
                else
                {
                    
                    if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                    {
                        unupdate_dimension(txtDepth, txtWidth, txtHeight);
                        btnRevit.Enabled = false;
                        btnSave.Enabled = false;
                    }
                    else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                    {
                        btnIlineInsert.Enabled = false;
                        btnILineSave.Enabled = false;
                        unupdate_dimension(txtILineDepth, txtIlineWidth, txtILineHeight);
                    }

                }

            }  //rbchecked

        }

        private void change_answer(object sender, EventArgs e)
        {
            System.Windows.Forms.ComboBox cmb = (System.Windows.Forms.ComboBox)sender;

            bool found = false;
            
            int i = cmb.Parent.Name.IndexOf("gb_");
            int j = 0;

            List<ReadXLS.clsSEQF_Product> lstdistinctrows = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();

            string cQuestion = "";

            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();
            
            System.Windows.Forms.Panel panelx;
            
            GroupBox g = (GroupBox)cmb.Parent;
            cmb.Enabled = false;
            panelx = (System.Windows.Forms.Panel)g.Parent;

            if (i >= 0)
            {

                cQuestion = g.Name.Substring(i + 3, cmb.Parent.Name.Length - i - 3);
                j = 0;
                found = false;
                
                while (j < lstQA.Count && !found)
                {
                
                    if (lstQA[j].gbQuestion == cQuestion)
                    {
                        found = true;
                    }
                    else
                    {
                        j++;
                    }

                }

                if (found) lstQA[j].gbAnswer = cmb.Text;
                
                tmp = new List<ReadXLS.clsSEQF_Product>();
                tmp1 = new List<ReadXLS.clsSEQF_Product>();
                lst_Filtered = cRev.get_list_of_rows_opt3(lst_Filtered, cQuestion, cmb.Text);

                reload_all_answers(panelx, lst_Filtered, lstQA);
                lstdistinctrows = get_distinct_answers(lst_Filtered, "Part_ID");
                
                if (lstdistinctrows.Count == 1)
                {
                
                    if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                    {
                        btnRevit.Enabled = true;
                        btnSave.Enabled = true;
                        update_dimensions(lst_Filtered, txtDepth, txtWidth, txtHeight);
                        strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                    }
                    else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                    {
                        btnIlineInsert.Enabled = true;
                        btnILineSave.Enabled = true;
                        update_dimensions(lst_Filtered, txtILineDepth, txtIlineWidth, txtILineHeight);
                        strPanelPartID = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "Part_ID");
                    }

                }
                else
                {
                    
                    if (currentpanel.Name == exp_SpecificQuestions_Enclosure.Name)
                    {
                        unupdate_dimension(txtDepth, txtWidth, txtHeight);
                        btnRevit.Enabled = false;
                        btnSave.Enabled = false;
                    }
                    else if (currentpanel.Name == exp_SpecificQuestions_ILine.Name)
                    {
                        btnIlineInsert.Enabled = false;
                        btnILineSave.Enabled = false;
                        unupdate_dimension( txtILineDepth,txtIlineWidth,txtILineHeight);
                    }

                }

            }

        }

        private System.Windows.Forms.ComboBox place_cmbbox_Enclosure(GroupBox g, string cQuestion, string strValue, int x, int y, out int yOut)
        {
            bool found = false;

            int ioffset = 9;
            int j = 0;
            
            LVSelector3.clsQuestionsandAnswers cqa = new LVSelector3.clsQuestionsandAnswers();

            System.Windows.Forms.ComboBox cmb = new System.Windows.Forms.ComboBox();

            cmb.Enabled = true;
            cmb.Size = new System.Drawing.Size(g.Width - x - ioffset, 13);
            cmb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));

            cmb.Location = new System.Drawing.Point(x, y);
            cmb.Name = "cmb" + cQuestion;
            cmb.SelectedValueChanged += new EventHandler(change_answer);

            g.Controls.Add(cmb);

            lstcmb_Input.Add(cmb);

            yOut = y;

            if (cmb.Location.Y + cmb.Size.Height + 10 >= g.Size.Height)
            {
                g.Height = cmb.Location.Y + cmb.Size.Height + ioffset;
                yOut = cmb.Location.Y + cmb.Size.Height + ioffset;
            }  // if (cmb.Location.Y + cmb.Size.Height + 10 >= g.Size.Height)

            j = 0;
            found = false;
            
            while (j < lstQA.Count && !found)
            {
                if (lstQA[j].gbQuestion == cQuestion)
                {
                    found = true;
                }
                else
                {
                    j++;
                }

            }

            if (found) lstQA[j].typeanswer = "Combo";
            
            return cmb;
        }

        private void add_to_combo(System.Windows.Forms.ComboBox cmbx, string strValue)
        {
            bool found = false;

            int i = 0;

            while (i < cmbx.Items.Count && !found)
            {
                if (strValue == cmbx.Items[i].ToString()) found = true;
                i++;
            }  // while (i < cmbx.Items.Count && !found)

            if (!found)
            {
                cmbx.Items.Add(strValue);
            }  // if (!found)
           // cmbx.Sorted = true;
        }

        private RadioButton place_radiobutton_Enclosure(GroupBox g, string strValue, int x, int y, out int yOut)
        {
            int ioffset = 0;
            int iroffset = 0;
            
            System.Windows.Forms.RadioButton cmb = new System.Windows.Forms.RadioButton();

            cmb.AutoSize = false;//
            cmb.Location = new System.Drawing.Point(x, y);
            cmb.Name = "rad" + g.Name + "_" + strValue;
            cmb.Size = new System.Drawing.Size(g.Width - 30, 20);
            cmb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            cmb.ForeColor = System.Drawing.Color.Black;
            cmb.Text = strValue;
            cmb.BackColor = System.Drawing.Color.Transparent;
            cmb.Enabled = true;
           // cmb.Click += new EventHandler(change_radio_answer);
            cmb.CheckedChanged += new EventHandler(change_radio_answer);

            g.Controls.Add(cmb);

            lstradio_Input.Add(cmb);

            yOut = y;

            if (cmb.Location.Y + cmb.Size.Height + ioffset >= g.Size.Height)
            {
                g.Height = cmb.Location.Y + cmb.Size.Height + ioffset;
                yOut = cmb.Location.Y + cmb.Size.Height + 3 + iroffset;
            }  // if (cmb.Location.Y + cmb.Size.Height + 10 >= g.Size.Height)

            return cmb;
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            if (lst_Filtered.Count>0)
                {
                    int irow = lst_Filtered[0].irownumber;
           // if (listBox_Enclosure.Items.Count > 0)
            //{
                    strTmp = clsRev.get_field(irow, lst_Filtered, "Catalog Link");
            
                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                if (lst_Solutions_Enclosure.Count > 0)
                {
                    strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "Catalog Link");

                    if (strTmp != "")
                    {
                        System.Diagnostics.Process.Start(strTmp);
                    }
                }
                else
                {
                    MessageBox.Show("Unable to display Catalog Link Webpage");
                }
            }

        }

        private void Button10_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            if (lst_Filtered.Count > 0)
            {
                int irow = lst_Filtered[0].irownumber;
                // if (listBox_Enclosure.Items.Count > 0)
                //{
                strTmp = clsRev.get_field(irow, lst_Filtered, "Specification Link");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                if (lst_Solutions_Enclosure.Count > 0)
                {
                    strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "Specification Link");

                    if (strTmp != "")
                    {
                        System.Diagnostics.Process.Start(strTmp);
                    }
                }
                else
                {
                    MessageBox.Show("Unable to display Catalog Link Webpage");
                }
            }



        }

        private void Button11_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();
         
            if (listBox_Enclosure.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "DigestPlus");
            
                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "DigestPlus");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }

        }

        private void Button12_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();
         
            if (listBox_Enclosure.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "3D Link");
            
                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "3D Link");
                
                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();
            
            if (listBox_Enclosure.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "Installation Manual");
            
                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "Installation Manual");
                
                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }

        }

       
        private void btnLVSpecifications_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            if (listBox_LVTransformer.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "Specification Link");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "Specification Link");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }

        }

        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {
            int ierror = 0;

            int tabIndex = GetTabIndex(e.Location);

            if (tabIndex != -1)
            {

                if (tabIndex == 0)
                {
                    ierror = tab0_selected();
                }
                else if (tabIndex == 1)
                {
                    ierror = tab2_selected();
                }

            }

        }

        private int GetTabIndex(System.Drawing.Point p)
        {
            int result = -1;

            for (int i1 = 0; i1 < tabControl1.TabCount; i1++)
            {
                if (tabControl1.GetTabRect(i1).Contains(p))
                    return i1;
            }

            return result;
        }

  
    
     
  
        private void button1_Click_1(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            if (listBox_LVTransformer.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "DigestPlus");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "DigestPlus");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }

        }

        private void btnLVInstallManual_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();
            
            if (listBox_LVTransformer.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "Installation Manual");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "Installation Manual");
                
                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
        }


        //  any revit calls
        private void btnRevit_Click(object sender, EventArgs e)
        {


            Database adb = ed.Document.Database;
            Document ActiveDoc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            db = ActiveDoc.Database;
            ed = ActiveDoc.Editor;
            //    iaDoc = acApp.ActiveDocument;
            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();
            string strDWG = "";
            if (lst_Filtered.Count > 0)
            {
                // this will be the symbol file
                strDWG = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "DWG");
                //             MessageBox.Show("Insert " + strDWG);
                // blref = iaDoc.ModelSpace.InsertBlock(pt, strstupidsymbol, 1, 1, 1, 0, System.Type.Missing);
                //   AddBlockTest(db,strDWG);
                BlockInsert_11(ActiveDoc, gbl_filepath + "\\Cloud Offline\\RFA\\" + strDWG, txtPanelName.Text);

            }


        }

        public void BlockInsert_11(Document ActiveDoc, string blockQualifiedFileName,string strPanelname)
        {
            if (File.Exists(blockQualifiedFileName))
            {
                string blockName = Path.GetFileNameWithoutExtension(blockQualifiedFileName);

                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForRead);
                    if (!bt.Has(blockName))
                    {
                        Database tmpDb = new Database(false, true);
                        tmpDb.ReadDwgFile(blockQualifiedFileName, System.IO.FileShare.Read, true, "");
                        // add the block to the ActiveDrawing blockTable

                        using (DocumentLock docLock = ActiveDoc.LockDocument())
                        {
                            db.Insert(blockName, tmpDb, true);
                        }
                    }

                    PromptPointResult ppr = ed.GetPoint("\nSpecify insertion point: ");
                    if (ppr.Status != PromptStatus.OK)
                        return;
                    using (DocumentLock docLock = ActiveDoc.LockDocument())
                    {
                        BlockTableRecord btr = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);
                        BlockReference br = new BlockReference(Point3d.Origin, bt[blockName]);

                        br.TransformBy(Matrix3d
                            .Displacement(ppr.Value - Point3d.Origin)
                            .PreMultiplyBy(ed.CurrentUserCoordinateSystem));

                        btr.AppendEntity(br);
                        tr.AddNewlyCreatedDBObject(br, true);
                        ApplyAttibutes(db, tr, br, new List<string>(new string[] {
					"EQUIPMENT_NAME_PLATE_DESIGNATION"
				}), new List<string>(new string[] {
					strPanelname
				}));
                        tr.Commit();
                    }
                }
            }
        }


        private static void ApplyAttibutes(Database db, Transaction tr, BlockReference bref, List<string> listTags, List<string> listValues)
        {
            BlockTableRecord btr = (BlockTableRecord)tr.GetObject(bref.BlockTableRecord, OpenMode.ForRead);

            foreach (ObjectId attId in btr)
            {
                Entity ent = (Entity)tr.GetObject(attId, OpenMode.ForRead);
                if (ent is AttributeDefinition)
                {
                    AttributeDefinition attDef = (AttributeDefinition)ent;
                    AttributeReference attRef = new AttributeReference();

                    attRef.SetAttributeFromBlock(attDef, bref.BlockTransform);
                    bref.AttributeCollection.AppendAttribute(attRef);
                    tr.AddNewlyCreatedDBObject(attRef, true);
                    if (listTags.Contains(attDef.Tag))
                    {
                        int found = listTags.BinarySearch(attDef.Tag);
                        if (found >= 0)
                        {
                            attRef.TextString = listValues[found];
                            attRef.AdjustAlignment(db);
                        }
                    }

                }
            }
        }

   
    
  
        private void btnLVTIncomingTop_Click(object sender, EventArgs e)
        {
         //   btnLVTIncomingTop.BackgroundImage = global::LVTransformer_Selector.Properties.Resources.power;
       //     btnLVTIncomingBottom.BackgroundImage = global::LVTransformer_Selector.Properties.Resources.power_off;
        }

        private void btnPnlIncomingBottom_Click(object sender, EventArgs e)
        {
            btnPnlIncomingBottom.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power;
            btnPnlIncomingTop.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power_off;
           blnNQPowerBottom = true;
        }

        private void btnPnlIncomingTop_Click(object sender, EventArgs e)
        {
            btnPnlIncomingTop.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power;
            btnPnlIncomingBottom.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power_off; 
            blnNQPowerBottom = false;
        }

        private void btnLVTIncomingBottom_Click(object sender, EventArgs e)
        {
        //    btnLVTIncomingBottom.BackgroundImage = global::LVTransformer_Selector.Properties.Resources.power;
        //    btnLVTIncomingTop.BackgroundImage = global::LVTransformer_Selector.Properties.Resources.power_off;
        }

        private void btnILineCatalog_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            if (listBox_LVTransformer.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "Catalog Link");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "Catalog Link");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            if (listBox_LVTransformer.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "DigestPlus");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "DigestPlus");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string strTmp = "";

            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            if (listBox_LVTransformer.Items.Count > 0)
            {
                strTmp = clsRev.get_field(irow_enclosure, lst_Solutions_Enclosure, "Specification Link");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }
            else
            {
                strTmp = clsRev.get_field(2, lst_Solutions_Enclosure, "Specification Link");

                if (strTmp != "")
                {
                    System.Diagnostics.Process.Start(strTmp);
                }

            }

        }

  
        private void button1_Click_2(object sender, EventArgs e)
        {
            blnIlinePowerBottom = false;
            btnILineIncomingTop.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power;
            btnILineIncomingBottom.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power_off;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            blnIlinePowerBottom = true;
            btnILineIncomingTop.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power_off;
            btnILineIncomingBottom.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.power;
        }


        private void btnDuplicate_Click(object sender, EventArgs e)
        {
            txtPanelName.Text = txtPanelName.Text + " 2";
        }

        private void btnClearContent_Click(object sender, EventArgs e)
        {

            combo_FavoritePanel.Text = "Enter Name";
            btnRevit.Text = "Insert Family";
  //          this.combo_FavoritePanel.ForeColor = System.Drawing.Color.Silver;
            tab0_selected();
         //   Calc_values_Enclosure(sender, e);
        }

        private void combo_FavoritePanel_SelectedIndexChanged(object sender, EventArgs e)
        {   
            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            string strInput2 = "A215_610";
            bool found = false;
            int i = 0;
            string strCount=ini.IniReadValue("NQFav", "Count");
            int iCount=Convert.ToInt32(strCount);
            string strValue="";



            if (combo_FavoritePanel.Text != "" && combo_FavoritePanel.Text!= "Enter Name")
            {
                lstQA.Clear();
                btnRevit.Enabled = false;
                btnSave.Enabled = false;

                if (File.Exists(gbl_filepath + "\\Cloud Offline\\" + strInput2 + ".XML"))
                {
                    load_xmlfile_Enclosure(gbl_filepath + "\\Cloud Offline\\" + strInput2 + ".XML", true);

                    load_question();
                    lst_Filtered = lst_Solutions_Enclosure;
                    unupdate_dimension(txtDepth, txtWidth, txtHeight);
                }
                else
                {
                }


                lst_Filtered = lst_Solutions_Enclosure;
                unupdate_dimension(txtDepth, txtWidth, txtHeight);
                i = 0;
                found = false;
                while (i < iCount && !found)
                {
                    strValue=ini.IniReadValue("NQFav", i.ToString());
                    if (combo_FavoritePanel.Text == strValue)
                    {
                        found = true;
                    }
                    else
                    {
                        i++;
                    }
                }

                if (found)
                {
                    strValue = ini.IniReadValue("NQFav_Type", i.ToString());
                    List<ReadXLS.clsSEQF_Product> lstpartid=clsRev.get_list_of_rows_opt1(lst_Solutions_Enclosure, "Part_ID", strValue);
                    if (lstpartid.Count > 0)
                    {
                        List<ReadXLS.clsSEQF_Product> lstFields = clsRev.get_row(lstpartid[0].irownumber, lst_Solutions_Enclosure);
                        foreach (ReadXLS.clsSEQF_Product cp in lstFields)
                        {
                            found = false;
                            i = 0;
                            int kk = 0;
                            if (cp.column != null)
                            {
                                while (i < currentpanel.Controls.Count && !found)
                                {
                                    if (currentpanel.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")
                                    {
                                        System.Windows.Forms.GroupBox g = (GroupBox)currentpanel.Controls[i];
                                        kk = 0;
                                        if (g.Text == cp.column.ToString())
                                        {
                                            while (kk < g.Controls.Count && !found)
                                            {

                                                if (g.Controls[kk].GetType().ToString() == "System.Windows.Forms.ComboBox")
                                                {
                                                    System.Windows.Forms.ComboBox cc = (System.Windows.Forms.ComboBox)g.Controls[kk];
                                                    cc.Enabled = true;
                                                    cc.Text = cp.value.ToString();
                                                    found = true;
                                                }
                                                else if (g.Controls[kk].GetType().ToString() == "System.Windows.Forms.RadioButton")
                                                {
                                                    System.Windows.Forms.RadioButton rb = (RadioButton)g.Controls[kk];
                                                    if (rb.Text == cp.value)
                                                    {
                                                        rb.Checked = true;
                                                        found = true;
                                                    }
                                                }
                                                kk++;
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                }
              
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            int i = 0;
            bool found = false;
            if (combo_FavoritePanel.Text != "" && combo_FavoritePanel.Text != "Enter Name")
            {
                i = 0;
                found = false;
                while (i < combo_FavoritePanel.Items.Count && !found)
                {
                    if (combo_FavoritePanel.Items[i].ToString() == combo_FavoritePanel.Text) found = true;  
                    i++;
                }
                if (!found)
                {
                    combo_FavoritePanel.Items.Add(combo_FavoritePanel.Text);
                    int ic = combo_FavoritePanel.Items.Count - 1;
                    ini.IniWriteValue("NQFav", ic.ToString(), combo_FavoritePanel.Text);
                    ini.IniWriteValue("NQFav_Type", ic.ToString(), strPanelPartID);
                    ini.IniWriteValue("NQFav", "Count", combo_FavoritePanel.Items.Count.ToString());
                    
                }

                combo_FavoritePanel.Items.Clear();
               // combo_FavoritePanel.Text = "";
                string strNQCount = "";
                int iNQCount = 0;
                 i = 0;
                strNQCount = ini.IniReadValue("NQFav", "Count");
                if (strNQCount != "")
                {
                    try
                    {
                        iNQCount = Convert.ToInt32(strNQCount);
                    }
                    catch
                    {
                    }
                }
                else
                {
                    iNQCount = 0;
                }
                i = 0;
                while (i < iNQCount)
                {
                    combo_FavoritePanel.Items.Add(ini.IniReadValue("NQFav", i.ToString()));
                    i++;
                }
                MessageBox.Show("Favorite Saved");
            }
        }



        private void frmSEProducts_FormClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            gbl_apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            gbl_filepath = Path.GetDirectoryName(gbl_apppath).ToString();
            if (File.Exists(gbl_filepath + "\\lvpanel.inuse"))
            {
                File.Delete(gbl_filepath + "\\lvpanel.inuse");
            }
         }

        private void btnRemove_Click(object sender, EventArgs e)
        { 
            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
             
            List<LVSelector3.clsQuestionsandAnswers> lstFavorites = new List<LVSelector3.clsQuestionsandAnswers>();
            LVSelector3.clsQuestionsandAnswers cFav = new LVSelector3.clsQuestionsandAnswers();
            int icount = 0;
            string strcount = ini.IniReadValue("NQFav", "Count");
            string strid = "";
            string strpart = "";
            int j=0;
            if (strcount != "")
            {
                icount = Convert.ToInt32(strcount);
                j=0;
                while (j<icount)
                {
                    strid=ini.IniReadValue("NQFav",j.ToString() );
                    strpart=ini.IniReadValue("NQFav_Type", j.ToString());
                    cFav = new LVSelector3.clsQuestionsandAnswers();
                    cFav.gbQuestion = strid;
                    cFav.gbAnswer = strpart;
                    lstFavorites.Add(cFav);
                    j++;
                }

            }
            if (combo_FavoritePanel.Text != "" && combo_FavoritePanel.Text != "Enter Name")
            {
                j = 0;
                bool found = false;
                while (j < lstFavorites.Count && !found)
                {
                    if (lstFavorites[j].gbQuestion == combo_FavoritePanel.Text)
                    {
                        found = true;
                    }
                    else
                    {
                        j++;
                    }
                }
                if (found)                lstFavorites.RemoveAt(j);

            }
            ini.IniWriteValue("NQFav", "Count", lstFavorites.Count.ToString());

            j = 0;
            foreach (LVSelector3.clsQuestionsandAnswers cfa in lstFavorites)
            {
                ini.IniWriteValue("NQFav", j.ToString(), cfa.gbQuestion);
                ini.IniWriteValue("NQFav_Type", j.ToString(), cfa.gbAnswer);
                j++;
            }
            combo_FavoritePanel.Items.Clear();
            combo_FavoritePanel.Text = "Enter Name"; 
//            this.combo_FavoritePanel.ForeColor = System.Drawing.Color.Silver;
            string strNQCount = "";
            int iNQCount = 0;
            int i = 0;
            strNQCount = ini.IniReadValue("NQFav", "Count");
            if (strNQCount != "")
            {
                try
                {
                    iNQCount = Convert.ToInt32(strNQCount);
                }
                catch
                {
                }
            }
            else
            {
                iNQCount = 0;
            }
            i = 0;
            while (i < iNQCount)
            {
                combo_FavoritePanel.Items.Add(ini.IniReadValue("NQFav", i.ToString()));
                i++;
            }
            MessageBox.Show("Favorite Removed");
            tab0_selected();
        }

    
   
   

        private void button2_Click_1(object sender, EventArgs e)
        {

            combo_ILineFavoritePanel.Text = "Enter Name";
//            combo_ILineFavoritePanel.ForeColor = System.Drawing.Color.Silver;
            tab2_selected();
           // Calc_values_Enclosure(sender, e);
        }

        private void btnILineSave_Click(object sender, EventArgs e)
        {
            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            int i = 0;
            bool found = false;
            if (combo_ILineFavoritePanel.Text != "" && combo_ILineFavoritePanel.Text!= "Enter Name")
            {
                i = 0;
                found = false;
                while (i < combo_ILineFavoritePanel.Items.Count && !found)
                {
                    if (combo_ILineFavoritePanel.Items[i].ToString() == combo_ILineFavoritePanel.Text) found = true;
                    i++;
                }
                if (!found)
                {
                    combo_ILineFavoritePanel.Items.Add(combo_ILineFavoritePanel.Text);

                    int ic = combo_ILineFavoritePanel.Items.Count - 1;
                    ini.IniWriteValue("ILineFav", ic.ToString(), combo_ILineFavoritePanel.Text);
                    ini.IniWriteValue("ILineFav_Type", ic.ToString(), strPanelPartID);
                    ini.IniWriteValue("ILineFav", "Count", combo_ILineFavoritePanel.Items.Count.ToString());
                }

                combo_ILineFavoritePanel.Items.Clear();
             //   combo_ILineFavoritePanel.Text = "";
                string strNQCount = "";
                int iNQCount = 0;
                i = 0;
                strNQCount = ini.IniReadValue("ILineFav", "Count");
                if (strNQCount != "")
                {
                    try
                    {
                        iNQCount = Convert.ToInt32(strNQCount);
                    }
                    catch
                    {
                    }
                }
                else
                {
                    iNQCount = 0;
                }
                i = 0;
                while (i < iNQCount)
                {
                    combo_ILineFavoritePanel.Items.Add(ini.IniReadValue("ILineFav", i.ToString()));
                    i++;
                }

                MessageBox.Show("Favorite Saved");
            }
        }

        private void combo_ILineFavoritePanel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            testplugin.clsRevitUtils clsRev = new testplugin.clsRevitUtils();

            bool found = false;
            int i = 0;
            string strCount = ini.IniReadValue("ILineFav", "Count");
            int iCount = Convert.ToInt32(strCount);
            string strValue = "";

            if (combo_ILineFavoritePanel.Text != "" && combo_ILineFavoritePanel.Text != "Enter Name")
            {

                lst_Filtered = lst_Solutions_Enclosure;
                unupdate_dimension(txtILineDepth, txtIlineWidth, txtILineHeight);
                i = 0;
                found = false;
                while (i < iCount && !found)
                {
                    strValue = ini.IniReadValue("ILineFav", i.ToString());
                    if (combo_ILineFavoritePanel.Text == strValue)
                    {
                        found = true;
                    }
                    else
                    {
                        i++;
                    }
                }

                if (found)
                {
                    strValue = ini.IniReadValue("ILineFav_Type", i.ToString());
                    List<ReadXLS.clsSEQF_Product> lstpartid = clsRev.get_list_of_rows_opt1(lst_Solutions_Enclosure, "Part_ID", strValue);
                    if (lstpartid.Count > 0)
                    {
                        List<ReadXLS.clsSEQF_Product> lstFields = clsRev.get_row(lstpartid[0].irownumber, lst_Solutions_Enclosure);
                        foreach (ReadXLS.clsSEQF_Product cp in lstFields)
                        {
                            found = false;
                            i = 0;
                            int kk = 0;
                            if (cp.column != null)
                            {
                                while (i < currentpanel.Controls.Count && !found)
                                {
                                    if (currentpanel.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")
                                    {
                                        System.Windows.Forms.GroupBox g = (GroupBox)currentpanel.Controls[i];
                                        kk = 0;
                                        if (g.Text == cp.column.ToString())
                                        {
                                            while (kk < g.Controls.Count && !found)
                                            {

                                                if (g.Controls[kk].GetType().ToString() == "System.Windows.Forms.ComboBox")
                                                {
                                                    System.Windows.Forms.ComboBox cc = (System.Windows.Forms.ComboBox)g.Controls[kk];
                                                    cc.Enabled = true;
                                                    cc.Text = cp.value.ToString();
                                                    found = true;
                                                }
                                                else if (g.Controls[kk].GetType().ToString() == "System.Windows.Forms.RadioButton")
                                                {
                                                    System.Windows.Forms.RadioButton rb = (RadioButton)g.Controls[kk];
                                                    if (rb.Text == cp.value)
                                                    {
                                                        rb.Checked = true;
                                                        found = true;
                                                    }
                                                }
                                                kk++;
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                }

            }
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            Switchboard_Selector.se_ini_file.IniFile ini = new Switchboard_Selector.se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");

            List<LVSelector3.clsQuestionsandAnswers> lstFavorites = new List<LVSelector3.clsQuestionsandAnswers>();
            LVSelector3.clsQuestionsandAnswers cFav = new LVSelector3.clsQuestionsandAnswers();
            int icount = 0;
            string strcount = ini.IniReadValue("ILineFav", "Count");
            string strid = "";
            string strpart = "";
            int j = 0;
            if (strcount != "")
            {
                icount = Convert.ToInt32(strcount);
                j = 0;
                while (j < icount)
                {
                    strid = ini.IniReadValue("LVTFav", j.ToString());
                    strpart = ini.IniReadValue("LVTFav_Type", j.ToString());
                    cFav = new LVSelector3.clsQuestionsandAnswers();
                    cFav.gbQuestion = strid;
                    cFav.gbAnswer = strpart;
                    lstFavorites.Add(cFav);
                    j++;
                }

            }
            if (combo_ILineFavoritePanel.Text != "" && combo_ILineFavoritePanel.Text != "Enter Favorite Name here")
            {
                j = 0;
                bool found = false;
                while (j < lstFavorites.Count && !found)
                {
                    if (lstFavorites[j].gbQuestion == combo_ILineFavoritePanel.Text)
                    {
                        found = true;
                    }
                    else
                    {
                        j++;
                    }
                }
                if (found) lstFavorites.RemoveAt(j);

            }
            ini.IniWriteValue("ILineFav", "Count", lstFavorites.Count.ToString());

            j = 0;
            foreach (LVSelector3.clsQuestionsandAnswers cfa in lstFavorites)
            {
                ini.IniWriteValue("ILineFav", j.ToString(), cfa.gbQuestion);
                ini.IniWriteValue("ILineFav_Type", j.ToString(), cfa.gbAnswer);
                j++;
            }
            combo_ILineFavoritePanel.Items.Clear();
            combo_ILineFavoritePanel.Text = "Enter Name";
  //         combo_ILineFavoritePanel.ForeColor = System.Drawing.Color.Silver;
            string strNQCount = "";
            int iNQCount = 0;
            int i = 0;
            strNQCount = ini.IniReadValue("ILineFav", "Count");
            if (strNQCount != "")
            {
                try
                {
                    iNQCount = Convert.ToInt32(strNQCount);
                }
                catch
                {
                }
            }
            else
            {
                iNQCount = 0;
            }
            i = 0;
            while (i < iNQCount)
            {
                combo_ILineFavoritePanel.Items.Add(ini.IniReadValue("ILineFav", i.ToString()));
                i++;
            }
            MessageBox.Show("Favorite Removed");
            tab2_selected();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            txtIlinePanelName.Text = txtIlinePanelName.Text + " 2";
        }

        private void btnLVTransformer_bottomIncoming_Click(object sender, EventArgs e)
        {

        }

  
      
        
       // private List<clsElements> SE_get_ele_active()
 
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

          //  string fmtStd = "Standard version:\n" +
          //                  "  major.minor.build.revision = {0}.{1}.{2}.{3}";

            Version std = new Version(1, 1, 0, 10);
            MessageBox.Show("Version : " + std.Major + "." + std.Minor + "." + std.Build + "." + std.Revision,"LayoutFAST");
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            try
            {

                System.Diagnostics.Process.Start("http://quotefast.schneider-electric.com/QuoteFast/QFHelp/prodhelp.php?ndx=40255&doc=bim&sn=150309");
            }
            catch
            {
                MessageBox.Show("Unable to launch http://quotefast.schneider-electric.com/QuoteFast/QFHelp/prodhelp.php?ndx=40255&doc=bim&sn=150309");
            }
        }

        private void btn3D_Interactive_Click(object sender, EventArgs e)
        {
            try
            {

                System.Diagnostics.Process.Start("http://www.schneider-electric.us/sites/us/en/support/product-support-resources/software-and-calculators/calculators-and-online-tools/kaon3D//surge-protective-devices/surgeloc.page");
            }
            catch
            {
                MessageBox.Show("Unable to launch http://www.schneider-electric.us/sites/us/en/support/product-support-resources/software-and-calculators/calculators-and-online-tools/kaon3D//surge-protective-devices/surgeloc.page");
            }
        }

        private void tutorialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            testplugin.Form1 f1 = new testplugin.Form1();
            f1.ShowDialog();
        }

        private void btnIlineInsert_Click(object sender, EventArgs e)
        {
            Database adb = ed.Document.Database;
            Document ActiveDoc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            db = ActiveDoc.Database;
            ed = ActiveDoc.Editor;
            //    iaDoc = acApp.ActiveDocument;
            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();
            string strDWG = "";
            if (lst_Filtered.Count > 0)
            {
                // this will be the symbol file
                strDWG = cRev.get_field(lst_Filtered[0].irownumber, lst_Filtered, "DWG");
                //             MessageBox.Show("Insert " + strDWG);
                // blref = iaDoc.ModelSpace.InsertBlock(pt, strstupidsymbol, 1, 1, 1, 0, System.Type.Missing);
                //   AddBlockTest(db,strDWG);
                BlockInsert_11(ActiveDoc, gbl_filepath + "\\Cloud Offline\\RFA\\" + strDWG, txtIlinePanelName.Text);

            }
        }



    }
}
