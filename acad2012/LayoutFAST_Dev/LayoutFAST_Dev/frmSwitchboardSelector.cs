﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Windows;

using MgdAcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using MgdAcDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcWindowsNS = Autodesk.AutoCAD.Windows;

namespace Switchboard_Selector
{
    public partial class frmSwitchboardSelector : System.Windows.Forms.Form
    {

        string gbl_apppath = "";
        string gbl_filepath = "";
        string gbl_xml = "\\Cloud Offline\\switchboard_2013.XML";

        bool blntab0 = true;
        bool blnAddToDelete = false;
        bool blnRequireFieldSelected = false;

        Button gblButtonBottom;
        Button gblButtonTop;


        int iMaxAmps =0;
        int iMaxSectionsAllowed = 25;
        int gbl_Max_Depth = 24;
        int iWidth_of_lineup = 0;
        int iSection = 1;
        int icurrenttabpage = 0;

        List<Switchboard_Selector.clsElements> lst_ele_active_se = new List<Switchboard_Selector.clsElements>();
        List<clsElements> lst_sections_elements = new List<clsElements>();
        
        List<ReadXLS.clsSEQF_Product> lst_Features = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_FeatureValues = new List<ReadXLS.clsSEQF_Product>();
        List<ReadXLS.clsSEQF_Product> lst_Product = new List<ReadXLS.clsSEQF_Product>(); 
        List<ReadXLS.clsSEQF_Product> lst_Solutions = new List<ReadXLS.clsSEQF_Product>();

        List<System.Windows.Forms.ComboBox> lstcmb = new List<System.Windows.Forms.ComboBox>();

        List<ReadXLS.clsGroupBoxes> lstGroupBox = new List<ReadXLS.clsGroupBoxes>();

        List<RadioButton> lstradio = new List<RadioButton>();

        List<clsSection> lstSections2Delete = new List<clsSection>();
        List<clsSection> lstSections = new List<clsSection>();

        System.Drawing.Point p_Section_location;

        string gbl_utilty_type = "";
        string gbl_ground_bus = "";
        string gbl_Short_Circuit_Rating = "";
        string gbl_Enclosure_Type = "";
        string gbl_distribution_system_rating = "";
        string gbl_system_ampacity = "";
        string gbl_bus_bar_material = "";
        TabPage tabpage_currenttabpage = null;



        Database db = HostApplicationServices.WorkingDatabase;
        Editor ed = MgdAcApplication.DocumentManager.MdiActiveDocument.Editor;

        private void frmSwitchboardSelector_FormClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            gbl_apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            gbl_filepath = Path.GetDirectoryName(gbl_apppath).ToString();
            if (File.Exists(gbl_filepath + "\\swbd.inuse"))
            {
                File.Delete(gbl_filepath + "\\swbd.inuse");
            }
        }

        public frmSwitchboardSelector()
        {

            gbl_apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            gbl_filepath = Path.GetDirectoryName(gbl_apppath).ToString();
            bool blnexitnow = false;
            testplugin.clsRevitUtils clsRevUtils = new testplugin.clsRevitUtils();

            int itab = 1;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();

            string strLineup = "";
            InitializeComponent();

            if (File.Exists(gbl_filepath + "\\swbd.inuse"))
            {
                DialogResult dialogResult = MessageBox.Show("Would you like to continue?", "LayoutFAST already started", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    //do something
                }
                else if (dialogResult == DialogResult.No)
                {
                    blnexitnow = true;
                }
            }
            else
            {
                File.Create(gbl_filepath + "\\swbd.inuse");
            }
             if (!blnexitnow)
             {
                 enable_tab_pages(tabPage8, false);
                 //  enable_tab_pages(tabPage1,false);
                 // enable_tab_pages(tabDevice,false);


                 p_Section_location.X = 0;
                 p_Section_location.Y = 00;

                 gbl_apppath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                 gbl_filepath = Path.GetDirectoryName(gbl_apppath).ToString();

                 se_ini_file.IniFile ini = new se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
                 if (File.Exists(gbl_filepath + gbl_xml))
                 {
                     tabpage_currenttabpage = tabControl4.TabPages[0];
                     load_xmlfile(gbl_filepath + gbl_xml);
                     load_questions();
                     if (!blnRequireFieldSelected) enable_section_buttons(false);


                     strLineup = ini.IniReadValue("Lineup", "Number");
                     txtLineupDescription.Text = "Switchboard Lineup " + strLineup;
                    // lst_sections_elements = SE_get_ele_active();
                     List<string> lst_lineup_names = get_lineup_names(lst_sections_elements);
                     //Build tabs
                     itab = 1;
                     foreach (string strlineupname in lst_lineup_names)
                     {
                         if (tabControl4.TabPages.Count < itab + 1)
                         {
                             tabControl4.TabPages.Add(strlineupname);
                         }
                         else
                         {
                             tabControl4.TabPages[itab].Text = strlineupname;
                         }
                         itab++;
                     }
                     //start on 2nd tab

                 }
                 else
                 {
                     MessageBox.Show("Database file does not exist");
                     blnexitnow = true;
                 }
             }
            
        }

        private List<string> get_lineup_names(List<clsElements> lstsectionelements)
        {
            List<string> lst_string=new List<string>();
            int i = 0;
            bool found = false;
            foreach (clsElements ce in lstsectionelements)
            {
                i = 0;
                found = false;
                while (i < lst_string.Count && !found)
                {
                    if (ce.strLineupDescription == lst_string[i]) found = true;
                    i++;
                }
                if (!found) lst_string.Add(ce.strLineupDescription);
            }
            return lst_string;
        }
        private void tabControl4_Selected(object sender, TabControlEventArgs e)
        {
            bool blneverythingok = false;
            string strLineup = "";
            se_ini_file.IniFile ini = new se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            tabpage_currenttabpage=e.TabPage;
            icurrenttabpage = e.TabPageIndex;
            if (e.TabPageIndex == 0)
            {
                //bubba
                blntab0 = true;
                enable_section_buttons(false);
                strLineup = ini.IniReadValue("Lineup", "Number");
                txtLineupDescription.Text = "Switchboard Lineup " + strLineup;
                btn_Insert_Lineup.Text = "Insert Lineup";
            }
            else
            {
                blntab0 = false;
                    enable_section_buttons(true);
                btn_Insert_Lineup.Text = "Update Lineup";
            }
            clear_lineup(tabpage_currenttabpage);
            blnAddToDelete = true;
            List<clsElements> lst_current_tabpage_sections = new List<clsElements>();
       //     lst_current_tabpage_sections = get_current_tabpage_sections(tabpage_currenttabpage, out blneverythingok);
            if (!blneverythingok)
            {
                tabpage_currenttabpage = tabControl4.TabPages[0];
                tabControl4.SelectedIndex = 0;                       
                blntab0 = true;
                enable_section_buttons(false);
                strLineup = ini.IniReadValue("Lineup", "Number");
                txtLineupDescription.Text = "Switchboard Lineup " + strLineup;
                btn_Insert_Lineup.Text = "Insert Lineup";
                clear_lineup(tabpage_currenttabpage);
                blnAddToDelete = true;
                lst_current_tabpage_sections = new List<clsElements>();
         
            }

            build_tabpage_lineup(lst_current_tabpage_sections);
            blnAddToDelete = false;

            //update left side questions
            int i = 0;
            GroupBox gg;
            System.Windows.Forms.ComboBox cb;
            int j = 0;
            string strq = "";
            if (e.TabPageIndex == 0)
            {
                load_questions();
            }
            else
            {
                while (i < panel_Sel_layout_Left.Controls.Count)
                {

                    if (panel_Sel_layout_Left.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")
                    {
                        gg = (System.Windows.Forms.GroupBox)panel_Sel_layout_Left.Controls[i];
                        j = 0;

                        while (j < gg.Controls.Count)
                        {

                            if (gg.Controls[j].GetType().ToString() == "System.Windows.Forms.ComboBox")
                            {
                                cb = (System.Windows.Forms.ComboBox)gg.Controls[j];

                                strq = cb.Name.Substring(3, cb.Name.Length - 3);
                                if (strq == "Ground Bus")
                                {
                                    cb.Text = gbl_ground_bus;
                                } if (strq == "Utility")
                                {
                                    cb.Text = gbl_utilty_type;
                                }
                                
                                if (strq == "Short Circuit Rating")
                                {
                                    cb.Text = gbl_Short_Circuit_Rating;
                                }

                                if (strq == "Enclosure Type")
                                {
                                    cb.Text = gbl_Enclosure_Type;
                                }
                                if (strq == "Distribution System Rating")
                                {
                                    cb.Text = gbl_distribution_system_rating;
                                }
                                if (strq == "Bus Bar Material")
                                {
                                    cb.Text = gbl_bus_bar_material;
                                }
                                if (strq == "System Ampacity")
                                {//bubba
                                    cb.Text = gbl_system_ampacity;
                                    try
                                    {
                                        iMaxAmps = Convert.ToInt32(gbl_system_ampacity);
                                    }
                                    catch
                                    {
                                        iMaxAmps = 5000;
                                    }
                                }

                            }  //if (gg.Controls[j].GetType().ToString() == "System.Windows.Forms.ComboBox")
                            else if (gg.Controls[j].GetType().ToString() == "System.Windows.Forms.RadioButton")
                            {
                                RadioButton rb = (System.Windows.Forms.RadioButton)gg.Controls[j];

                                if (rb.Text == gbl_distribution_system_rating || rb.Text == gbl_bus_bar_material)
                                {
                                    rb.Checked = true;
                                }

                            }
                            j++;
                        }  //while (j < gg.Controls.Count)

                    }  //if (panel_Sel_layout_Left.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")

                    i++;
                }  //while (i < panel_Sel_layout_Left.Controls.Count)
            }

        }

        private void build_tabpage_lineup(List<clsElements> List_of_some_sections)
        {
            string strCC = "";
            string strmf = "";
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();

            int irr = 0;
            string strSectionType = "";
  
            testplugin.clsRevitUtils clsRevUtils = new testplugin.clsRevitUtils();
            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            int irow = 0;
            string strw = "";
            string strAmps = "";
            List<string> lstamps = new List<string>();
            foreach (clsElements seELe in List_of_some_sections)
            {
                
                lstamps.Clear();
                
                txtLineupDescription.Text = seELe.strLineupDescription;
                List<ReadXLS.clsSEQF_Product> tl = new List<ReadXLS.clsSEQF_Product>();
                // tl = clsRevUtils.get_list_of_rows_opt3(lst_Solutions, "Type Catalog",strCC);
                tl = clsRevUtils.get_list_of_rows_opt3(lst_Solutions, "Part_ID",strCC);





                if (tl.Count > 0)
                {
                   
                    irr = tl[0].irownumber;
                    strSectionType = clsRevUtils.get_field(irr, lst_Solutions, "Section Type");
                    strAmps = clsRevUtils.get_field(irr, lst_Solutions, "Max Dev Amps");
                  tmp = clsRevUtils.get_list_of_rows(lst_Solutions, "Section Type", strSectionType,  "Part_ID",strCC);
                    
                    if (tmp.Count > 0)
                    {


                       strmf = clsRevUtils.get_field(irr, lst_Solutions, "Type");
                        tmp1 = clsRevUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", strSectionType);

                if (tmp1.Count > 0)
                {
                    iMaxAmps = Convert.ToInt32(gbl_system_ampacity);
                    tmp2 = clsRevUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);

                    lstamps = clsRevUtils.get_Max_Device_Amps(tmp2, iMaxAmps);
                }


                        irow = tmp[0].irownumber;

                        if (strSectionType == "Individually Mounted")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Mains", LayoutFAST_Dev.Properties.Resources.jb_SingleIMD,null, strw, lstamps);
                        }
                        //       else if (cls.strFamily.ToString() == "Feeders")
                        else if (strSectionType == "Stacked")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_stackedIMD, null, strw, lstamps);
                        }
                        else if (strSectionType == "Single ILine")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_SingleILine, null, strw, lstamps);
                        }
                        else if (strSectionType == "Double ILine")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_DoubleILine, null, strw, lstamps);
                        }
                        else if (strSectionType == "Metering")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Utilities", LayoutFAST_Dev.Properties.Resources.jb_meteronly, null, strw, lstamps);
                        }
                        else if (strSectionType == "Auxiliary")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Auxiliaries", LayoutFAST_Dev.Properties.Resources.jb_aux, null, strw, lstamps);
                        }
                        else if (strSectionType == "Combo")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Combos", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker, null, strw, lstamps);
                        }
                        else if (strSectionType == "Socket3")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Socket3s", LayoutFAST_Dev.Properties.Resources.jb_3meters, null, strw, lstamps);
                        }
                        else if (strSectionType == "Socket6")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Socket6s", LayoutFAST_Dev.Properties.Resources.jb_6meter, null, strw, lstamps);
                        }
                        else if (strSectionType == "Individually Mtd BP")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Individually Mtd BPs", LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc, null, strw, lstamps);
                        }
                        else if (strSectionType == "QMB")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "QMBs", LayoutFAST_Dev.Properties.Resources.jb_QMB, null, strw, lstamps);
                        }
                        else if (strSectionType == "STACKEDBP")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "STACKEDBPs", LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc, null, strw, lstamps);
                        }
                        else if (strSectionType == "ComboBP")
                        {
                            strw = clsRevUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "ComboBPs", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc, null, strw, lstamps);
                        }

                    }  //if (tmp.Count > 0)

                } //if (tl.Count > 0)


            }  //foreach (clsElements seELe in lst_ele_active_se)
        }

        private void btn_AuxiliarySection_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                strmf = "Auxiliaries";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "Auxiliary");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
       
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                    if (lstAmps.Count > 1)
                    {
                        strAmps = lstAmps[lstAmps.Count - 1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }

                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else if (strAmps != "-1")
                    {
       
                        tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Auxiliary", "Max Dev Amps", strAmps);

                        if (tmp3.Count > 0)
                        {
                            irow = tmp3[0].irownumber;
                            strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                            place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_aux, null, strw, lstAmps);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
 
         
        }

        private void btn_Clear_Lineup_Click(object sender, EventArgs e)
        {
            
            clear_lineup2(tabpage_currenttabpage);
        }

        private void btn_Duplicate_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            clsSection ccc = new clsSection(0, 0, 0, null, null, null, null, null, null,null,null,null);

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            
            int i = 0;
            int irow = 0;
            
            string strx = "";
            string strw = "";
            string stramps = "";
            List<string> lstamps = new List<string>();
            
            if (iSection<iMaxSectionsAllowed)
            {
            i = b.Name.IndexOf("__");

            if (i > 0)
            {
                strx = b.Name.Substring(i + 2, b.Name.Length - i - 2);
           
                foreach (clsSection cls in lstSections)
                {
                    if (cls.iSectionNumber.ToString() == strx)
                    {
                        ccc=cls;
                    }
                }
            
                if (ccc != null)
                {
                    tmp = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", ccc.strFamily.ToString(), "Type Catalog", ccc.strCatType.ToString());

                    if (tmp.Count > 0)
                    {
                        irow = tmp[0].irownumber;
                        stramps = clsRevitUtils.get_field(irow, lst_Solutions, "Section Amps");
                        lstamps.Clear();
                        lstamps.Add(stramps);
                        if (ccc.strSectionType.ToString() == "Individually Mounted")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Mains", LayoutFAST_Dev.Properties.Resources.jb_SingleIMD, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Stacked")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_stackedIMD, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Single ILine")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_SingleILine, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Double ILine")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_DoubleILine, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Metering")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Utilities", LayoutFAST_Dev.Properties.Resources.jb_meteronly, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Auxiliary")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Auxiliaries", LayoutFAST_Dev.Properties.Resources.jb_aux, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Combo")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Combos", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Socket3")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Socket3s", LayoutFAST_Dev.Properties.Resources.jb_3meters, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "Socket6")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Socket6s", LayoutFAST_Dev.Properties.Resources.jb_6meter, null, strw, lstamps);
                        }

                        else if (ccc.strSectionType.ToString() == "Individually Mtd BP")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "Individually Mtd BPs", LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "QMB")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "QMBs", LayoutFAST_Dev.Properties.Resources.jb_QMB, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "STACKEDBP")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "STACKEDBPs", LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc, null, strw, lstamps);
                        }
                        else if (ccc.strSectionType.ToString() == "ComboBP")
                        {
                            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
                            place_the_section(irow, "ComboBPs", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc, null, strw, lstamps);
                        }

                    }  //if (tmp.Count > 0)

                }  //if (ccc != null)


            }  //if (i > 0)
            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }

        private void btn_Incoming_Bottom_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.BackgroundImage == LayoutFAST_Dev.Properties.Resources.power)
            {
            }
            else
            {
                gblButtonBottom.BackgroundImage = LayoutFAST_Dev.Properties.Resources.power;
                gblButtonTop.BackgroundImage = LayoutFAST_Dev.Properties.Resources.power_off;
            }
        }

        private void btn_Incoming_top_Click(object sender, EventArgs e)
        {
            Button b1 = (Button)sender;
            if (b1.BackgroundImage == LayoutFAST_Dev.Properties.Resources.power)
            {
            }
            else
            {
                gblButtonTop.BackgroundImage = LayoutFAST_Dev.Properties.Resources.power;
                gblButtonBottom.BackgroundImage = LayoutFAST_Dev.Properties.Resources.power_off;
            }
        }

        private void btn_toRight_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();
            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<clsSection> lstTmp = new List<clsSection>();
            string strSectionType = "";
            Button b = (Button)sender;
            string strx = "0";
            int i = 0;
            i = b.Name.IndexOf("__");
            if (i > 0)
            {
                strx = b.Name.Substring(i + 2, b.Name.Length - i - 2);
                if (strx == lstSections.Count.ToString())
                {
                }
                else
                {
                    try
                    {
                        i = Convert.ToInt32(strx);
                        lstSections[i-1].iSectionNumber = lstSections[i-1].iSectionNumber + 1;
                        lstSections[i ].iSectionNumber = lstSections[i].iSectionNumber - 1;
                        lstTmp = lstSections.OrderBy(lp => lp.iSectionNumber).ToList();
                        lstSections.Clear();
                        tabpage_currenttabpage.Controls.Clear();
                        lstSections.Clear();
                        iSection = 1;
                        p_Section_location.X = 0;
                        p_Section_location.Y = 0;
                        iWidth_of_lineup = 0;
                        foreach (clsSection cls in lstTmp)
                        {
                            tmp = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", cls.strFamily.ToString(), "Type Catalog", cls.strCatType.ToString());

                            if (tmp.Count > 0)
                            {

                                strSectionType = clsRevitUtils.get_field(tmp[0].irownumber, lst_Solutions, "Section Type");
                                //  if (cls.strFamily.ToString() == "Mains")
                                if (strSectionType == "Individually Mounted")
                                {
                                    get_info_to_place_section(cls, "Mains", LayoutFAST_Dev.Properties.Resources.jb_SingleIMD, tmp[0].irownumber);
                                    //       blnMainInLineup = true;
                                }
                                //       else if (cls.strFamily.ToString() == "Feeders")
                                else if (strSectionType == "Stacked")
                                {
                                    get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_stackedIMD, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Single ILine")
                                {
                                    get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_SingleILine, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Double ILine")
                                {
                                    get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_DoubleILine, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Metering")
                                {
                                    get_info_to_place_section(cls, "Utilities", LayoutFAST_Dev.Properties.Resources.jb_meteronly, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Auxiliary")
                                {
                                    get_info_to_place_section(cls, "Auxiliaries", LayoutFAST_Dev.Properties.Resources.jb_aux, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Combo")
                                {
                                    get_info_to_place_section(cls, "Combos", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Socket3")
                                {
                                    get_info_to_place_section(cls, "Socket3s", LayoutFAST_Dev.Properties.Resources.jb_3meters, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Socket6")
                                {
                                    get_info_to_place_section(cls, "Socket6s", LayoutFAST_Dev.Properties.Resources.jb_6meter, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Individually Mtd BP")
                                {
                                    get_info_to_place_section(cls, "Individually Mtd BPs", LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc, tmp[0].irownumber);
                                }
                                else if (strSectionType == "QMB")
                                {
                                    get_info_to_place_section(cls, "QMBs", LayoutFAST_Dev.Properties.Resources.jb_QMB, tmp[0].irownumber);
                                }
                                else if (strSectionType == "STACKEDBP")
                                {
                                    get_info_to_place_section(cls, "STACKEDBPs", LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc, tmp[0].irownumber);
                                }
                                else if (strSectionType == "ComboBP")
                                {
                                    get_info_to_place_section(cls, "ComboBPs", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc, tmp[0].irownumber);
                                }

                                
                         
                            }

                        }  //foreach (clsSection cls in lstTmp)
                    }
                    catch
                    {
                    }


                }
            }
        }
        private void btn_toLeft_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();
            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<clsSection> lstTmp = new List<clsSection>();
            string strSectionType = "";
            Button b = (Button)sender;
            string strx = "0";
            int i = 0;
            i = b.Name.IndexOf("__");
            if (i > 0)
            {
                strx = b.Name.Substring(i + 2, b.Name.Length - i - 2);
                if (strx == "1")
                {
                }
                else
                {
                    try
                    {
                        i = Convert.ToInt32(strx);
                        lstSections[i-1].iSectionNumber = lstSections[i-1].iSectionNumber - 1;
                        lstSections[i - 2].iSectionNumber = lstSections[i - 2].iSectionNumber + 1;
                        lstTmp = lstSections.OrderBy(lp => lp.iSectionNumber).ToList();
                        iWidth_of_lineup = 0;
                        tabpage_currenttabpage.Controls.Clear();
                        lstSections.Clear();
                        iSection = 1;
                        p_Section_location.X = 0;
                        p_Section_location.Y = 0;
                  
                        foreach (clsSection cls in lstTmp)
                        {
                            tmp = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", cls.strFamily.ToString(), "Type Catalog", cls.strCatType.ToString());

                            if (tmp.Count > 0)
                            {
                                strSectionType=clsRevitUtils.get_field(tmp[0].irownumber,lst_Solutions,"Section Type");
                                if (strSectionType == "Individually Mounted")
                                {
                                    get_info_to_place_section(cls, "Mains", LayoutFAST_Dev.Properties.Resources.jb_SingleIMD, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Stacked")
                                {
                                    get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_stackedIMD, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Single ILine")
                                {
                                    get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_SingleILine, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Double ILine")
                                {
                                    get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_DoubleILine, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Metering")
                                {
                                    get_info_to_place_section(cls, "Utilities", LayoutFAST_Dev.Properties.Resources.jb_meteronly, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Auxiliary")
                                {
                                    get_info_to_place_section(cls, "Auxiliaries", LayoutFAST_Dev.Properties.Resources.jb_aux, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Combo")
                                {
                                    get_info_to_place_section(cls, "Combos", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Socket3")
                                {
                                    get_info_to_place_section(cls, "Socket3s", LayoutFAST_Dev.Properties.Resources.jb_3meters, tmp[0].irownumber);
                                }
                                else if (strSectionType  == "Socket6")
                                {
                                    get_info_to_place_section(cls, "Socket6s", LayoutFAST_Dev.Properties.Resources.jb_6meter, tmp[0].irownumber);
                                }
                                else if (strSectionType == "Individually Mtd BP")
                                {
                                    get_info_to_place_section(cls, "Individually Mtd BPs", LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc, tmp[0].irownumber);
                                }
                                else if (strSectionType == "QMB")
                                {
                                    get_info_to_place_section(cls, "QMBs", LayoutFAST_Dev.Properties.Resources.jb_QMB, tmp[0].irownumber);
                                }
                                else if (strSectionType == "STACKEDBP")
                                {
                                    get_info_to_place_section(cls, "STACKEDBPs", LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc, tmp[0].irownumber);
                                }
                                else if (strSectionType == "ComboBP")
                                {
                                    get_info_to_place_section(cls, "ComboBPs", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc, tmp[0].irownumber);
                                }
                                
                            }

                        }  //foreach (clsSection cls in lstTmp)
                    }
                    catch
                    {
                    }
                  

                }
            }
        }

        private void btn_Remove_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            string strSectionType = "";
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            List<clsSection> lstTmp = new List<clsSection>();

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            
            int i = 0;
            string strx = "";
            gbl_Max_Depth = 24; 
            iWidth_of_lineup = 0;
            i = b.Name.IndexOf("__");
            if (i > 0)
            {
                strx = b.Name.Substring(i + 2, b.Name.Length - i - 2);

                // rebuild list
                lstTmp = new List<clsSection>();
                
                foreach (clsSection cls in lstSections)
                {
                    if (cls.iSectionNumber.ToString() != strx) lstTmp.Add(cls);
                }

            }
            
            tabpage_currenttabpage.Controls.Clear();
            lstSections.Clear();
            iSection = 1;
            p_Section_location.X = 0;
            p_Section_location.Y = 0;
       
            foreach (clsSection cls in lstTmp)
            {
                tmp = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", cls.strFamily.ToString(), "Type Catalog", cls.strCatType.ToString());

                if (tmp.Count > 0)
                {
                    strSectionType = clsRevitUtils.get_field(tmp[0].irownumber, lst_Solutions, "Section Type");
                    if (strSectionType == "Individually Mounted")
                    {
                        get_info_to_place_section(cls, "Mains", LayoutFAST_Dev.Properties.Resources.jb_SingleIMD, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Stacked")
                    {
                        get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_stackedIMD, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Single ILine")
                    {
                        get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_SingleILine, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Double ILine")
                    {
                        get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_DoubleILine, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Metering")
                    {
                        get_info_to_place_section(cls, "Utilities", LayoutFAST_Dev.Properties.Resources.jb_meteronly, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Auxiliary")
                    {
                        get_info_to_place_section(cls, "Auxiliaries", LayoutFAST_Dev.Properties.Resources.jb_aux, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Combo")
                    {
                        get_info_to_place_section(cls, "Combos", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Socket3")
                    {
                        get_info_to_place_section(cls, "Socket3s", LayoutFAST_Dev.Properties.Resources.jb_3meters, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Socket6")
                    {
                        get_info_to_place_section(cls, "Socket6s", LayoutFAST_Dev.Properties.Resources.jb_6meter, tmp[0].irownumber);
                    }
                    else if (strSectionType == "Individually Mtd BP")
                    {
                        get_info_to_place_section(cls, "Individually Mtd BPs", LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc, tmp[0].irownumber);
                    }
                    else if (strSectionType == "QMB")
                    {
                        get_info_to_place_section(cls, "QMBs", LayoutFAST_Dev.Properties.Resources.jb_QMB, tmp[0].irownumber);
                    }
                    else if (strSectionType == "STACKEDBP")
                    {
                        get_info_to_place_section(cls, "STACKEDBPs", LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc, tmp[0].irownumber);
                    }
                    else if (strSectionType == "ComboBP")
                    {
                        get_info_to_place_section(cls, "ComboBPs", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc, tmp[0].irownumber);
                    }
   
                }

            }  //foreach (clsSection cls in lstTmp)

        }

        private void btn_SingleBreaker_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                strmf = "Mains";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "Individually Mounted");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                    if (lstAmps.Count > 1)
                    {
                       strAmps = lstAmps[lstAmps.Count - 1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                    if (strAmps != "-1")
                    {
          
                        tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Individually Mounted", "Max Dev Amps", strAmps);

                        if (tmp3.Count > 0)
                        {
                            irow = tmp3[0].irownumber;
                            strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                            place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_SingleIMD, null, strw, lstAmps);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }

        private void btn_StackedFeeders_Click(object sender, EventArgs e)
        {

            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                strmf = "Feeders";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "Stacked");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);

                    if (lstAmps.Count > 1)
                    {
                        strAmps = lstAmps[lstAmps.Count - 1];
                   }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                    if (strAmps != "-1")
                    {
          
                        tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type","Stacked", "Max Dev Amps", strAmps);

                        if (tmp3.Count > 0)
                        {
                            irow = tmp3[0].irownumber;
                            strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                            place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_stackedIMD, null, strw, lstAmps);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }

        private void btn_SingleIline_Click(object sender, EventArgs e)
        {

            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                strmf = "Feeders";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "Single ILine");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
           
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                    if (lstAmps.Count > 1)
                    {
                        strAmps = lstAmps[lstAmps.Count - 1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                    if (strAmps != "-1")
                    {
                        tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type",  "Single ILine", "Max Dev Amps", strAmps);

                        if (tmp3.Count > 0)
                        {
                            irow = tmp3[0].irownumber;
                            strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                            place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_SingleILine, null, strw, lstAmps);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }



        }

        private void btn_DoubleIline_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();
            
            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";
            
            if (iSection<25)
            {
                strmf = "Feeders";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "Double ILine");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);

                    if (lstAmps.Count > 1)
                    {
                        strAmps = lstAmps[lstAmps.Count - 1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                    if (strAmps != "-1")
                    {
                       
                        tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Double ILine", "Max Dev Amps", strAmps);

                        if (tmp3.Count > 0)
                        {
                            irow = tmp3[tmp3.Count-1].irownumber;
                            strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                            place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_DoubleILine, null, strw, lstAmps);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }
           
            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }

        private void btn_Meter_Section_Click(object sender, EventArgs e)
        {

            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "Utilities";

            if (iSection < 25)
            {
                if (gbl_utilty_type=="EUSERC" || gbl_utilty_type=="Non-EUSERC")
                {
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Section Type", "Metering", "Utility Type", gbl_utilty_type);


                if (tmp1.Count > 0)
                {
                    tmp= clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
           
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                    if (lstAmps.Count > 1)
                    {
                        strAmps=lstAmps[lstAmps.Count-1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                    if (strAmps != "-1")
                    {
                         tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Metering", "Max Dev Amps", strAmps);

                        if (tmp3.Count > 0)
                        {
                            irow = tmp3[0].irownumber;
                            strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                            place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_meteronly, null, strw,lstAmps);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

                }
                else
                {
                    MessageBox.Show("A type of Utility needs to be selected in left panel");
                }
            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }

      
        }

        private void btn_Specifications_Click(object sender, EventArgs e)
        {
            if (lst_Solutions.Count > 0)
            {
                launch_webpage(2, lst_Solutions, "Specification Link");
            }
            else
            {
                MessageBox.Show("Unable to launch Specification link");
            }
        }

        private void btn_Diget_Click(object sender, EventArgs e)
        {
            if (lst_Solutions.Count > 0)
            {
                launch_webpage(2, lst_Solutions, "DigestPlus");
            }
            else
            {
                MessageBox.Show("Unable to launch Digest link");
            }
        }

        private void btn_3dView_Click(object sender, EventArgs e)
        {
            launch_webpage(2, lst_Solutions, "3D Link");
        }

        private void btn_Insert_Lineup_Click(object sender, EventArgs e)
        {
            string strType = "";
            double dblWidth = 0;
            int i = 0;
            Database adb = ed.Document.Database;
            Document ActiveDoc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            db = ActiveDoc.Database;
            ed = ActiveDoc.Editor;
            //    iaDoc = acApp.ActiveDocument;
            testplugin.clsRevitUtils cRev = new testplugin.clsRevitUtils();
            string strDWG = "";
            int irow = 0;

            double dblDepth = 2;
            double dDepth = 24;
            i = 0;

            string strDepth = "24";

            string strxdepth = "24";
            while (i < lstSections.Count)
            {
                if (lstSections[i].dblDepth > dDepth) dDepth = lstSections[i].dblDepth;
                i++;
            }

            if (gbl_Enclosure_Type == "3R - Outdoor")
            {
                strxdepth = (dDepth + 13).ToString();
                dblDepth = Convert.ToDouble(dDepth + 13) / 12;
            }
            else
            {
                strxdepth = dDepth.ToString();
                dblDepth = Convert.ToDouble(dDepth) / 12;
            }



            PromptPointResult ppr = ed.GetPoint("\nSpecify insertion point: ");
            if (ppr.Status != PromptStatus.OK) return;
            Point3d p3_insert=ppr.Value;
            i = 0;
            while (i < lstSections.Count)
            {
                strType = lstSections[i].strCatType;
                dblWidth = lstSections[i].dblWidth;
                 List<ReadXLS.clsSEQF_Product> lsttmp = cRev.get_list_of_rows_opt1(lst_Solutions, "Part_ID", lstSections[i].strPartID);
                if (lsttmp.Count > 0)
                {
                    irow = lsttmp[0].irownumber;
                }
                else
                {
                    irow = 0; 
                }
                strDWG = cRev.get_field(irow, lst_Solutions, "DWG");
                strDWG = findcorrectdwg(strDWG, strxdepth);
                BlockInsert_11(ActiveDoc, gbl_filepath + "\\Cloud Offline\\RFA\\" + strDWG, strType,p3_insert);
                p3_insert = new Point3d(p3_insert.X + dblWidth, p3_insert.Y, p3_insert.Z);
                
                i++;
            }


        }
        private string findcorrectdwg(string strDWG,string strxdepth)
        {
            string strResult = strDWG;
            string strFileDepth = "";
            string strFilenoext = "";
            string strTmp = "";
            string strTDepth = "";
            string strFileExt = "";
            string strTmpDWG = "";
            if (strDWG != "")
            {
                if (strDWG.Length > 2)
                {
                    strFilenoext = Path.GetFileNameWithoutExtension(strDWG);
                    if (strFilenoext.Length > 2)
                    {
                        strFileExt = Path.GetExtension(strDWG);
                        strTmp = strFilenoext.Substring(0, strFilenoext.Length - 2);
                        strTDepth = strFilenoext.Substring(strFilenoext.Length - 2, 2);
                   
                        strTmpDWG = strTmp + strxdepth + strFileExt;
                        if (File.Exists(gbl_filepath + "\\Cloud Offline\\RFA\\" + strTmpDWG))
                        {
                            strResult = strTmpDWG;
                        }

                    }

                }

            }
            return strResult;
        }
        public void BlockInsert_11(Document ActiveDoc, string blockQualifiedFileName, string strPanelname,Point3d p3_insert)
        {
            if (File.Exists(blockQualifiedFileName))
            {
                string blockName = Path.GetFileNameWithoutExtension(blockQualifiedFileName);

                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForRead);
                    if (!bt.Has(blockName))
                    {
                        Database tmpDb = new Database(false, true);
                        tmpDb.ReadDwgFile(blockQualifiedFileName, System.IO.FileShare.Read, true, "");
                        // add the block to the ActiveDrawing blockTable

                        using (DocumentLock docLock = ActiveDoc.LockDocument())
                        {
                            db.Insert(blockName, tmpDb, true);
                        }
                    }

                  
                    using (DocumentLock docLock = ActiveDoc.LockDocument())
                    {
                        BlockTableRecord btr = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);
                        BlockReference br = new BlockReference(Point3d.Origin, bt[blockName]);

                        br.TransformBy(Matrix3d
                            .Displacement(p3_insert - Point3d.Origin)
                            .PreMultiplyBy(ed.CurrentUserCoordinateSystem));

                        btr.AppendEntity(br);
                        tr.AddNewlyCreatedDBObject(br, true);
                        ApplyAttibutes(db, tr, br, new List<string>(new string[] {
					"EQUIPMENT_NAME_PLATE_DESIGNATION"
				}), new List<string>(new string[] {
					strPanelname
				}));
                        tr.Commit();
                    }
                }
            }

              
          
        }

        private static void ApplyAttibutes(Database db, Transaction tr, BlockReference bref, List<string> listTags, List<string> listValues)
        {
            BlockTableRecord btr = (BlockTableRecord)tr.GetObject(bref.BlockTableRecord, OpenMode.ForRead);

            foreach (ObjectId attId in btr)
            {
                Entity ent = (Entity)tr.GetObject(attId, OpenMode.ForRead);
                if (ent is AttributeDefinition)
                {
                    AttributeDefinition attDef = (AttributeDefinition)ent;
                    AttributeReference attRef = new AttributeReference();

                    attRef.SetAttributeFromBlock(attDef, bref.BlockTransform);
                    bref.AttributeCollection.AppendAttribute(attRef);
                    tr.AddNewlyCreatedDBObject(attRef, true);
                    if (listTags.Contains(attDef.Tag))
                    {
                        int found = listTags.BinarySearch(attDef.Tag);
                        if (found >= 0)
                        {
                            attRef.TextString = listValues[found];
                            attRef.AdjustAlignment(db);
                        }
                    }

                }
            }
        }


        // 
        // Revit calls
        //

    

   
  
        private void Add_Section(int iwidth, int idepth, System.Drawing.Image im1, string strType, string strRFA, string strCatType, string strShortDesc, string strPartID, string strElementInstanceID, string strSectionAmps,string strSectionType,List<string> lstamps,string strMaxAmps)
        {
            clsSection cSec = new clsSection(0, 0, 0, null, null, null, null, null, null,null,null,null);
           // v120, 429
            int ipanelwidth = 140;
            int ipanelheight = 375;
    //        int ipanelwidth = 160;
    //        int ipanelheight = 528;
            int iwidth2 = iwidth * 2;

            Size sz = new Size();

            sz.Width = iwidth2;
            sz.Height = 180;
            cSec.iSectionNumber = iSection;
            cSec.dblWidth = iwidth;
            cSec.dblDepth = idepth;
            cSec.strPartID = strPartID;
            cSec.strFamily = strType;
            cSec.strRFA = strRFA;
            cSec.strCatType = strCatType;
            cSec.strShortDescription = strShortDesc;
            cSec.strInstance = strElementInstanceID;
            cSec.strSectionAmps = strSectionAmps;
            cSec.strSectionType = strSectionType;
            cSec.combovalue = strMaxAmps;
            lstSections.Add(cSec);
            if (blnAddToDelete) lstSections2Delete.Add(cSec);

            // builds the section panel on bottom of the screen

            // 
            // panel1
            // 
            System.Windows.Forms.Panel pnl_section = new System.Windows.Forms.Panel();
            tabpage_currenttabpage.Controls.Add(pnl_section);


            System.Windows.Forms.Panel pnl_section_top = new System.Windows.Forms.Panel();
            System.Windows.Forms.Panel pnl_section_bottom = new System.Windows.Forms.Panel();
            Button btnSectionPicture = new Button();

            pnl_section.Controls.Add(btnSectionPicture);
            pnl_section.Controls.Add(pnl_section_top);
            pnl_section.Controls.Add(pnl_section_bottom);

            pnl_section.Location = p_Section_location;
            pnl_section.Margin = new System.Windows.Forms.Padding(4);
            pnl_section.Name = "section_panel_" + iSection.ToString();
            pnl_section.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

            if (cSec.dblWidth <= 24)
            {
                pnl_section.Size = new System.Drawing.Size(ipanelwidth, ipanelheight);
            }
            else if (cSec.dblWidth <= 30)
            {
           //     ipanelwidth = ipanelwidth + 15;
                ipanelwidth = ipanelwidth + 10;
                pnl_section.Size = new System.Drawing.Size(ipanelwidth, ipanelheight);
            }
            else if (cSec.dblWidth <= 36)
            {
              //  ipanelwidth = ipanelwidth + 30;
                ipanelwidth = ipanelwidth + 20;
                pnl_section.Size = new System.Drawing.Size(ipanelwidth, ipanelheight);
            }
            else if (cSec.dblWidth<=42)
            {
             //   ipanelwidth = ipanelwidth + 45;
                ipanelwidth = ipanelwidth + 30;
                pnl_section.Size = new System.Drawing.Size(ipanelwidth, ipanelheight);
            }
            else if (cSec.dblWidth <= 48)
            {
              //  ipanelwidth = ipanelwidth + 60;
                ipanelwidth = ipanelwidth + 40;
                pnl_section.Size = new System.Drawing.Size(ipanelwidth, ipanelheight);
            }
            else
            {
              //  ipanelwidth = ipanelwidth + 90;
                ipanelwidth = ipanelwidth + 50;
                pnl_section.Size = new System.Drawing.Size(ipanelwidth, ipanelheight);
            }


 
            // 
            // btnSectionPicture
            // 
            btnSectionPicture.BackgroundImage = im1;
            btnSectionPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            btnSectionPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            btnSectionPicture.FlatAppearance.BorderSize = 0;
            btnSectionPicture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          //  btnSectionPicture.Location = new System.Drawing.Point(0, 86);
            btnSectionPicture.Location = new System.Drawing.Point(0, 72);
            btnSectionPicture.Margin = new System.Windows.Forms.Padding(4);
            btnSectionPicture.Name = "btnSectionPicture_" + iSection.ToString();
            btnSectionPicture.Size = new System.Drawing.Size(ipanelwidth, 241);
        //   btnSectionPicture.Size = new System.Drawing.Size(ipanelwidth, 369);  

            string strTip = "Section : " + iSection.ToString() + " \n" +
                "Section Type " + strSectionType.ToString();
            toolTip1.SetToolTip(btnSectionPicture, strTip);
           
            btnSectionPicture.UseVisualStyleBackColor = true;
           // 
            // pnl_section_top
            // 
            Label lbl_DeviceAmps = new Label();
            Label lbl_Section_Number = new Label();
            Button btn_Incoming_top = new Button();
            System.Windows.Forms.ComboBox cb = new System.Windows.Forms.ComboBox();

            if (iSection == 1) pnl_section_top.Controls.Add(btn_Incoming_top);
            pnl_section_top.Controls.Add(lbl_Section_Number);
            pnl_section_top.Controls.Add(lbl_DeviceAmps);
            pnl_section_top.Controls.Add(cb);

            pnl_section_top.Location = new System.Drawing.Point(0, 0);
            pnl_section_top.Margin = new System.Windows.Forms.Padding(4);
            pnl_section_top.Name = "section_panel_top_" + iSection.ToString();
//            pnl_section_top.Size = new System.Drawing.Size(ipanelwidth, 86);
            pnl_section_top.Size = new System.Drawing.Size(ipanelwidth, 73);


            if (iSection == 1)
            {
                // 
                // btn_Incoming_top
                // 
                btn_Incoming_top.BackgroundImage = LayoutFAST_Dev.Properties.Resources.power;
                btn_Incoming_top.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                btn_Incoming_top.Location = new System.Drawing.Point((ipanelwidth / 2) - 15, 21);
                btn_Incoming_top.Margin = new System.Windows.Forms.Padding(4);
                btn_Incoming_top.Name = "btn_Incoming_top_" + iSection.ToString();

                btn_Incoming_top.Click += new EventHandler(btn_Incoming_top_Click);
                btn_Incoming_top.Size = new System.Drawing.Size(30, 30);
                btn_Incoming_top.UseVisualStyleBackColor = true;
                gblButtonTop = btn_Incoming_top;
            }
            // 
            // lbl_Section_Number
            // 
            lbl_Section_Number.AutoSize = true;
           lbl_Section_Number.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
           lbl_Section_Number.Name = "lbl_Section_Number_" + iSection.ToString();
//           lbl_Section_Number.Size = new System.Drawing.Size(67, 17);
           lbl_Section_Number.Size = new System.Drawing.Size(67, 13);
            lbl_Section_Number.Text = "Section " + iSection.ToString() +" - "+ iwidth.ToString() + "w";
            lbl_Section_Number.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            lbl_Section_Number.Location = new System.Drawing.Point(ipanelwidth / 2 - lbl_Section_Number.Width / 2, 2);
            // 
            // pnl_section_bottom
            // 

            lbl_DeviceAmps.AutoSize = true;
            lbl_DeviceAmps.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            lbl_DeviceAmps.Name = "lbl_Device_Amps_" + iSection.ToString();
            if (((ipanelwidth/2)+10)<85)
            {
                lbl_DeviceAmps.Size = new System.Drawing.Size(62, 17);
                lbl_DeviceAmps.Text = "Max Dev A";
            }
            else
            {
            lbl_DeviceAmps.Size = new System.Drawing.Size(67, 17);
            lbl_DeviceAmps.Text = "Max Dev Amp";
            }
            lbl_DeviceAmps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lbl_DeviceAmps.Location = new System.Drawing.Point(2,55);

            cb.AutoSize = true;
            cb.Text = strMaxAmps;
           cb.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
           cb.Name = "comboboxAmps__" + iSection.ToString();
//           cb.Size = new System.Drawing.Size((ipanelwidth / 2) - 21, 17);
           if (((ipanelwidth / 2) + 10) < lbl_DeviceAmps.Size.Width)
           {
               //           cb.Location = new System.Drawing.Point((ipanelwidth / 2) + 10, 55);
               cb.Location = new System.Drawing.Point(lbl_DeviceAmps.Size.Width + 2 + 3, 55);
               cb.Size=new System.Drawing.Size(ipanelwidth-lbl_DeviceAmps.Size.Width-2-5,17);
         //      cb.Location = new System.Drawing.Point((ipanelwidth / 2)+3, 55);
         //      cb.Size = new System.Drawing.Size((ipanelwidth / 2) - 5, 17);
           }
           else
           {
               //           cb.Location = new System.Drawing.Point((ipanelwidth / 2) + 10, 55);

               cb.Location = new System.Drawing.Point(lbl_DeviceAmps.Size.Width + 2 + 3, 55);
               cb.Size = new System.Drawing.Size(ipanelwidth - lbl_DeviceAmps.Size.Width - 2 - 5, 17);
//               cb.Location = new System.Drawing.Point((ipanelwidth / 2), 55);
 //              cb.Size = new System.Drawing.Size((ipanelwidth / 2) -2, 17);
           }
           foreach (string sss in lstamps)
           {
               cb.Items.Add(sss);
              // cb.Text = sss;
           }

           cb.SelectedValueChanged += new EventHandler(combobox_SelectedValueChanged);
            Button btn_Duplicate = new Button();
            Button btn_Remove = new Button();
            Button btn_toLeft = new Button();
            Button btn_toRight = new Button();
            Label lbl_Size = new Label();
            Button btn_Incoming_Bottom = new Button();

            pnl_section_bottom.Controls.Add(btn_Duplicate);
            pnl_section_bottom.Controls.Add(btn_Remove);
            pnl_section_bottom.Controls.Add(btn_toLeft);
            pnl_section_bottom.Controls.Add(btn_toRight);
            if (iSection == 1) pnl_section_bottom.Controls.Add(btn_Incoming_Bottom);

            //0, 354
//            pnl_section_bottom.Location = new System.Drawing.Point(0, 456);
            pnl_section_bottom.Location = new System.Drawing.Point(0, 300);
            pnl_section_bottom.Margin = new System.Windows.Forms.Padding(4);
            pnl_section_bottom.Name = "Section_Panel_Bottom_" + iSection.ToString();
 //           pnl_section_bottom.Size = new System.Drawing.Size(ipanelwidth, 70
            pnl_section_bottom.Size = new System.Drawing.Size(ipanelwidth, 75);
     
            // 
            // btn_Duplicate
            // 
            btn_Duplicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            btn_Duplicate.Location = new System.Drawing.Point(3, 46);
            btn_Duplicate.Margin = new System.Windows.Forms.Padding(4);
            btn_Duplicate.Name = "btn_Duplicate__" + iSection.ToString();
            //btn_Duplicate.Size = new System.Drawing.Size(72, 31);
            btn_Duplicate.Size = new System.Drawing.Size(65, 27);
            //     54, 27
            btn_Duplicate.Text = "Duplicate";
            btn_Duplicate.UseVisualStyleBackColor = true;
            btn_Duplicate.Click += new EventHandler(btn_Duplicate_Click);
            // 
            // btn_Remove
            // 
            btn_Remove.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            btn_Remove.Location = new System.Drawing.Point(ipanelwidth -68, 46);
 //           btn_Remove.Location = new System.Drawing.Point(ipanelwidth - 75, 36);
            btn_Remove.Margin = new System.Windows.Forms.Padding(4);
            btn_Remove.Name = "btn_Remove__" + iSection.ToString();
          //  btn_Remove.Size = new System.Drawing.Size(71, 31);
            btn_Remove.Size = new System.Drawing.Size(65, 27);
            btn_Remove.Text = "Remove";
            btn_Remove.UseVisualStyleBackColor = true;
            btn_Remove.Click += new EventHandler(btn_Remove_Click);

            // 
            // button35
            // 
//            btn_toRight.Location = new System.Drawing.Point(ipanelwidth - 48, 5);
            btn_toRight.Location = new System.Drawing.Point(ipanelwidth - 32, 15);
            btn_toRight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            btn_toRight.Name = "btn_toRight__" + iSection.ToString();
          //  btn_toRight.Size = new System.Drawing.Size(39, 30);
            btn_toRight.Size = new System.Drawing.Size(29, 24);
            //   29, 24
            btn_toRight.TabIndex = 7;
            btn_toRight.Text = ">";
            btn_toRight.UseVisualStyleBackColor = true;
            btn_toRight.Click += new System.EventHandler(btn_toRight_Click);
            // 
            // button36
            // 
            btn_toLeft.Location = new System.Drawing.Point(3, 15);
            btn_toLeft.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            btn_toLeft.Name = "btn_toLeft__" + iSection.ToString();
           // btn_toLeft.Size = new System.Drawing.Size(39, 30);
            btn_toLeft.Size = new System.Drawing.Size(29, 24);
            btn_toLeft.TabIndex = 6;
            btn_toLeft.Text = "<";
            btn_toLeft.UseVisualStyleBackColor = true;
            btn_toLeft.Click += new System.EventHandler(btn_toLeft_Click);

            if (iSection == 1)
            {
                btn_Incoming_Bottom.BackgroundImage = LayoutFAST_Dev.Properties.Resources.power_off;
                btn_Incoming_Bottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                btn_Incoming_Bottom.Location = new System.Drawing.Point((ipanelwidth / 2) - 15, 15);
                btn_Incoming_Bottom.Margin = new System.Windows.Forms.Padding(4);
                btn_Incoming_Bottom.Name = "btn_Incoiming_Bottom_" + iSection.ToString();
                if (iSection != 1) btn_Incoming_Bottom.Visible = false;
                btn_Incoming_Bottom.Size = new System.Drawing.Size(30, 30);
                btn_Incoming_Bottom.UseVisualStyleBackColor = true;

                btn_Incoming_Bottom.Click += new EventHandler(btn_Incoming_Bottom_Click);
                gblButtonBottom = btn_Incoming_Bottom;
            }

            if (idepth > gbl_Max_Depth) gbl_Max_Depth = idepth;


            p_Section_location.X = p_Section_location.X + pnl_section.Width;

            iWidth_of_lineup = iWidth_of_lineup + iwidth;
            //if nema 3r add 13 inches
            if (gbl_Enclosure_Type == "3R - Outdoor")
            {
                int itmpdepth = gbl_Max_Depth + 13;
                lbl_Lineup_Dimensions.Text = "Total Foot Print = 91.5h x " + iWidth_of_lineup.ToString() + "w x " + itmpdepth.ToString() + "d";

            }
            else
            {
                lbl_Lineup_Dimensions.Text = "Total Foot Print = 91.5h x " + iWidth_of_lineup.ToString() + "w x " + gbl_Max_Depth.ToString() + "d";

            }


            iSection++;
        }


        private void clear_lineup(TabPage tb)
        {
            tb.Controls.Clear();

            lstSections.Clear();
            lstSections2Delete.Clear();
            iSection = 1;
            p_Section_location.X = 0;
            p_Section_location.Y = 0;
            if (tabControl4.TabPages[0]==tb)
            {
                btn_Insert_Lineup.Text = "Insert Lineup";
                enable_section_buttons(false);
            }
            else
            {
                btn_Insert_Lineup.Text = "Update Lineup";
                enable_section_buttons(true);
            }
            iMaxAmps = 0;
            gbl_Max_Depth = 24;
            lblLineupAmpacity.Text = iMaxAmps.ToString();
            iWidth_of_lineup = 0;
            lbl_Lineup_Dimensions.Text = "Total Foot Print = 91.5h x 0w x 24d";


        }
        private void clear_lineup2(TabPage tb)
        {
            int itab = 1;
            string strLineup = "";
            tb.Controls.Clear();

            lstSections.Clear();
            lstSections2Delete.Clear();
            iSection = 1;
            p_Section_location.X = 0;
            p_Section_location.Y = 0;
            if (tabControl4.TabPages[0]==tb)
            {
                btn_Insert_Lineup.Text = "Insert Lineup";
                enable_section_buttons(false);
            }
            else
            {
                btn_Insert_Lineup.Text = "Update Lineup";
                enable_section_buttons(true);
            }
            iMaxAmps = 0;
            gbl_Max_Depth = 24;
            lblLineupAmpacity.Text = iMaxAmps.ToString();
            iWidth_of_lineup = 0;
            lbl_Lineup_Dimensions.Text = "Total Foot Print = 91.5h x 0w x 24d";

            se_ini_file.IniFile ini = new se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            load_xmlfile(gbl_filepath + gbl_xml);
            load_questions();
            if (!blnRequireFieldSelected) enable_section_buttons(false);


            strLineup = ini.IniReadValue("Lineup", "Number");
            txtLineupDescription.Text = "Switchboard Lineup " + strLineup;
         //   lst_sections_elements = SE_get_ele_active();
            List<string> lst_lineup_names = get_lineup_names(lst_sections_elements);
            //Build tabs
            itab = 1;
            foreach (string strlineupname in lst_lineup_names)
            {
                if (tabControl4.TabPages.Count < itab + 1)
                {
                    tabControl4.TabPages.Add(strlineupname);
                }
                else
                {
                    tabControl4.TabPages[itab].Text = strlineupname;
                }
                itab++;
            }
        }

        private void enable_section_buttons(bool bln_enable_value)
        {
            panel_6Meters.Enabled = bln_enable_value;
            panel_auxiliary.Enabled = bln_enable_value;
            panel_doubleILine.Enabled = bln_enable_value;
            panel_MeterSection.Enabled = bln_enable_value;
            panel_single_section.Enabled = bln_enable_value;
            panel_singleILine.Enabled = bln_enable_value;
            panel_stacked.Enabled = bln_enable_value;
            panel_3high_Meter.Enabled = bln_enable_value;
            panel_combo.Enabled = bln_enable_value;
            panel_QMB.Enabled = bln_enable_value;
            panel_boltloc.Enabled = bln_enable_value;
            panel_StackedBoltLoc.Enabled = bln_enable_value;
            panel_utilityBP.Enabled = bln_enable_value;


            btn_Socket6.Enabled = bln_enable_value;
            btn_AuxiliarySection.Enabled = bln_enable_value;
            btn_DoubleIline.Enabled = bln_enable_value;
            btn_Meter_Section.Enabled = bln_enable_value;
            btn_SingleBreaker.Enabled = bln_enable_value;
            btn_SingleIline.Enabled = bln_enable_value;
            btn_StackedFeeders.Enabled = bln_enable_value;
            btn_Socket3.Enabled = bln_enable_value;
            btn_Combo.Enabled = bln_enable_value;
            btnQMB.Enabled = bln_enable_value;
            btnBoltLoc.Enabled = bln_enable_value;
            btnStackedSwitch.Enabled = bln_enable_value;
            btnUtilityBP.Enabled = bln_enable_value;
            
        }

        private void get_info_to_place_section(clsSection cls, string strType, System.Drawing.Image im, int irow)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            string strInstance = "";
            string strw = "";
            string strAmps = "";
            List<string> lstamps = new List<string>();

            if (cls.strInstance == null)
            {
                strInstance = null;
            }
            else
            {
                strInstance = cls.strInstance.ToString();
            }
            strw = clsRevitUtils.get_field(irow, lst_Solutions, "Width");
            strAmps = clsRevitUtils.get_field(irow, lst_Solutions, "Section Amps");
            lstamps.Add(strAmps);

            List<ReadXLS.clsSEQF_Product>  tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type",cls.strFamily, "Section Type", cls.strSectionType);

            if (tmp1.Count > 0)
            {
                List<ReadXLS.clsSEQF_Product> tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                List<string> lstAmps = clsRevitUtils.get_Max_Device_Amps (tmp, iMaxAmps);
                place_the_section(irow, strType, im, strInstance, strw, lstAmps);
            }
        }


        private void launch_help(string strURLLink)
        {

            if (strURLLink != "")
            {
                try
                {
                    System.Diagnostics.Process.Start(strURLLink);
                }
                catch
                {
                    MessageBox.Show("Could not launch webbrowswer");
                }
            }

        }


        private void launch_webpage(int irownumber, List<ReadXLS.clsSEQF_Product> tmplist, string strFieldName)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            string strTmp = clsRevitUtils.get_field(irownumber, tmplist, strFieldName);

            if (strTmp != "")
            {
                try
                {
                    System.Diagnostics.Process.Start(strTmp);
                }
                catch
                {
                    MessageBox.Show("Could not launch webbrowswer");
                }
            }

        }

        private void load_questions()
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            ReadXLS.clsGroupBoxes cGB = new ReadXLS.clsGroupBoxes();

            System.Windows.Forms.ComboBox cb = new System.Windows.Forms.ComboBox();

            int x = 10;
            int xg = 5;
            int y = 10;
            int yg = 10;

            RadioButton rb = new RadioButton();

            panel_Sel_layout_Left.Controls.Clear();

            foreach (ReadXLS.clsSEQF_Product cp in lst_Features)
            {
                try
                {
                    cGB = new ReadXLS.clsGroupBoxes();

                    if (cp.sheet == "Features")
                    {

                        if (cp.column == "Field_Type" && cp.value != "button")
                        {
                            GroupBox g = place_Groupbox(panel_Sel_layout_Left, cp.row, x, y);
                            cGB.gbQuestion = g;
                            lstGroupBox.Add(cGB);
                            xg = 10;
                            yg = 20;

                            if (cp.value == "select")
                            {
                                // create combobox
                                System.Windows.Forms.ComboBox cc = place_cmbbox(g, cp.row, "any", xg, yg, out yg);
                                List<ReadXLS.clsSEQF_Product> tmplst = lst_FeatureValues.Where(lp => lp.row == cp.row && lp.column == "Value").ToList();

                                foreach (ReadXLS.clsSEQF_Product cf in tmplst)
                                {
                                    try
                                    {
                                        //add to combobox list of answers
                                        clsRevitUtils.add_to_combo(cc, cf.value);
                                        List<ReadXLS.clsSEQF_Product> tmpdef = lst_FeatureValues.Where(lp => lp.irownumber == cf.irownumber && lp.column == "Is_Default").ToList();

                                        if (tmpdef.Count > 0)
                                        {
                                            if (tmpdef[0] != null)
                                            {
                                                if (tmpdef[0].value.ToString() == "y")
                                                {
                                                    cc.Text = cf.value.ToString();
                                                    if (cGB.gbQuestion.Text == "Ground Bus")
                                                    {
                                                        gbl_ground_bus = cf.value.ToString();
                                                    }

                                                    if (cGB.gbQuestion.Text == "Short Circuit Rating")
                                                    {
                                                        gbl_Short_Circuit_Rating = cf.value.ToString();
                                                    }

                                                    if (cGB.gbQuestion.Text == "Enclosure Type")
                                                    {
                                                        gbl_Enclosure_Type = cf.value.ToString();
                                                    }
                                                    if (cGB.gbQuestion.Text == "Distribution System Rating")
                                                    {
                                                        gbl_distribution_system_rating = cf.value.ToString();
                                                    }
                                                    if (cGB.gbQuestion.Text == "Bus Bar Material")
                                                    {
                                                        gbl_bus_bar_material = cf.value.ToString();
                                                    }
                                                    if (cGB.gbQuestion.Text == "Utility")
                                                    {
                                                        gbl_utilty_type = cf.value.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (System.Exception exp1)
                                    {
                                        MessageBox.Show(exp1.Message.ToString());
                                    }

                                }  //foreach (ReadXLS.clsSEQF_Product cf in tmplst)

                            }  //if (cp.value == "select")
                            else if (cp.value == "radio")
                            {
                                List<ReadXLS.clsSEQF_Product> tmplst = lst_FeatureValues.Where(lp => lp.row == cp.row && lp.column == "Value").ToList();

                                foreach (ReadXLS.clsSEQF_Product cf in tmplst)
                                {
                                    rb = place_radiobutton(g, cf.value, xg, yg, out yg);
                                     List<ReadXLS.clsSEQF_Product> tmpdef = lst_FeatureValues.Where(lp => lp.irownumber == cf.irownumber && lp.column == "Is_Default").ToList();

                                     if (tmpdef.Count > 0)
                                     {
                                         if (tmpdef[0] != null)
                                         {
                                             if (tmpdef[0].value.ToString() == "y")
                                             {
                                                 rb.Checked = true;
                                                 if (cGB.gbQuestion.Text == "Distribution System Rating")
                                                 {
                                                     gbl_distribution_system_rating = cf.value.ToString();
                                                 }
                                                 if (cGB.gbQuestion.Text == "Bus Bar Material")
                                                 {
                                                     gbl_bus_bar_material = cf.value.ToString();
                                                 }
                                             }
                                         }
                                     }
                                }
                            }

                            y = y + g.Size.Height + 5;

                        }  //if (cp.column == "Field_Type" && cp.value != "button")

                    }  //if (cp.sheet == "Features")
                }
                catch (System.Exception exp)
                {
                    MessageBox.Show(exp.Message.ToString());
                }
            }  //foreach (ReadXLS.clsSEQF_Product cp in lst_Features)

        }

        private void load_xmlfile(string strFile)
        {
            lst_Product = new List<ReadXLS.clsSEQF_Product>();
            lst_Features = new List<ReadXLS.clsSEQF_Product>();
            lst_FeatureValues = new List<ReadXLS.clsSEQF_Product>();
            lst_Solutions = new List<ReadXLS.clsSEQF_Product>();

            List<ReadXLS.clsSEQF_Product> lst_XML = new List<ReadXLS.clsSEQF_Product>();

            LayoutFAST_Dev.SE_QFProductED seqf = new LayoutFAST_Dev.SE_QFProductED();

            if (File.Exists(strFile))
            {
                lst_XML = seqf.XLS2XML_readRecord(strFile);
                foreach (ReadXLS.clsSEQF_Product clp in lst_XML)
                {
                    if (clp.sheet == "Product")
                        lst_Product.Add(clp);
                    else if (clp.sheet == "Features")
                        lst_Features.Add(clp);
                    else if (clp.sheet == "Feature Values")
                        lst_FeatureValues.Add(clp);
                    else if (clp.sheet.Contains("Solutions"))
                    {
                        lst_Solutions.Add(clp);
                    }

                }
            }
        }

        private System.Windows.Forms.ComboBox place_cmbbox(GroupBox g, string cQuestion, string strValue, int x, int y, out int yOut)
        {
            int ioffset = 9;

            System.Windows.Forms.ComboBox cmb = new System.Windows.Forms.ComboBox();

            cmb.Enabled = true;
            cmb.Size = new System.Drawing.Size(g.Width - x - ioffset, 13);
            cmb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));

            cmb.Location = new System.Drawing.Point(x, y);
            cmb.Name = "cmb" + cQuestion;
            cmb.SelectedValueChanged += new EventHandler(select_item);
            cmb.LostFocus+=new EventHandler(cmb_LostFocus);
            g.Controls.Add(cmb);
            lstcmb.Add(cmb);
            yOut = y;

            if (cmb.Location.Y + cmb.Size.Height + ioffset >= g.Size.Height)
            {
                g.Height = cmb.Location.Y + cmb.Size.Height + ioffset;
                yOut = cmb.Location.Y + cmb.Size.Height + ioffset;
            }

            return cmb;
        }

        private void cmb_LostFocus(object sender, EventArgs e)
        {
            System.Windows.Forms.ComboBox c = (System.Windows.Forms.ComboBox)sender;
            if (c.Text == "")
            {
                if (c.Name.Contains("System Ampacity"))
                {
                    clear_lineup(tabpage_currenttabpage);
                }
            }
        }
        private GroupBox place_Groupbox(System.Windows.Forms.Panel panelx, string strHeader, int x, int y)
        {
            GroupBox g = new GroupBox();

            g.Location = new System.Drawing.Point(x, y);
            g.Name = "gb_" + strHeader;
            g.Size = new System.Drawing.Size(panelx.Width - x - 30, 30);
            g.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            g.Text = strHeader;
            panelx.Controls.Add(g);
            return g;
        }

        private RadioButton place_radiobutton(GroupBox g, string strValue, int x, int y, out int yOut)
        {
            System.Windows.Forms.RadioButton cmb = new System.Windows.Forms.RadioButton();

            cmb.AutoSize = true;//
            cmb.Location = new System.Drawing.Point(x, y);
            cmb.Name = "rad" + g.Name + "_" + strValue;
            cmb.Size = new System.Drawing.Size(g.Width - x - 9, 13);
            cmb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));

            cmb.Text = strValue;
            cmb.Tag = strValue;
            cmb.Enabled = true;
            cmb.Click += new EventHandler(select_item_radio);
            cmb.UseVisualStyleBackColor = true;

            g.Controls.Add(cmb);
            lstradio.Add(cmb);
            yOut = y;

            if (cmb.Location.Y + cmb.Size.Height + 9 >= g.Size.Height)
            {
                g.Height = cmb.Location.Y + cmb.Size.Height + 9;
                yOut = cmb.Location.Y + cmb.Size.Height + 4;
            }

            return cmb;
        }
    
        private void place_the_section(int irow, string strType, System.Drawing.Image image, string strElementInstanceID, string strSectionWidth,List<string>lstamps)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int idepth = 0;
            int iwidth = 0;

            List<ReadXLS.clsSEQF_Product> tmplst = lst_Solutions.Where(lp => lp.irownumber == irow).ToList();

            string strCat = "";
            string strDepth = "";
            string strDesc = "";
            string strPartID = "";
            string strRfa = "";
            string strwidth = "";
            string strSectionAmps = "";
            string strSectionType = "";
            string strMaxAmps = "";

            if (strSectionWidth == "")
            {
                strwidth = clsRevitUtils.get_field(irow, tmplst, "Width");

                if (strwidth != "")
                    iwidth = Convert.ToInt32(strwidth);
                else
                    iwidth = 0;
            }
            else
            {
                iwidth = Convert.ToInt32(strSectionWidth);
            }

            strMaxAmps = clsRevitUtils.get_field(irow, tmplst, "Max Dev Amps");
            strDepth = clsRevitUtils.get_field(irow, tmplst, "Minimum Depth");

            if (strDepth != "")
                try
                {
                    idepth = Convert.ToInt32(strDepth);
                }
                catch
                {
                    idepth = 0;
                }
            else
                idepth = 0;

            strRfa = clsRevitUtils.get_field(irow, tmplst, "RFA File");
            strCat = clsRevitUtils.get_field(irow, tmplst, "Type Catalog");
            strPartID = clsRevitUtils.get_field(irow, tmplst, "Part_ID");
            strDesc = clsRevitUtils.get_field(irow, tmplst, "Short Description");
            strSectionAmps = clsRevitUtils.get_field(irow, tmplst, "Section Amps");
            strSectionType = clsRevitUtils.get_field(irow, tmplst, "Section Type");

            Add_Section(iwidth, idepth, image, strType, strRfa, strCat, strDesc, strPartID, strElementInstanceID, strSectionAmps, strSectionType, lstamps, strMaxAmps);
        }



      


        private void combobox_SelectedValueChanged(object sender, EventArgs e)
        {
            string strx="";
            int i=0;
            int i_sect_Number = 0;
            testplugin.clsRevitUtils clsRevit = new testplugin.clsRevitUtils();
            clsSection ccc = new clsSection(0, 0, 0, null, null, null, null, null, null,null,null,null);
            System.Windows.Forms.ComboBox cbSender;
            try
            {
                cbSender = (System.Windows.Forms.ComboBox)sender;
            }
            catch
            {
                cbSender = null;
            }

            if (cbSender != null)
            {
                i = cbSender.Name.IndexOf("__");
              
                strx = cbSender.Name.Substring(i + 2, cbSender.Name.Length - i - 2);
           
                foreach (clsSection cls in lstSections)
                {
                    if (cls.iSectionNumber.ToString() == strx)
                    {
                        i_sect_Number = cls.iSectionNumber;
                        ccc=cls;
                    }
                }
                int irow = 0;
                if (ccc != null)
                {
                    lstSections[i_sect_Number - 1].combovalue = cbSender.Text;
                    List<ReadXLS.clsSEQF_Product> lt = clsRevit.get_list_of_rows(lst_Solutions, "Section Type", ccc.strSectionType, "Max Dev Amps", cbSender.Text);
                    if (gbl_utilty_type == "EUSERC" || gbl_utilty_type == "Non-EUSERC")
                    {
                        List<ReadXLS.clsSEQF_Product> ltU = clsRevit.find_rows_with_this_field(lt, lst_Solutions, "Utility Type", gbl_utilty_type);
                        if (ltU.Count>0)
                        {
                           irow=ltU[0].irownumber;
                        }
                        else
                        {
                       irow   = lt[0].irownumber;
                        }

                    }
                    else
                    {
                       irow   = lt[0].irownumber;
                    }
                    List<ReadXLS.clsSEQF_Product> tmplst = lst_Solutions.Where(lp => lp.irownumber == irow).ToList();

                    string strCat = "";
                    string strDepth = "";
                    string strDesc = "";
                    string strPartID = "";
                    string strRfa = "";
                    string strwidth = "";
                    string strSectionAmps = "";
                    string strSectionType = "";
                    int iwidth = 0;
                    int idepth = 0;
                        strwidth = clsRevit.get_field(irow, tmplst, "Width");

                        if (strwidth != "")
                            iwidth = Convert.ToInt32(strwidth);
                        else
                            iwidth = 0;
                 

                    strDepth = clsRevit.get_field(irow, tmplst, "Minimum Depth");

                    if (strDepth != "")
                        try
                        {
                            idepth = Convert.ToInt32(strDepth);
                        }
                        catch
                        {
                            idepth = 0;
                        }
                    else
                        idepth = 0;

                    strRfa = clsRevit.get_field(irow, tmplst, "RFA File");
                    strCat = clsRevit.get_field(irow, tmplst, "Type Catalog");
                    strPartID = clsRevit.get_field(irow, tmplst, "Part_ID");
                    strDesc = clsRevit.get_field(irow, tmplst, "Short Description");
                    strSectionAmps = clsRevit.get_field(irow, tmplst, "Section Amps");
                    strSectionType = clsRevit.get_field(irow, tmplst, "Section Type");
                    lstSections[i_sect_Number-1].dblDepth = idepth;
                    lstSections[i_sect_Number - 1].dblWidth = iwidth;
                    lstSections[i_sect_Number - 1].strCatType = strCat;
                    lstSections[i_sect_Number - 1].strPartID = strPartID;
                    lstSections[i_sect_Number - 1].strRFA = strRfa;
                    lstSections[i_sect_Number - 1].strSectionAmps = cbSender.Text;
                    lstSections[i_sect_Number - 1].strSectionType = strSectionType;
                    lstSections[i_sect_Number - 1].strShortDescription = strDesc;


                    List<clsSection> lstTmp = new List<clsSection>();
                    foreach (clsSection ccx in lstSections)
                    {
                        lstTmp.Add(ccx);
                    }
                    List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
                    List<ReadXLS.clsSEQF_Product> tmp_combo = new List<ReadXLS.clsSEQF_Product>();

                              tabpage_currenttabpage.Controls.Clear();
                        lstSections.Clear();
                        iSection = 1;
                       p_Section_location.X = 0;
                        p_Section_location.Y = 0;
                        iWidth_of_lineup = 0;

                        gbl_Max_Depth = 24;
                        foreach (clsSection cls in lstTmp)
                        {
                            tmp = clsRevit.get_list_of_rows(lst_Solutions, "Type", cls.strFamily.ToString(), "Type Catalog", cls.strCatType.ToString());

                            if (tmp.Count > 0)
                            {
                                string comboinput = find_combo_box_value_for_current_section(cls);
                               tmp_combo = clsRevit.find_rows_with_this_field(tmp, lst_Solutions, "Max Dev Amps", cls.combovalue);
                                if (tmp_combo.Count > 0)
                                {

                                    strSectionType = clsRevit.get_field(tmp_combo[0].irownumber, lst_Solutions, "Section Type");
                                    if (strSectionType == "Individually Mounted")
                                    {
                                        get_info_to_place_section(cls, "Mains", LayoutFAST_Dev.Properties.Resources.jb_SingleIMD, tmp_combo[0].irownumber);
                                        //       blnMainInLineup = true;
                                    }
                                   else if (strSectionType == "Stacked")
                                    {
                                        get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_stackedIMD, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Single ILine")
                                    {
                                        get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_SingleILine, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Double ILine")
                                    {
                                        get_info_to_place_section(cls, "Feeders", LayoutFAST_Dev.Properties.Resources.jb_DoubleILine, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Metering")
                                    {
                                        get_info_to_place_section(cls, "Utilities", LayoutFAST_Dev.Properties.Resources.jb_meteronly, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Auxiliary")
                                    {
                                        get_info_to_place_section(cls, "Auxiliaries", LayoutFAST_Dev.Properties.Resources.jb_aux, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Combo")
                                    {
                                        get_info_to_place_section(cls, "Combos", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Socket3")
                                    {
                                        get_info_to_place_section(cls, "Socket3s", LayoutFAST_Dev.Properties.Resources.jb_3meters, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Socket6")
                                    {
                                        get_info_to_place_section(cls, "Socket6s", LayoutFAST_Dev.Properties.Resources.jb_6meter, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "Individually Mtd BP")
                                    {
                                        get_info_to_place_section(cls, "Individually Mtd BPs", LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "QMB")
                                    {
                                        get_info_to_place_section(cls, "QMBs", LayoutFAST_Dev.Properties.Resources.jb_QMB, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "STACKEDBP")
                                    {
                                        get_info_to_place_section(cls, "STACKEDBPs", LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc, tmp_combo[0].irownumber);
                                    }
                                    else if (strSectionType == "ComboBP")
                                    {
                                        get_info_to_place_section(cls, "ComboBPs", LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc, tmp_combo[0].irownumber);
                                    }
                                }
                            }

                        }


                }
            }
        }

        private string find_combo_box_value_for_current_section(clsSection cls)
        {
            string strcombovalue = "";


            return strcombovalue;
        }
        private void select_item(object sender, EventArgs e)
        {
            bool found = false;

            testplugin.clsRevitUtils clsRevUtils = new testplugin.clsRevitUtils();

            System.Windows.Forms.ComboBox cb = null;

            GroupBox gg = null;

            int i = 0;
            int j = 0;
            int k = 0;

            List<ReadXLS.clsSEQF_Product> tm = new List<ReadXLS.clsSEQF_Product>();

            string stra = "";
            string strq = "";

            while (i < panel_Sel_layout_Left.Controls.Count)
            {

                if (panel_Sel_layout_Left.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")
                {
                    gg = (System.Windows.Forms.GroupBox)panel_Sel_layout_Left.Controls[i];
                    j = 0;

                    while (j < gg.Controls.Count)
                    {

                        if (gg.Controls[j].GetType().ToString() == "System.Windows.Forms.ComboBox")
                        {
                            cb = (System.Windows.Forms.ComboBox)gg.Controls[j];

                            strq = cb.Name.Substring(3, cb.Name.Length - 3);
                            stra = cb.Text;
                            k = 0;
                            found = false;

                            while (k < lstGroupBox.Count && !found)
                            {

                                if (lstGroupBox[k].gbQuestion == gg)
                                {
                                    if (stra != lstGroupBox[k].gbAnswer)
                                    {
                                        lstGroupBox[k].gbAnswer = stra;
                                        found = true;

                                        if (lstGroupBox[k].gbQuestion.Text == "System Ampacity")
                                        {
                                            if (btn_Insert_Lineup.Text == "Update Lineup")
                                            {
                                            }
                                            else
                                            {
                                                clear_lineup(tabpage_currenttabpage);
                                                gbl_system_ampacity = stra;
                                                if (stra != "")
                                                {
                                                    iMaxAmps = Convert.ToInt32(stra);
                                                    lblLineupAmpacity.Text = stra;
                                                    List<ReadXLS.clsSEQF_Product> lstRequired = clsRevUtils.get_list_of_rows(lst_Features, "Field_Type", "select", "Required", "y");

                                                    if (lstRequired.Count > 0)
                                                    {
                                                        enable_section_buttons(true);
                                                        blnRequireFieldSelected = true;
                                                    }
                                                }  //if (stra != "")
                                            }

                                        }  //if (lstGroupBox[k].gbQuestion.Text == "System Ampacity")


                                        if (lstGroupBox[k].gbQuestion.Text == "Ground Bus")
                                        {
                                            gbl_ground_bus = stra;
                                        }

                                        if (lstGroupBox[k].gbQuestion.Text == "Short Circuit Rating")
                                        {
                                            gbl_Short_Circuit_Rating = stra;
                                        }

                                        if (lstGroupBox[k].gbQuestion.Text == "Enclosure Type")
                                        {
                                            gbl_Enclosure_Type = stra;
                                            if (gbl_Enclosure_Type == "3R - Outdoor")
                                            {
                                                int idepth = gbl_Max_Depth + 13;
                                                lbl_Lineup_Dimensions.Text = "Total Foot Print = 91.5h x " + iWidth_of_lineup.ToString() + "w x " + idepth.ToString() + "d";
                                            }
                                            else
                                            {
                                                lbl_Lineup_Dimensions.Text = "Total Foot Print = 91.5h x " + iWidth_of_lineup.ToString() + "w x " + gbl_Max_Depth.ToString() + "d";
                                            }
                                        }
                                        if (lstGroupBox[k].gbQuestion.Text == "Distribution System Rating")
                                        {
                                            gbl_distribution_system_rating = stra;
                                        }
                                        if (lstGroupBox[k].gbQuestion.Text == "Bus Bar Material")
                                        {
                                            gbl_bus_bar_material = stra;
                                        }



                                        if (lstGroupBox[k].gbQuestion.Text == "Utility")
                                        {
                                            gbl_utilty_type = stra;
                                            if (stra != "")
                                            {
                                                if (stra == "na")
                                                {
                                                    
                                                    btn_Meter_Section.Enabled = false;
                                                    btn_Socket3.Enabled = false;
                                                    btn_Socket6.Enabled = false;
                                                    btn_Combo.Enabled = false;

                                                }
                                                else
                                                {
                                               
                                                    btn_Meter_Section.Enabled = true;
                                                    btn_Socket3.Enabled = true;
                                                    if (stra == "EUSERC")
                                                    {
                                                        btn_Combo.Enabled = true;
                                                        btn_Socket6.Enabled = true;
                                                    }
                                                    else
                                                    {
                                                        btn_Socket6.Enabled = false;
                                                        btn_Combo.Enabled = false;
                                                    }
                                                }

                                            }  //if (stra != "")

                                        }  //if (lstGroupBox[k].gbQuestion.Text == "System Ampacity")
                                    }
                                }  //if (lstGroupBox[k].gbQuestion == gg)

                                k++;
                            }  //while (k < lstGroupBox.Count && !found)

                        }  //if (gg.Controls[j].GetType().ToString() == "System.Windows.Forms.ComboBox")

                        j++;
                    }  //while (j < gg.Controls.Count)

                }  //if (panel_Sel_layout_Left.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")

                i++;
            }  //while (i < panel_Sel_layout_Left.Controls.Count)

            if (blnRequireFieldSelected) enable_section_buttons(true);

        }

        private void select_item_radio(object sender, EventArgs e)
        {
            bool found = false;

            GroupBox gg = null;

            int i = 0;
            int j = 0;
            int k = 0;

            RadioButton rb = null;
            RadioButton rbSender = null;

            string stra = "";
            string strq = "";

            try
            {
                rbSender = (RadioButton)sender;
            }
            catch
            {
                rbSender = null;
            }

            if (rbSender != null)
            {
                found = false;

                while (i < panel_Sel_layout_Left.Controls.Count && !found)
                {

                    if (panel_Sel_layout_Left.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")
                    {
                        gg = (System.Windows.Forms.GroupBox)panel_Sel_layout_Left.Controls[i];
                        j = 0;

                        while (j < gg.Controls.Count && !found)
                        {

                            if (gg.Controls[j].GetType().ToString() == "System.Windows.Forms.RadioButton")
                            {
                                rb = (System.Windows.Forms.RadioButton)gg.Controls[j];

                                if (rb == rbSender)
                                {
                                    strq = rb.Name.Substring(3, rb.Name.Length - 3);
                                    stra = rb.Tag.ToString();
                                    k = 0;

                                    while (k < lstGroupBox.Count && !found)
                                    {

                                        if (lstGroupBox[k].gbQuestion == gg)
                                        {
                                            lstGroupBox[k].gbAnswer = stra;
                                            found = true;
                                            if (lstGroupBox[k].gbQuestion.Text.ToString() =="Distribution System Rating")
                                            {
                                                gbl_distribution_system_rating=stra;
                                            }
                                            if (lstGroupBox[k].gbQuestion.Text.ToString() == "Bus Bar Material")
                                            {
                                                gbl_bus_bar_material = stra;
                                            }

                                        }  //if (lstGroupBox[k].gbQuestion == gg)

                                        k++;
                                    }  //while (k < lstGroupBox.Count && !found)

                                }  //if (rb == rbSender)

                            }  //if (gg.Controls[j].GetType().ToString() == "System.Windows.Forms.RadioButton")

                            j++;
                        }  //while (j < gg.Controls.Count && !found)

                    }  //if (panel_Sel_layout_Left.Controls[i].GetType().ToString() == "System.Windows.Forms.GroupBox")

                    i++;
                }  //while (i < panel_Sel_layout_Left.Controls.Count && !found)

                if (blnRequireFieldSelected) enable_section_buttons(true);
            }  //if (rbSender != null)

        }

        private void enable_tab_pages(TabPage page, bool blnEnabled)
        {
            ((System.Windows.Forms.Control)page).Enabled = blnEnabled;
            ((System.Windows.Forms.Control)page).Visible = blnEnabled;
        }


 

        private void get_lineup_section(string strPValue, out int iSectionNumber, out string strLineup, out string strDesc)
        {
            int isec = 0;
            string strL = "";
            string strBay = "";
            int i = strPValue.IndexOf("~");
            int len = strPValue.Length;
            string strN = "";
            strL = strPValue.Substring(0, i);
            strN = strPValue.Substring(i + 1, len - i - 1);
            i = strN.IndexOf("~");
            len = strN.Length;
            strBay = strN.Substring(0, i);
            strDesc = strN.Substring(i + 1, len - i - 1);
            if (strN.Length > 0) isec = Convert.ToInt32(strBay);
            iSectionNumber = isec;
            strLineup = strL;
        }

        private void btn_Socket6_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;
            int i = 0;
            int ilastirow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strmaxiampy = "";
            string strAmps = "";
            string strw = "";
            string strmf = "";
            string struseSystemAmps = "";
            string strSysAmps = "";
            int iSysAmps = 0;
            int ioldSysamps = -1;
            int ii = 0;
            bool found = false;

            if (iSection < 25)
            {
                if (gbl_utilty_type=="EUSERC" || gbl_utilty_type=="Non-EUSERC")
                {
                strmf = "Socket6s";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Utility Type", gbl_utilty_type, "Section Type", "Socket6");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);

                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                    if (tmp.Count > 0)
                    {
                        struseSystemAmps = clsRevitUtils.get_field(tmp[0].irownumber, tmp, "Use System Amps");

                        if (struseSystemAmps == "Y")
                        {

                            i = 0;
                            ilastirow = -1;
                            while (i < tmp.Count)
                            {
                                if (tmp[i].irownumber != ilastirow)
                                {
                                    strmaxiampy = clsRevitUtils.get_field(tmp[i].irownumber, tmp, "Max Dev Amps");
                                    strSysAmps = clsRevitUtils.get_field(tmp[i].irownumber, tmp, "System Ampacity");
                                    try
                                    {
                                        iSysAmps = Convert.ToInt32(strSysAmps);
                                        if (iSysAmps <= iMaxAmps)
                                        {
                                            if (iSysAmps > ioldSysamps) ioldSysamps = iSysAmps;
                                        }
                                    }
                                    catch
                                    {
                                    }
                                    ii = 0;
                                    found = false;
                                    while (ii < lstAmps.Count && !found)
                                    {
                                        if (strmaxiampy == lstAmps[ii]) found = true;
                                        ii++;
                                    }
                                    if (!found) lstAmps.Add(strmaxiampy);

                                    ilastirow = tmp[i].irownumber;
                                }
                                i++;
                            }
                            strAmps = ioldSysamps.ToString();
                        }

                        else
                        {
                           // lstAmps = clsRevitUtils.get_Section_Amps(tmp, iMaxAmps);
                            if (lstAmps.Count > 1)
                            {
                                strAmps = lstAmps[lstAmps.Count - 1];
                            }
                            else if (lstAmps.Count == 1)
                            {
                                strAmps = lstAmps[0];
                            }
                            else
                            {
                                strAmps = "-1";
                            }
                        }

                        if (strAmps == "-2") //cancelled
                        {
                        }
                        else if (strAmps != "-1")
                        {
                            if (struseSystemAmps == "Y")
                            {
                                tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Socket6", "System Ampacity", strAmps);
                            }
                            else
                            {
                                tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Socket6", "Max Dev Amps", strAmps);
                            }
                            if (tmp3.Count > 0)
                            {
                                irow = tmp3[0].irownumber;
                                strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                                place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_6meter, null, strw, lstAmps);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Sections are available at the selected system ampacity");
                        }
                    }
                    
                    }   
                    else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("A type of Utility needs to be selected in left panel");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }

        private void btn_Socket3_Click(object sender, EventArgs e)
        {
           // bool found = false;
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;
            int i = 0;
            int ilastirow=0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strmaxiampy = "";
            string strAmps = "";
            string strw = "";
            string strmf = "";
            string struseSystemAmps = "";
            string strSysAmps = "";
            int iSysAmps = 0;
            int ioldSysamps = -1;
            int ii = 0;
            bool found = false;

            if (iSection < 25)
            {
                if (gbl_utilty_type=="EUSERC" || gbl_utilty_type=="Non-EUSERC")
                {
                    strmf = "Socket3s";
                    tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Utility Type", gbl_utilty_type, "Section Type", "Socket3");

                    if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                    if (tmp.Count > 0)
                    {
                        struseSystemAmps = clsRevitUtils.get_field(tmp[0].irownumber, tmp, "Use System Amps");

                        if (struseSystemAmps == "Y")
                        {

                            i = 0; 
                            ilastirow = -1;
                            while (i < tmp.Count)
                            {
                                if (tmp[i].irownumber != ilastirow)
                                {
                                    strmaxiampy = clsRevitUtils.get_field(tmp[i].irownumber, tmp, "Max Dev Amps");
                                    strSysAmps = clsRevitUtils.get_field(tmp[i].irownumber, tmp, "System Ampacity");
                                    try
                                    {
                                        iSysAmps = Convert.ToInt32(strSysAmps);
                                        if (iSysAmps <= iMaxAmps)
                                        {
                                            if (iSysAmps > ioldSysamps) ioldSysamps = iSysAmps;
                                        }
                                        ii = 0;
                                        found = false;
                                        while (ii < lstAmps.Count && !found)
                                        {
                                            if (strmaxiampy == lstAmps[ii]) found = true;
                                            ii++;
                                        }
                                        if (!found) lstAmps.Add(strmaxiampy);
                                    }
                                    catch
                                    {
                                    }
                                    ilastirow = tmp[i].irownumber;
                                }
                                i++;
                            }
                            strAmps = ioldSysamps.ToString();
                            
                        }

                        else
                        {
                         //   lstAmps = clsRevitUtils.get_Section_Amps(tmp, iMaxAmps);
                            if (lstAmps.Count > 1)
                            {
                                strAmps = lstAmps[lstAmps.Count - 1];
                            }
                            else if (lstAmps.Count == 1)
                            {
                                strAmps = lstAmps[0];
                            }
                            else
                            {
                                strAmps = "-1";
                            }
                        }

                        if (strAmps == "-2") //cancelled
                        {
                        }
                        else if (strAmps != "-1")
                        {
                            if (struseSystemAmps == "Y")
                            {
                                tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Socket3", "System Ampacity", strAmps);
                            }
                            else
                            {
                                tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Socket3", "Max Dev Amps", strAmps);
                            }
                            if (tmp3.Count > 0)
                            {
                                irow = tmp3[0].irownumber;
                                strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                                place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_3meters, null, strw, lstAmps);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Sections are available at the selected system ampacity");
                        }
                    }
                    }   
                    else
                    {
                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("A type of Utility needs to be selected in left panel");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
 
        }

        private void btn_Combo_Click(object sender, EventArgs e)
        {

            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                if (gbl_utilty_type=="EUSERC" || gbl_utilty_type=="Non-EUSERC")
                {
                    strmf = "Combos";
                    tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "Combo");

                    if (tmp1.Count > 0)
                    {
                        tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
        
                        lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                        if (lstAmps.Count > 1)
                        {
                            strAmps = lstAmps[lstAmps.Count - 1];
                        }
                        else if (lstAmps.Count == 1)
                        {
                            strAmps = lstAmps[0];
                        }
                        else
                        {
                            strAmps = "-1";
                        }

                        if (strAmps == "-2") //cancelled
                        {
                        }
                        else if (strAmps != "-1")
                        {
                            tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Combo", "Max Dev Amps", strAmps);

                            if (tmp3.Count > 0)
                            {
                                irow = tmp3[0].irownumber;
                                strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                                place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker, null, strw, lstAmps);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Sections are available at the selected system ampacity");
                        }
                    }   
                    else
                    {

                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("A type of Utility needs to be selected in left panel");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
 
        }

        private void btn_Catalog_Click(object sender, EventArgs e)
        {
            if (lst_Solutions.Count > 0)
            {
                launch_webpage(2, lst_Solutions, "Catalog Link");
            }
            else
            {
                MessageBox.Show("Unable to launch Catalog link");
            }
           
        }

   
  
        private void txtLineupDescription_TextChanged(object sender, EventArgs e)
        {
            if (blntab0)  tabControl4.TabPages[0].Text = txtLineupDescription.Text;
        }

        private void txtLineupDescription_LostFocus(object sender, EventArgs e)
        {
            int i=1;
            string strLineup = "";
            bool found=false;

            se_ini_file.IniFile ini = new se_ini_file.IniFile(gbl_filepath + "\\Switchboards.INI");
            if (icurrenttabpage == 0)
            {
                while (i < tabControl4.TabPages.Count && !found)
                {
                    if (tabControl4.TabPages[i].Text == txtLineupDescription.Text)
                    {
                        found = true;
                    }
                    else
                    {
                        i++;
                    }

                }
            }
            if (found)
            {
                MessageBox.Show("Lineup name already exist in Project.");
                strLineup = ini.IniReadValue("Lineup", "Number");
                txtLineupDescription.Text = "Switchboard Lineup " + strLineup;
            }
            else if (txtLineupDescription.Text == "")
            {
                MessageBox.Show("Lineup name needs a valid input.");
                strLineup = ini.IniReadValue("Lineup", "Number");
                txtLineupDescription.Text = "Switchboard Lineup " + strLineup;
            }
        }
        
        private void btnQMB_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                strmf = "QMBs";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "QMB");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                    if (lstAmps.Count > 1)
                    {
                       strAmps = lstAmps[lstAmps.Count - 1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                        if (strAmps != "-1")
                        {
                            tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "QMB", "Max Dev Amps", strAmps);

                            if (tmp3.Count > 0)
                            {
                                irow = tmp3[0].irownumber;
                                strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                                place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_QMB, null, strw, lstAmps);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Sections are available at the selected system ampacity");
                        }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }

        private void btnBoltLoc_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                strmf = "Individually Mtd BPs";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "Individually Mtd BP");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);

                    if (lstAmps.Count > 1)
                    {
                        strAmps = lstAmps[lstAmps.Count - 1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                        if (strAmps != "-1")
                        {
                            tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "Individually Mtd BP", "Max Dev Amps", strAmps);
                            if (tmp3.Count > 0)
                            {
                                irow = tmp3[0].irownumber;
                                strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                                place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc, null, strw, lstAmps);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Sections are available at the selected system ampacity");
                        }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }

        private void btnStackedSwitch_Click(object sender, EventArgs e)
        {
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                strmf = "STACKEDBPs";
                tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Section Type", "STACKEDBP");

                if (tmp1.Count > 0)
                {
                    tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
                    lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);

                    if (lstAmps.Count > 1)
                    {
                        strAmps = lstAmps[lstAmps.Count - 1];
                    }
                    else if (lstAmps.Count == 1)
                    {
                        strAmps = lstAmps[0];
                    }
                    else
                    {
                        strAmps = "-1";
                    }
                    if (strAmps == "-2") //cancelled
                    {
                    }
                    else
                        if (strAmps != "-1")
                        {
                
                            tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "STACKEDBP", "Max Dev Amps", strAmps);
                            if (tmp3.Count > 0)
                            {
                                irow = tmp3[0].irownumber;
                                strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                                place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc, null, strw, lstAmps);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Sections are available at the selected system ampacity");
                        }
                }
                else
                {
                    MessageBox.Show("No Sections are available at the selected system ampacity");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }
        }


        private void btnUtilityBP_Click(object sender, EventArgs e)
        {
            
            testplugin.clsRevitUtils clsRevitUtils = new testplugin.clsRevitUtils();

            int irow = 0;

            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp2 = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> tmp3 = new List<ReadXLS.clsSEQF_Product>();

            List<string> lstAmps = new List<string>();

            string strAmps = "";
            string strw = "";
            string strmf = "";

            if (iSection < 25)
            {
                if (gbl_utilty_type=="EUSERC" || gbl_utilty_type=="Non-EUSERC")
                {
                    strmf = "ComboBPs";
                    tmp1 = clsRevitUtils.get_list_of_rows(lst_Solutions, "Type", strmf, "Utility Type", gbl_utilty_type);
            
                    if (tmp1.Count > 0)
                    {
                        tmp = clsRevitUtils.get_list_of_rows_with_irownumber(tmp1, lst_Solutions);
              
                        lstAmps = clsRevitUtils.get_Max_Device_Amps(tmp, iMaxAmps);
                        if (lstAmps.Count > 1)
                        {
                            strAmps = lstAmps[lstAmps.Count - 1];
                      }
                        else if (lstAmps.Count == 1)
                        {
                            strAmps = lstAmps[0];
                        }
                        else
                        {
                            strAmps = "-1";
                        }

                        if (strAmps == "-2") //cancelled
                        {
                        }
                        else if (strAmps != "-1")
                        {
                           tmp3 = clsRevitUtils.get_list_of_rows(tmp, "Section Type", "ComboBP", "Max Dev Amps", strAmps);

                            if (tmp3.Count > 0)
                            {
                                irow = tmp3[0].irownumber;
                                strw = clsRevitUtils.get_field(irow, tmp3, "Width");
                                place_the_section(irow, strmf, LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc, null, strw, lstAmps);
                            }
                        }
                        else
                        {
                            MessageBox.Show("No Sections are available at the selected system ampacity");
                        }
                    }   
                    else
                    {

                        MessageBox.Show("No Sections are available at the selected system ampacity");
                    }
                }
                else
                {
                    MessageBox.Show("A type of Utility needs to be selected in left panel");
                }

            }
            else
            {
                MessageBox.Show("Maximum number of sections is 25");
            }


        }

        private void panel_sel_layout_top_center_Paint(object sender, PaintEventArgs e)
        {

        }






        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Version std = new Version(1, 1, 0, 10);
            MessageBox.Show("Version : " + std.Major + "." + std.Minor + "." + std.Build + "." + std.Revision, "LayoutFAST");
    
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            if (lst_Solutions.Count > 0)
            {
                launch_help("http://quotefast.schneider-electric.com/QuoteFast/QFHelp/prodhelp.php?ndx=40254&doc=bim&sn=150272");
                
            }
            else
            {
                MessageBox.Show("Unable to launch help at http://quotefast.schneider-electric.com/QuoteFast/QFHelp/prodhelp.php?ndx=40254&doc=bim&sn=150272");
            }

        }

        private void tutorialToolStripMenuItem_Click(object sender, EventArgs e)
        {

            LayoutFAST_Dev.Form1sw f1 = new LayoutFAST_Dev.Form1sw();
            f1.ShowDialog();
        }

        private void panel_Sel_layout_Left_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel_sel_layout_all_Left_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
