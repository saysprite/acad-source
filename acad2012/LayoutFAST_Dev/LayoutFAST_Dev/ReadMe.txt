
========================================================================
		AutoCAD .NET Addin VS Project Created by AcadNetAddinWizard
		
		Updates can be checked on:
		http://spiderinnet1.typepad.com
========================================================================

The project supports the following options:

+ Project properties:
	Assembly name: LayoutFAST_Dev
	AutoCAD flavor/version: AutoCAD 2012 - English
	API lib path: C:\Program Files\Autodesk\AutoCAD 2012 - English
	Start AutoCAD: True
	Dwg to open: 
	Use AutoCAD COM interfaces: True
+ Application extension:
	Add one: False
	Class name: ExtApp
+ Command method:
	Add one: True
	Class name: clsLayoutFAST_Dev
	Command group: CmdGroup1
	Command name: LayoutFAST_Dev
	Help file name: LayoutFAST_Dev
	Help topic ID: Command1
	Document wide: True
	Application wide: False
	Run it when AutoCAD is launched from inside the debugger: True
	Print out some debugging information: True
	Command flags:
		Modal
