﻿namespace Switchboard_Selector
{
    partial class frmSwitchboardSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSwitchboardSelector));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SectionLayout = new System.Windows.Forms.TabPage();
            this.panel_Center_Tabs = new System.Windows.Forms.Panel();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.panel_Sel_layout_bottom = new System.Windows.Forms.Panel();
            this.btn_Clear_Lineup = new System.Windows.Forms.Button();
            this.btn_Send_Lineup = new System.Windows.Forms.Button();
            this.lbl_Lineup_Dimensions = new System.Windows.Forms.Label();
            this.btn_RequestDrawings = new System.Windows.Forms.Button();
            this.btn_Insert_Lineup = new System.Windows.Forms.Button();
            this.btn_3dView = new System.Windows.Forms.Button();
            this.btn_Save_to_Favorites = new System.Windows.Forms.Button();
            this.btn_Catalog = new System.Windows.Forms.Button();
            this.btn_Specifications = new System.Windows.Forms.Button();
            this.btn_Diget = new System.Windows.Forms.Button();
            this.panel_sel_layout_top_center = new System.Windows.Forms.Panel();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel_utilityBP = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.btnUtilityBP = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.panel_StackedBoltLoc = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.btnStackedSwitch = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.panel_boltloc = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.btnBoltLoc = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.panel_QMB = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQMB = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.panel_combo = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Combo = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.panel_3high_Meter = new System.Windows.Forms.Panel();
            this.lblLineupAmpacity = new System.Windows.Forms.Label();
            this.btn_Socket3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel_6Meters = new System.Windows.Forms.Panel();
            this.btn_Socket6 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.panel_single_section = new System.Windows.Forms.Panel();
            this.btn_SingleBreaker = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel_auxiliary = new System.Windows.Forms.Panel();
            this.btn_AuxiliarySection = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panel_stacked = new System.Windows.Forms.Panel();
            this.btn_StackedFeeders = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel_MeterSection = new System.Windows.Forms.Panel();
            this.btn_Meter_Section = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel_singleILine = new System.Windows.Forms.Panel();
            this.btn_SingleIline = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel_doubleILine = new System.Windows.Forms.Panel();
            this.btn_DoubleIline = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel_sel_layout_all_Left = new System.Windows.Forms.Panel();
            this.pnl_Sel_Layout_left_bottom = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button50 = new System.Windows.Forms.Button();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.panel_Sel_layout_Left = new System.Windows.Forms.Panel();
            this.pnl_Sel_layout_Lineup_Designation = new System.Windows.Forms.Panel();
            this.txtLineupDescription = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tutorialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1.SuspendLayout();
            this.SectionLayout.SuspendLayout();
            this.panel_Center_Tabs.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.panel_Sel_layout_bottom.SuspendLayout();
            this.panel_sel_layout_top_center.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.panel_utilityBP.SuspendLayout();
            this.panel_StackedBoltLoc.SuspendLayout();
            this.panel_boltloc.SuspendLayout();
            this.panel_QMB.SuspendLayout();
            this.panel_combo.SuspendLayout();
            this.panel_3high_Meter.SuspendLayout();
            this.panel_6Meters.SuspendLayout();
            this.panel_single_section.SuspendLayout();
            this.panel_auxiliary.SuspendLayout();
            this.panel_stacked.SuspendLayout();
            this.panel_MeterSection.SuspendLayout();
            this.panel_singleILine.SuspendLayout();
            this.panel_doubleILine.SuspendLayout();
            this.panel_sel_layout_all_Left.SuspendLayout();
            this.pnl_Sel_Layout_left_bottom.SuspendLayout();
            this.pnl_Sel_layout_Lineup_Designation.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.SectionLayout);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1323, 703);
            this.tabControl1.TabIndex = 1;
            // 
            // SectionLayout
            // 
            this.SectionLayout.BackColor = System.Drawing.SystemColors.Control;
            this.SectionLayout.Controls.Add(this.panel_Center_Tabs);
            this.SectionLayout.Controls.Add(this.panel_Sel_layout_bottom);
            this.SectionLayout.Controls.Add(this.panel_sel_layout_top_center);
            this.SectionLayout.Controls.Add(this.panel1);
            this.SectionLayout.Location = new System.Drawing.Point(4, 22);
            this.SectionLayout.Name = "SectionLayout";
            this.SectionLayout.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.SectionLayout.Size = new System.Drawing.Size(1315, 677);
            this.SectionLayout.TabIndex = 1;
            this.SectionLayout.Text = "Section Layout";
            // 
            // panel_Center_Tabs
            // 
            this.panel_Center_Tabs.AutoScroll = true;
            this.panel_Center_Tabs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Center_Tabs.Controls.Add(this.tabControl4);
            this.panel_Center_Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Center_Tabs.Location = new System.Drawing.Point(203, 240);
            this.panel_Center_Tabs.Name = "panel_Center_Tabs";
            this.panel_Center_Tabs.Size = new System.Drawing.Size(1109, 409);
            this.panel_Center_Tabs.TabIndex = 13;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage9);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(0, 0);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(1107, 407);
            this.tabControl4.TabIndex = 24;
            this.tabControl4.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl4_Selected);
            // 
            // tabPage9
            // 
            this.tabPage9.AutoScroll = true;
            this.tabPage9.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage9.Size = new System.Drawing.Size(1099, 381);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "Lineup";
            // 
            // panel_Sel_layout_bottom
            // 
            this.panel_Sel_layout_bottom.Controls.Add(this.btn_Send_Lineup);
            this.panel_Sel_layout_bottom.Controls.Add(this.btn_RequestDrawings);
            this.panel_Sel_layout_bottom.Controls.Add(this.btn_3dView);
            this.panel_Sel_layout_bottom.Controls.Add(this.btn_Save_to_Favorites);
            this.panel_Sel_layout_bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_Sel_layout_bottom.Location = new System.Drawing.Point(203, 649);
            this.panel_Sel_layout_bottom.Name = "panel_Sel_layout_bottom";
            this.panel_Sel_layout_bottom.Size = new System.Drawing.Size(1109, 25);
            this.panel_Sel_layout_bottom.TabIndex = 15;
            this.panel_Sel_layout_bottom.Visible = false;
            // 
            // btn_Clear_Lineup
            // 
            this.btn_Clear_Lineup.Location = new System.Drawing.Point(0, 617);
            this.btn_Clear_Lineup.Name = "btn_Clear_Lineup";
            this.btn_Clear_Lineup.Size = new System.Drawing.Size(194, 23);
            this.btn_Clear_Lineup.TabIndex = 63;
            this.btn_Clear_Lineup.Text = "New LineUp";
            this.btn_Clear_Lineup.UseVisualStyleBackColor = true;
            this.btn_Clear_Lineup.Click += new System.EventHandler(this.btn_Clear_Lineup_Click);
            // 
            // btn_Send_Lineup
            // 
            this.btn_Send_Lineup.Location = new System.Drawing.Point(562, 6);
            this.btn_Send_Lineup.Name = "btn_Send_Lineup";
            this.btn_Send_Lineup.Size = new System.Drawing.Size(77, 21);
            this.btn_Send_Lineup.TabIndex = 29;
            this.btn_Send_Lineup.Text = "Send LineUp";
            this.btn_Send_Lineup.UseVisualStyleBackColor = true;
            this.btn_Send_Lineup.Visible = false;
            // 
            // lbl_Lineup_Dimensions
            // 
            this.lbl_Lineup_Dimensions.AutoSize = true;
            this.lbl_Lineup_Dimensions.Location = new System.Drawing.Point(0, 536);
            this.lbl_Lineup_Dimensions.Name = "lbl_Lineup_Dimensions";
            this.lbl_Lineup_Dimensions.Size = new System.Drawing.Size(192, 13);
            this.lbl_Lineup_Dimensions.TabIndex = 28;
            this.lbl_Lineup_Dimensions.Text = "Total Foot Print = 91.5h x 10-0\"w x 36d";
            // 
            // btn_RequestDrawings
            // 
            this.btn_RequestDrawings.Location = new System.Drawing.Point(235, 28);
            this.btn_RequestDrawings.Name = "btn_RequestDrawings";
            this.btn_RequestDrawings.Size = new System.Drawing.Size(102, 23);
            this.btn_RequestDrawings.TabIndex = 26;
            this.btn_RequestDrawings.Text = "Request Drawings";
            this.btn_RequestDrawings.UseVisualStyleBackColor = true;
            this.btn_RequestDrawings.Visible = false;
            // 
            // btn_Insert_Lineup
            // 
            this.btn_Insert_Lineup.Location = new System.Drawing.Point(0, 643);
            this.btn_Insert_Lineup.Name = "btn_Insert_Lineup";
            this.btn_Insert_Lineup.Size = new System.Drawing.Size(194, 23);
            this.btn_Insert_Lineup.TabIndex = 27;
            this.btn_Insert_Lineup.Text = "Insert Lineup";
            this.btn_Insert_Lineup.UseVisualStyleBackColor = true;
            this.btn_Insert_Lineup.Click += new System.EventHandler(this.btn_Insert_Lineup_Click);
            // 
            // btn_3dView
            // 
            this.btn_3dView.Location = new System.Drawing.Point(26, 11);
            this.btn_3dView.Name = "btn_3dView";
            this.btn_3dView.Size = new System.Drawing.Size(60, 23);
            this.btn_3dView.TabIndex = 0;
            this.btn_3dView.Text = "3D View";
            this.btn_3dView.UseVisualStyleBackColor = true;
            this.btn_3dView.Visible = false;
            this.btn_3dView.Click += new System.EventHandler(this.btn_3dView_Click);
            // 
            // btn_Save_to_Favorites
            // 
            this.btn_Save_to_Favorites.Location = new System.Drawing.Point(562, 26);
            this.btn_Save_to_Favorites.Name = "btn_Save_to_Favorites";
            this.btn_Save_to_Favorites.Size = new System.Drawing.Size(94, 25);
            this.btn_Save_to_Favorites.TabIndex = 62;
            this.btn_Save_to_Favorites.Text = "Save to Favorite";
            this.btn_Save_to_Favorites.UseVisualStyleBackColor = true;
            this.btn_Save_to_Favorites.Visible = false;
            // 
            // btn_Catalog
            // 
            this.btn_Catalog.Location = new System.Drawing.Point(0, 552);
            this.btn_Catalog.Name = "btn_Catalog";
            this.btn_Catalog.Size = new System.Drawing.Size(64, 23);
            this.btn_Catalog.TabIndex = 23;
            this.btn_Catalog.Text = "Catalog";
            this.btn_Catalog.UseVisualStyleBackColor = true;
            this.btn_Catalog.Click += new System.EventHandler(this.btn_Catalog_Click);
            // 
            // btn_Specifications
            // 
            this.btn_Specifications.Location = new System.Drawing.Point(117, 552);
            this.btn_Specifications.Name = "btn_Specifications";
            this.btn_Specifications.Size = new System.Drawing.Size(77, 23);
            this.btn_Specifications.TabIndex = 24;
            this.btn_Specifications.Text = "Specification";
            this.btn_Specifications.UseVisualStyleBackColor = true;
            this.btn_Specifications.Click += new System.EventHandler(this.btn_Specifications_Click);
            // 
            // btn_Diget
            // 
            this.btn_Diget.Location = new System.Drawing.Point(0, 581);
            this.btn_Diget.Name = "btn_Diget";
            this.btn_Diget.Size = new System.Drawing.Size(67, 23);
            this.btn_Diget.TabIndex = 25;
            this.btn_Diget.Text = "Digest";
            this.btn_Diget.UseVisualStyleBackColor = true;
            this.btn_Diget.Click += new System.EventHandler(this.btn_Diget_Click);
            // 
            // panel_sel_layout_top_center
            // 
            this.panel_sel_layout_top_center.AutoScroll = true;
            this.panel_sel_layout_top_center.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_sel_layout_top_center.Controls.Add(this.tabControl3);
            this.panel_sel_layout_top_center.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_sel_layout_top_center.Location = new System.Drawing.Point(203, 3);
            this.panel_sel_layout_top_center.Name = "panel_sel_layout_top_center";
            this.panel_sel_layout_top_center.Size = new System.Drawing.Size(1109, 237);
            this.panel_sel_layout_top_center.TabIndex = 12;
            this.panel_sel_layout_top_center.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_sel_layout_top_center_Paint);
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage7);
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(1100, 222);
            this.tabControl3.TabIndex = 24;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage7.Controls.Add(this.panel_utilityBP);
            this.tabPage7.Controls.Add(this.panel_StackedBoltLoc);
            this.tabPage7.Controls.Add(this.panel_boltloc);
            this.tabPage7.Controls.Add(this.panel_QMB);
            this.tabPage7.Controls.Add(this.panel_combo);
            this.tabPage7.Controls.Add(this.panel_3high_Meter);
            this.tabPage7.Controls.Add(this.panel_6Meters);
            this.tabPage7.Controls.Add(this.panel_single_section);
            this.tabPage7.Controls.Add(this.panel_auxiliary);
            this.tabPage7.Controls.Add(this.panel_stacked);
            this.tabPage7.Controls.Add(this.panel_MeterSection);
            this.tabPage7.Controls.Add(this.panel_singleILine);
            this.tabPage7.Controls.Add(this.panel_doubleILine);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage7.Size = new System.Drawing.Size(1092, 196);
            this.tabPage7.TabIndex = 0;
            this.tabPage7.Text = "Sections";
            // 
            // panel_utilityBP
            // 
            this.panel_utilityBP.Controls.Add(this.label10);
            this.panel_utilityBP.Controls.Add(this.btnUtilityBP);
            this.panel_utilityBP.Controls.Add(this.label12);
            this.panel_utilityBP.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_utilityBP.Location = new System.Drawing.Point(1001, 3);
            this.panel_utilityBP.Name = "panel_utilityBP";
            this.panel_utilityBP.Size = new System.Drawing.Size(83, 190);
            this.panel_utilityBP.TabIndex = 29;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(95, 2);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "5000";
            this.label10.Visible = false;
            // 
            // btnUtilityBP
            // 
            this.btnUtilityBP.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_MeteroverBoltLoc;
            this.btnUtilityBP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnUtilityBP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUtilityBP.ForeColor = System.Drawing.Color.White;
            this.btnUtilityBP.Location = new System.Drawing.Point(7, 42);
            this.btnUtilityBP.Name = "btnUtilityBP";
            this.btnUtilityBP.Size = new System.Drawing.Size(68, 145);
            this.btnUtilityBP.TabIndex = 10;
            this.btnUtilityBP.UseVisualStyleBackColor = true;
            this.btnUtilityBP.Click += new System.EventHandler(this.btnUtilityBP_Click);
            // 
            // label12
            // 
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Location = new System.Drawing.Point(0, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 39);
            this.label12.TabIndex = 10;
            this.label12.Text = "Utility Meter/BP Combo ";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_StackedBoltLoc
            // 
            this.panel_StackedBoltLoc.Controls.Add(this.label28);
            this.panel_StackedBoltLoc.Controls.Add(this.btnStackedSwitch);
            this.panel_StackedBoltLoc.Controls.Add(this.label30);
            this.panel_StackedBoltLoc.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_StackedBoltLoc.Location = new System.Drawing.Point(918, 3);
            this.panel_StackedBoltLoc.Name = "panel_StackedBoltLoc";
            this.panel_StackedBoltLoc.Size = new System.Drawing.Size(83, 190);
            this.panel_StackedBoltLoc.TabIndex = 28;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(95, 2);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(31, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "5000";
            this.label28.Visible = false;
            // 
            // btnStackedSwitch
            // 
            this.btnStackedSwitch.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_StackedBoltLoc;
            this.btnStackedSwitch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnStackedSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStackedSwitch.ForeColor = System.Drawing.Color.White;
            this.btnStackedSwitch.Location = new System.Drawing.Point(7, 42);
            this.btnStackedSwitch.Name = "btnStackedSwitch";
            this.btnStackedSwitch.Size = new System.Drawing.Size(68, 145);
            this.btnStackedSwitch.TabIndex = 10;
            this.btnStackedSwitch.UseVisualStyleBackColor = true;
            this.btnStackedSwitch.Click += new System.EventHandler(this.btnStackedSwitch_Click);
            // 
            // label30
            // 
            this.label30.Dock = System.Windows.Forms.DockStyle.Top;
            this.label30.Location = new System.Drawing.Point(0, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 39);
            this.label30.TabIndex = 10;
            this.label30.Text = "Stacked Indv Fused SW";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_boltloc
            // 
            this.panel_boltloc.Controls.Add(this.label25);
            this.panel_boltloc.Controls.Add(this.btnBoltLoc);
            this.panel_boltloc.Controls.Add(this.label27);
            this.panel_boltloc.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_boltloc.Location = new System.Drawing.Point(835, 3);
            this.panel_boltloc.Name = "panel_boltloc";
            this.panel_boltloc.Size = new System.Drawing.Size(83, 190);
            this.panel_boltloc.TabIndex = 27;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(95, 2);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 26;
            this.label25.Text = "5000";
            this.label25.Visible = false;
            // 
            // btnBoltLoc
            // 
            this.btnBoltLoc.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_SingleBoltLoc;
            this.btnBoltLoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBoltLoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBoltLoc.ForeColor = System.Drawing.Color.White;
            this.btnBoltLoc.Location = new System.Drawing.Point(7, 42);
            this.btnBoltLoc.Name = "btnBoltLoc";
            this.btnBoltLoc.Size = new System.Drawing.Size(68, 145);
            this.btnBoltLoc.TabIndex = 10;
            this.btnBoltLoc.UseVisualStyleBackColor = true;
            this.btnBoltLoc.Click += new System.EventHandler(this.btnBoltLoc_Click);
            // 
            // label27
            // 
            this.label27.Dock = System.Windows.Forms.DockStyle.Top;
            this.label27.Location = new System.Drawing.Point(0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(83, 39);
            this.label27.TabIndex = 10;
            this.label27.Text = "Indv Fused SW";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_QMB
            // 
            this.panel_QMB.Controls.Add(this.label1);
            this.panel_QMB.Controls.Add(this.btnQMB);
            this.panel_QMB.Controls.Add(this.label24);
            this.panel_QMB.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_QMB.Location = new System.Drawing.Point(752, 3);
            this.panel_QMB.Name = "panel_QMB";
            this.panel_QMB.Size = new System.Drawing.Size(83, 190);
            this.panel_QMB.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "5000";
            this.label1.Visible = false;
            // 
            // btnQMB
            // 
            this.btnQMB.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_QMB;
            this.btnQMB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnQMB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQMB.ForeColor = System.Drawing.Color.White;
            this.btnQMB.Location = new System.Drawing.Point(7, 42);
            this.btnQMB.Name = "btnQMB";
            this.btnQMB.Size = new System.Drawing.Size(68, 145);
            this.btnQMB.TabIndex = 10;
            this.btnQMB.UseVisualStyleBackColor = true;
            this.btnQMB.Click += new System.EventHandler(this.btnQMB_Click);
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 39);
            this.label24.TabIndex = 10;
            this.label24.Text = "Group Mtd Fuse Sw";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_combo
            // 
            this.panel_combo.Controls.Add(this.label9);
            this.panel_combo.Controls.Add(this.btn_Combo);
            this.panel_combo.Controls.Add(this.label21);
            this.panel_combo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_combo.Location = new System.Drawing.Point(669, 3);
            this.panel_combo.Name = "panel_combo";
            this.panel_combo.Size = new System.Drawing.Size(83, 190);
            this.panel_combo.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(95, 2);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "5000";
            this.label9.Visible = false;
            // 
            // btn_Combo
            // 
            this.btn_Combo.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_MeteroverBreaker;
            this.btn_Combo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_Combo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Combo.ForeColor = System.Drawing.Color.White;
            this.btn_Combo.Location = new System.Drawing.Point(7, 42);
            this.btn_Combo.Name = "btn_Combo";
            this.btn_Combo.Size = new System.Drawing.Size(68, 145);
            this.btn_Combo.TabIndex = 10;
            this.btn_Combo.UseVisualStyleBackColor = true;
            this.btn_Combo.Click += new System.EventHandler(this.btn_Combo_Click);
            // 
            // label21
            // 
            this.label21.Dock = System.Windows.Forms.DockStyle.Top;
            this.label21.Location = new System.Drawing.Point(0, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 39);
            this.label21.TabIndex = 10;
            this.label21.Text = "Utility Meter / MB Combo";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_3high_Meter
            // 
            this.panel_3high_Meter.Controls.Add(this.lblLineupAmpacity);
            this.panel_3high_Meter.Controls.Add(this.btn_Socket3);
            this.panel_3high_Meter.Controls.Add(this.label5);
            this.panel_3high_Meter.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_3high_Meter.Location = new System.Drawing.Point(586, 3);
            this.panel_3high_Meter.Name = "panel_3high_Meter";
            this.panel_3high_Meter.Size = new System.Drawing.Size(83, 190);
            this.panel_3high_Meter.TabIndex = 20;
            // 
            // lblLineupAmpacity
            // 
            this.lblLineupAmpacity.AutoSize = true;
            this.lblLineupAmpacity.Location = new System.Drawing.Point(95, 2);
            this.lblLineupAmpacity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLineupAmpacity.Name = "lblLineupAmpacity";
            this.lblLineupAmpacity.Size = new System.Drawing.Size(31, 13);
            this.lblLineupAmpacity.TabIndex = 26;
            this.lblLineupAmpacity.Text = "5000";
            this.lblLineupAmpacity.Visible = false;
            // 
            // btn_Socket3
            // 
            this.btn_Socket3.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_3meters;
            this.btn_Socket3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_Socket3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Socket3.ForeColor = System.Drawing.Color.White;
            this.btn_Socket3.Location = new System.Drawing.Point(7, 42);
            this.btn_Socket3.Name = "btn_Socket3";
            this.btn_Socket3.Size = new System.Drawing.Size(68, 145);
            this.btn_Socket3.TabIndex = 10;
            this.btn_Socket3.UseVisualStyleBackColor = true;
            this.btn_Socket3.Click += new System.EventHandler(this.btn_Socket3_Click);
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 39);
            this.label5.TabIndex = 10;
            this.label5.Text = "3 Socket Multi-Metering ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_6Meters
            // 
            this.panel_6Meters.Controls.Add(this.btn_Socket6);
            this.panel_6Meters.Controls.Add(this.label6);
            this.panel_6Meters.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_6Meters.Location = new System.Drawing.Point(503, 3);
            this.panel_6Meters.Name = "panel_6Meters";
            this.panel_6Meters.Size = new System.Drawing.Size(83, 190);
            this.panel_6Meters.TabIndex = 21;
            // 
            // btn_Socket6
            // 
            this.btn_Socket6.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_6meter;
            this.btn_Socket6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_Socket6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Socket6.ForeColor = System.Drawing.Color.White;
            this.btn_Socket6.Location = new System.Drawing.Point(7, 42);
            this.btn_Socket6.Name = "btn_Socket6";
            this.btn_Socket6.Size = new System.Drawing.Size(68, 145);
            this.btn_Socket6.TabIndex = 10;
            this.btn_Socket6.UseVisualStyleBackColor = true;
            this.btn_Socket6.Click += new System.EventHandler(this.btn_Socket6_Click);
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 39);
            this.label6.TabIndex = 10;
            this.label6.Text = "6 Socket Multi-Metering ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_single_section
            // 
            this.panel_single_section.Controls.Add(this.btn_SingleBreaker);
            this.panel_single_section.Controls.Add(this.label11);
            this.panel_single_section.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_single_section.Location = new System.Drawing.Point(420, 3);
            this.panel_single_section.Name = "panel_single_section";
            this.panel_single_section.Size = new System.Drawing.Size(83, 190);
            this.panel_single_section.TabIndex = 16;
            // 
            // btn_SingleBreaker
            // 
            this.btn_SingleBreaker.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_SingleIMD;
            this.btn_SingleBreaker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_SingleBreaker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SingleBreaker.ForeColor = System.Drawing.Color.White;
            this.btn_SingleBreaker.Location = new System.Drawing.Point(7, 42);
            this.btn_SingleBreaker.Name = "btn_SingleBreaker";
            this.btn_SingleBreaker.Size = new System.Drawing.Size(68, 145);
            this.btn_SingleBreaker.TabIndex = 10;
            this.btn_SingleBreaker.UseVisualStyleBackColor = true;
            this.btn_SingleBreaker.Click += new System.EventHandler(this.btn_SingleBreaker_Click);
            // 
            // label11
            // 
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 39);
            this.label11.TabIndex = 10;
            this.label11.Text = "Indiv Fixed Mtd Breaker";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_auxiliary
            // 
            this.panel_auxiliary.Controls.Add(this.btn_AuxiliarySection);
            this.panel_auxiliary.Controls.Add(this.label8);
            this.panel_auxiliary.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_auxiliary.Location = new System.Drawing.Point(337, 3);
            this.panel_auxiliary.Name = "panel_auxiliary";
            this.panel_auxiliary.Size = new System.Drawing.Size(83, 190);
            this.panel_auxiliary.TabIndex = 23;
            // 
            // btn_AuxiliarySection
            // 
            this.btn_AuxiliarySection.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_aux;
            this.btn_AuxiliarySection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_AuxiliarySection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AuxiliarySection.ForeColor = System.Drawing.Color.White;
            this.btn_AuxiliarySection.Location = new System.Drawing.Point(7, 42);
            this.btn_AuxiliarySection.Name = "btn_AuxiliarySection";
            this.btn_AuxiliarySection.Size = new System.Drawing.Size(68, 145);
            this.btn_AuxiliarySection.TabIndex = 10;
            this.btn_AuxiliarySection.UseVisualStyleBackColor = true;
            this.btn_AuxiliarySection.Click += new System.EventHandler(this.btn_AuxiliarySection_Click);
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 39);
            this.label8.TabIndex = 10;
            this.label8.Text = "Auxiliary Section";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_stacked
            // 
            this.panel_stacked.Controls.Add(this.btn_StackedFeeders);
            this.panel_stacked.Controls.Add(this.label2);
            this.panel_stacked.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_stacked.Location = new System.Drawing.Point(254, 3);
            this.panel_stacked.Name = "panel_stacked";
            this.panel_stacked.Size = new System.Drawing.Size(83, 190);
            this.panel_stacked.TabIndex = 17;
            // 
            // btn_StackedFeeders
            // 
            this.btn_StackedFeeders.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_stackedIMD;
            this.btn_StackedFeeders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_StackedFeeders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_StackedFeeders.ForeColor = System.Drawing.Color.White;
            this.btn_StackedFeeders.Location = new System.Drawing.Point(7, 42);
            this.btn_StackedFeeders.Name = "btn_StackedFeeders";
            this.btn_StackedFeeders.Size = new System.Drawing.Size(68, 145);
            this.btn_StackedFeeders.TabIndex = 10;
            this.btn_StackedFeeders.UseVisualStyleBackColor = true;
            this.btn_StackedFeeders.Click += new System.EventHandler(this.btn_StackedFeeders_Click);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 39);
            this.label2.TabIndex = 10;
            this.label2.Text = "Stacked Indiv Mtd Breaker";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_MeterSection
            // 
            this.panel_MeterSection.Controls.Add(this.btn_Meter_Section);
            this.panel_MeterSection.Controls.Add(this.label7);
            this.panel_MeterSection.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_MeterSection.Location = new System.Drawing.Point(171, 3);
            this.panel_MeterSection.Name = "panel_MeterSection";
            this.panel_MeterSection.Size = new System.Drawing.Size(83, 190);
            this.panel_MeterSection.TabIndex = 22;
            // 
            // btn_Meter_Section
            // 
            this.btn_Meter_Section.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_meteronly;
            this.btn_Meter_Section.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_Meter_Section.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Meter_Section.ForeColor = System.Drawing.Color.White;
            this.btn_Meter_Section.Location = new System.Drawing.Point(7, 42);
            this.btn_Meter_Section.Name = "btn_Meter_Section";
            this.btn_Meter_Section.Size = new System.Drawing.Size(68, 145);
            this.btn_Meter_Section.TabIndex = 10;
            this.btn_Meter_Section.UseVisualStyleBackColor = true;
            this.btn_Meter_Section.Click += new System.EventHandler(this.btn_Meter_Section_Click);
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 39);
            this.label7.TabIndex = 10;
            this.label7.Text = "Utility Metering";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_singleILine
            // 
            this.panel_singleILine.Controls.Add(this.btn_SingleIline);
            this.panel_singleILine.Controls.Add(this.label3);
            this.panel_singleILine.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_singleILine.Location = new System.Drawing.Point(88, 3);
            this.panel_singleILine.Name = "panel_singleILine";
            this.panel_singleILine.Size = new System.Drawing.Size(83, 190);
            this.panel_singleILine.TabIndex = 18;
            // 
            // btn_SingleIline
            // 
            this.btn_SingleIline.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_SingleILine;
            this.btn_SingleIline.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_SingleIline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SingleIline.ForeColor = System.Drawing.Color.White;
            this.btn_SingleIline.Location = new System.Drawing.Point(7, 42);
            this.btn_SingleIline.Name = "btn_SingleIline";
            this.btn_SingleIline.Size = new System.Drawing.Size(68, 145);
            this.btn_SingleIline.TabIndex = 10;
            this.btn_SingleIline.UseVisualStyleBackColor = true;
            this.btn_SingleIline.Click += new System.EventHandler(this.btn_SingleIline_Click);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 39);
            this.label3.TabIndex = 10;
            this.label3.Text = "Group Mtd CBs Single Row";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_doubleILine
            // 
            this.panel_doubleILine.Controls.Add(this.btn_DoubleIline);
            this.panel_doubleILine.Controls.Add(this.label4);
            this.panel_doubleILine.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_doubleILine.Location = new System.Drawing.Point(3, 3);
            this.panel_doubleILine.Name = "panel_doubleILine";
            this.panel_doubleILine.Size = new System.Drawing.Size(85, 190);
            this.panel_doubleILine.TabIndex = 19;
            // 
            // btn_DoubleIline
            // 
            this.btn_DoubleIline.BackgroundImage = global::LayoutFAST_Dev.Properties.Resources.jb_DoubleILine;
            this.btn_DoubleIline.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_DoubleIline.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_DoubleIline.ForeColor = System.Drawing.Color.White;
            this.btn_DoubleIline.Location = new System.Drawing.Point(4, 42);
            this.btn_DoubleIline.Name = "btn_DoubleIline";
            this.btn_DoubleIline.Size = new System.Drawing.Size(77, 145);
            this.btn_DoubleIline.TabIndex = 10;
            this.btn_DoubleIline.UseVisualStyleBackColor = true;
            this.btn_DoubleIline.Click += new System.EventHandler(this.btn_DoubleIline_Click);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 39);
            this.label4.TabIndex = 10;
            this.label4.Text = "Group Mtd CBs Double Row";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_sel_layout_all_Left
            // 
            this.panel_sel_layout_all_Left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_sel_layout_all_Left.Controls.Add(this.pnl_Sel_Layout_left_bottom);
            this.panel_sel_layout_all_Left.Controls.Add(this.panel_Sel_layout_Left);
            this.panel_sel_layout_all_Left.Controls.Add(this.pnl_Sel_layout_Lineup_Designation);
            this.panel_sel_layout_all_Left.Location = new System.Drawing.Point(0, 0);
            this.panel_sel_layout_all_Left.Name = "panel_sel_layout_all_Left";
            this.panel_sel_layout_all_Left.Size = new System.Drawing.Size(194, 533);
            this.panel_sel_layout_all_Left.TabIndex = 14;
            this.panel_sel_layout_all_Left.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_sel_layout_all_Left_Paint);
            // 
            // pnl_Sel_Layout_left_bottom
            // 
            this.pnl_Sel_Layout_left_bottom.Controls.Add(this.label32);
            this.pnl_Sel_Layout_left_bottom.Controls.Add(this.label31);
            this.pnl_Sel_Layout_left_bottom.Controls.Add(this.comboBox8);
            this.pnl_Sel_Layout_left_bottom.Controls.Add(this.label23);
            this.pnl_Sel_Layout_left_bottom.Controls.Add(this.button50);
            this.pnl_Sel_Layout_left_bottom.Controls.Add(this.comboBox7);
            this.pnl_Sel_Layout_left_bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl_Sel_Layout_left_bottom.Location = new System.Drawing.Point(192, 292);
            this.pnl_Sel_Layout_left_bottom.Name = "pnl_Sel_Layout_left_bottom";
            this.pnl_Sel_Layout_left_bottom.Size = new System.Drawing.Size(0, 239);
            this.pnl_Sel_Layout_left_bottom.TabIndex = 13;
            this.pnl_Sel_Layout_left_bottom.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(164, 195);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(19, 13);
            this.label32.TabIndex = 71;
            this.label32.Text = "[1]";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(13, 195);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 13);
            this.label31.TabIndex = 70;
            this.label31.Text = "Queue:";
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(13, 214);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(174, 21);
            this.comboBox8.TabIndex = 69;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 77);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 13);
            this.label23.TabIndex = 68;
            this.label23.Text = "Favorite:";
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(9, 8);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(174, 25);
            this.button50.TabIndex = 59;
            this.button50.Text = "Apply";
            this.button50.UseVisualStyleBackColor = true;
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(9, 96);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(174, 21);
            this.comboBox7.TabIndex = 67;
            // 
            // panel_Sel_layout_Left
            // 
            this.panel_Sel_layout_Left.AutoScroll = true;
            this.panel_Sel_layout_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_Sel_layout_Left.Location = new System.Drawing.Point(0, 44);
            this.panel_Sel_layout_Left.Name = "panel_Sel_layout_Left";
            this.panel_Sel_layout_Left.Size = new System.Drawing.Size(192, 487);
            this.panel_Sel_layout_Left.TabIndex = 12;
            this.panel_Sel_layout_Left.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Sel_layout_Left_Paint);
            // 
            // pnl_Sel_layout_Lineup_Designation
            // 
            this.pnl_Sel_layout_Lineup_Designation.AutoScroll = true;
            this.pnl_Sel_layout_Lineup_Designation.Controls.Add(this.txtLineupDescription);
            this.pnl_Sel_layout_Lineup_Designation.Controls.Add(this.label22);
            this.pnl_Sel_layout_Lineup_Designation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Sel_layout_Lineup_Designation.Location = new System.Drawing.Point(0, 0);
            this.pnl_Sel_layout_Lineup_Designation.Name = "pnl_Sel_layout_Lineup_Designation";
            this.pnl_Sel_layout_Lineup_Designation.Size = new System.Drawing.Size(192, 44);
            this.pnl_Sel_layout_Lineup_Designation.TabIndex = 14;
            // 
            // txtLineupDescription
            // 
            this.txtLineupDescription.Location = new System.Drawing.Point(8, 22);
            this.txtLineupDescription.Name = "txtLineupDescription";
            this.txtLineupDescription.Size = new System.Drawing.Size(162, 20);
            this.txtLineupDescription.TabIndex = 64;
            this.txtLineupDescription.Text = "Switchboard Lineup 1";
            this.txtLineupDescription.TextChanged += new System.EventHandler(this.txtLineupDescription_TextChanged);
            this.txtLineupDescription.LostFocus += new System.EventHandler(this.txtLineupDescription_LostFocus);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 5);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 13);
            this.label22.TabIndex = 63;
            this.label22.Text = "LineUp Designation:";
            // 
            // tabPage8
            // 
            this.tabPage8.Location = new System.Drawing.Point(0, 0);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(200, 100);
            this.tabPage8.TabIndex = 0;
            // 
            // tabPage10
            // 
            this.tabPage10.Location = new System.Drawing.Point(0, 0);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(200, 100);
            this.tabPage10.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.tutorialToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1323, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "File";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem4,
            this.toolStripMenuItem2});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem3.Text = "File";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.toolStripMenuItem1.Text = "About";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(107, 22);
            this.toolStripMenuItem4.Text = "Help";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
            this.toolStripMenuItem2.Text = "Close";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // tutorialToolStripMenuItem
            // 
            this.tutorialToolStripMenuItem.Name = "tutorialToolStripMenuItem";
            this.tutorialToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.tutorialToolStripMenuItem.Text = " Tutorial";
            this.tutorialToolStripMenuItem.Click += new System.EventHandler(this.tutorialToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_Clear_Lineup);
            this.panel1.Controls.Add(this.panel_sel_layout_all_Left);
            this.panel1.Controls.Add(this.btn_Insert_Lineup);
            this.panel1.Controls.Add(this.lbl_Lineup_Dimensions);
            this.panel1.Controls.Add(this.btn_Catalog);
            this.panel1.Controls.Add(this.btn_Specifications);
            this.panel1.Controls.Add(this.btn_Diget);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 671);
            this.panel1.TabIndex = 0;
            // 
            // frmSwitchboardSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1323, 727);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSwitchboardSelector";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LayoutFAST";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSwitchboardSelector_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.SectionLayout.ResumeLayout(false);
            this.panel_Center_Tabs.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.panel_Sel_layout_bottom.ResumeLayout(false);
            this.panel_sel_layout_top_center.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.panel_utilityBP.ResumeLayout(false);
            this.panel_utilityBP.PerformLayout();
            this.panel_StackedBoltLoc.ResumeLayout(false);
            this.panel_StackedBoltLoc.PerformLayout();
            this.panel_boltloc.ResumeLayout(false);
            this.panel_boltloc.PerformLayout();
            this.panel_QMB.ResumeLayout(false);
            this.panel_QMB.PerformLayout();
            this.panel_combo.ResumeLayout(false);
            this.panel_combo.PerformLayout();
            this.panel_3high_Meter.ResumeLayout(false);
            this.panel_3high_Meter.PerformLayout();
            this.panel_6Meters.ResumeLayout(false);
            this.panel_single_section.ResumeLayout(false);
            this.panel_auxiliary.ResumeLayout(false);
            this.panel_stacked.ResumeLayout(false);
            this.panel_MeterSection.ResumeLayout(false);
            this.panel_singleILine.ResumeLayout(false);
            this.panel_doubleILine.ResumeLayout(false);
            this.panel_sel_layout_all_Left.ResumeLayout(false);
            this.pnl_Sel_Layout_left_bottom.ResumeLayout(false);
            this.pnl_Sel_Layout_left_bottom.PerformLayout();
            this.pnl_Sel_layout_Lineup_Designation.ResumeLayout(false);
            this.pnl_Sel_layout_Lineup_Designation.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage SectionLayout;
        private System.Windows.Forms.Panel panel_Sel_layout_bottom;
        private System.Windows.Forms.Button btn_Send_Lineup;
        private System.Windows.Forms.Label lbl_Lineup_Dimensions;
        private System.Windows.Forms.Button btn_RequestDrawings;
        private System.Windows.Forms.Button btn_Insert_Lineup;
        private System.Windows.Forms.Button btn_3dView;
        private System.Windows.Forms.Button btn_Save_to_Favorites;
        private System.Windows.Forms.Button btn_Catalog;
        private System.Windows.Forms.Button btn_Specifications;
        private System.Windows.Forms.Button btn_Diget;
        private System.Windows.Forms.Panel panel_Center_Tabs;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Panel panel_sel_layout_top_center;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Panel panel_single_section;
        private System.Windows.Forms.Button btn_SingleBreaker;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel_auxiliary;
        private System.Windows.Forms.Button btn_AuxiliarySection;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel_stacked;
        private System.Windows.Forms.Button btn_StackedFeeders;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel_MeterSection;
        private System.Windows.Forms.Button btn_Meter_Section;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel_singleILine;
        private System.Windows.Forms.Button btn_SingleIline;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel_6Meters;
        private System.Windows.Forms.Button btn_Socket6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel_doubleILine;
        private System.Windows.Forms.Button btn_DoubleIline;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel_3high_Meter;
        private System.Windows.Forms.Button btn_Socket3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Panel panel_sel_layout_all_Left;
        private System.Windows.Forms.Panel pnl_Sel_Layout_left_bottom;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Panel panel_Sel_layout_Left;
        private System.Windows.Forms.Panel pnl_Sel_layout_Lineup_Designation;
        private System.Windows.Forms.TextBox txtLineupDescription;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btn_Clear_Lineup;
        private System.Windows.Forms.Label lblLineupAmpacity;
        private System.Windows.Forms.Panel panel_combo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_Combo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel_boltloc;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnBoltLoc;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel_QMB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQMB;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel_StackedBoltLoc;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnStackedSwitch;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel_utilityBP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnUtilityBP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem tutorialToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
    }
}