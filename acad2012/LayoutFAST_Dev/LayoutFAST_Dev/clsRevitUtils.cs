﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace testplugin
{
    class clsRevitUtils
    {

        public List<String> get_Max_Device_Amps(List<ReadXLS.clsSEQF_Product> tmp, Int32 iSystemAmps)
        {
            bool found = false;

            int i = 0;
            int iSectionAmps = 0;
            List<String> lstWidths = new List<string>();
            List<ReadXLS.clsSEQF_Product> lt = tmp.Where(lp => lp.column == "Max Dev Amps").ToList();
            foreach (ReadXLS.clsSEQF_Product cp in lt)
            {
                if (cp.column == "Max Dev Amps")
                {
                    i = 0;
                    found = false;
                    while (i < lstWidths.Count && !found)
                    {
                        if (lstWidths[i] == cp.value)
                        {
                            found = true;
                        }
                        i++;
                    }
                    if (!found)
                    {
                        iSectionAmps = Convert.ToInt32(cp.value);
                        if (iSectionAmps <= iSystemAmps)
                        {
                            lstWidths.Add(cp.value.ToString());
                        }
                    }
                }
            }

            return lstWidths;
        }

        public string get_field(int irownumber, List<ReadXLS.clsSEQF_Product> tmplist, string strField)
        {
            List<ReadXLS.clsSEQF_Product> tlst = new List<ReadXLS.clsSEQF_Product>();

            string strq = "";

            tlst = tmplist.Where(lp => lp.column == strField && lp.irownumber == irownumber).ToList();

            if (tlst.Count > 0)
            {
                if (tlst[0].value != null)
                {
                    strq = tlst[0].value.ToString();
                }
            }

            return strq;
        }

        public List<ReadXLS.clsSEQF_Product> get_row(int irownumber, List<ReadXLS.clsSEQF_Product> tmplist)
        {
            List<ReadXLS.clsSEQF_Product> tlst = new List<ReadXLS.clsSEQF_Product>();


            tlst = tmplist.Where(lp => lp.irownumber == irownumber).ToList();

            return tlst;
        }

        public List<ReadXLS.clsSEQF_Product> get_list_of_rows_opt1(List<ReadXLS.clsSEQF_Product> lstInput, string strColumn, string strValue)
        {
            List<ReadXLS.clsSEQF_Product> lstTMP = new List<ReadXLS.clsSEQF_Product>();

            lstTMP = lstInput.Where(lp => lp.value == strValue && lp.column == strColumn).ToList();

            return lstTMP;
        }
        public List<ReadXLS.clsSEQF_Product> find_rows_with_this_field(List<ReadXLS.clsSEQF_Product> lInput, List<ReadXLS.clsSEQF_Product> lst_Overall, string strColumn, string strValue)
        {
            List<ReadXLS.clsSEQF_Product> tmp = new List<ReadXLS.clsSEQF_Product>();
            foreach (ReadXLS.clsSEQF_Product cc in lInput)
            {
                List<ReadXLS.clsSEQF_Product> xtmp = lst_Overall.Where(lp => lp.irownumber == cc.irownumber && strColumn == lp.column && strValue == lp.value).ToList();
                if (xtmp.Count > 0)
                {
                    tmp.Add(xtmp[0]);
                }
            }
            return tmp;
        }
        public Int32 calc_max_amps(List<ReadXLS.clsSEQF_Product> tmp, string strw, int iMaxAmps, out int irow1)
        {
            int iamps = iMaxAmps;

            List<ReadXLS.clsSEQF_Product> tmp1 = new List<ReadXLS.clsSEQF_Product>();

            string ima = "";

            irow1 = 0;
            tmp1 = get_list_of_rows_opt1(tmp, "Width", strw);

            if (tmp1.Count > 0)
            {
                irow1 = tmp1[0].irownumber;
                ima = get_field(tmp1[0].irownumber, tmp, "max amps");

                try
                {
                    int i = Convert.ToInt32(ima);
                    if (i < iamps) iamps = i;
                }
                catch
                {
                    iamps = -1;
                }
            }
            return iamps;
        }

        public List<String> get_utility_types(List<ReadXLS.clsSEQF_Product> tmp)
        {
            bool found = false;

            int i = 0;

            List<String> lstWidths = new List<string>();

            foreach (ReadXLS.clsSEQF_Product cp in tmp)
            {
                if (cp.column == "Utility Type")
                {
                    found = false;
                    i = 0;
                    while (i < lstWidths.Count && !found)
                    {
                        if (lstWidths[i].ToString() == cp.value.ToString())
                        {
                            found = true;
                        }
                        else
                        {
                            i++;
                        }
                    }
                    if (!found) lstWidths.Add(cp.value.ToString());
                }
            }

            return lstWidths;
        }

        public List<String> get_widths(List<ReadXLS.clsSEQF_Product> tmp)
        {
            bool found = false;

            int i = 0;

            List<String> lstWidths = new List<string>();

            foreach (ReadXLS.clsSEQF_Product cp in tmp)
            {
                if (cp.column == "Width")
                {
                    i = 0;
                    while (i < lstWidths.Count && !found)
                    {
                        if (lstWidths[i] == cp.value) found = true;
                        i++;
                    }
                    if (!found) lstWidths.Add(cp.value.ToString());
                }
            }

            return lstWidths;
        }

        public List<ReadXLS.clsSEQF_Product> get_list_of_rows_opt3(List<ReadXLS.clsSEQF_Product> lstInput, string strColumn, string strValue)
        {
            List<ReadXLS.clsSEQF_Product> lstOutput = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> lstrow = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> lstTMP = new List<ReadXLS.clsSEQF_Product>();

            lstTMP = lstInput.Where(lp => lp.value == strValue && lp.column == strColumn).ToList();

            foreach (ReadXLS.clsSEQF_Product cgb in lstTMP)
            {
                lstrow = new List<ReadXLS.clsSEQF_Product>();
                lstrow = lstInput.Where(lp => lp.irownumber == cgb.irownumber).ToList();
                foreach (ReadXLS.clsSEQF_Product cc in lstrow)
                {

                    lstOutput.Add(cc);
                }

            }

            return lstOutput;
        }
     
        public void get_lineup_section(string strPValue, out int iSectionNumber, out string strLineup, out string strDesc)
        {
            int i = strPValue.IndexOf("~");
            int isec = 0;
            int len = strPValue.Length;
            
            string strBay = "";
            string strL = "";
            string strN = "";

            strL = strPValue.Substring(0, i);
            strN = strPValue.Substring(i + 1, len - i - 1);
            i = strN.IndexOf("~");
            len = strN.Length;
            strBay = strN.Substring(0, i);
            strDesc = strN.Substring(i + 1, len - i - 1);
            if (strN.Length > 0) isec = Convert.ToInt32(strBay);
            iSectionNumber = isec;
            strLineup = strL;
        }

     
        public List<ReadXLS.clsSEQF_Product> get_list_of_rows_opt2(List<ReadXLS.clsSEQF_Product> lstInput, string strColumn, string strValue)
        {
            List<ReadXLS.clsSEQF_Product> lstTMP = new List<ReadXLS.clsSEQF_Product>();

            lstTMP = lstInput.Where(lp => lp.value == strValue && lp.column == strColumn).ToList();

            return lstTMP;
        }

        public List<ReadXLS.clsSEQF_Product> get_list_of_rows_with_irownumber(List<ReadXLS.clsSEQF_Product> lstInput, List<ReadXLS.clsSEQF_Product> lst_Solutions)
        {
            List<ReadXLS.clsSEQF_Product> lstOutput = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> lstTMP = new List<ReadXLS.clsSEQF_Product>();

            foreach (ReadXLS.clsSEQF_Product cp in lstInput)
            {
                lstOutput = lst_Solutions.Where(lp => lp.irownumber == cp.irownumber).ToList();

                foreach (ReadXLS.clsSEQF_Product co in lstOutput)
                {
                    lstTMP.Add(co);
                }

            }

            return lstTMP;
        }
        public List<ReadXLS.clsSEQF_Product> get_list_of_rows(List<ReadXLS.clsSEQF_Product> lstInput, string strColumn, string strValue, string subcolumn, string subvalue)
        {
            bool found = false;
            List<ReadXLS.clsSEQF_Product> lstOutput = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> lstrow = new List<ReadXLS.clsSEQF_Product>();
            List<ReadXLS.clsSEQF_Product> lstTMP = new List<ReadXLS.clsSEQF_Product>();

            lstTMP = lstInput.Where(lp => lp.value == strValue && lp.column == strColumn).ToList();

            foreach (ReadXLS.clsSEQF_Product cgb in lstTMP)
            {
                found = false;
                lstrow = new List<ReadXLS.clsSEQF_Product>();
                lstrow = lstInput.Where(lp => lp.irownumber == cgb.irownumber).ToList();
                foreach (ReadXLS.clsSEQF_Product cc in lstrow)
                {
                    if (cc.value == subvalue && cc.column == subcolumn) found = true;
                }
                if (found)
                {
                    foreach (ReadXLS.clsSEQF_Product cc in lstrow)
                    {
                        lstOutput.Add(cc);
                    }
                }
            }

            return lstOutput;
        }

        public void add_to_combo(System.Windows.Forms.ComboBox cmbx, string strValue)
        {
            bool found = false;

            int i = 0;

            while (i < cmbx.Items.Count && !found)
            {
                if (strValue == cmbx.Items[i].ToString()) found = true;
                i++;
            }

            if (!found)
            {
                cmbx.Items.Add(strValue);
            }
        }
     

    }
}
